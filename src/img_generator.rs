use image::*;
extern crate num_complex;
use chrono::prelude::*;
use rand::{SeedableRng, rngs::StdRng, Rng};
use rand::seq::SliceRandom;
use serde::{Serialize, Deserialize};
extern crate palette;
use palette::{Srgb, Hsv, Hsl, Pixel};
use std::path::{Path,PathBuf};
use crate::pkg_chaos_game::{ChaosGameConfig,gen_chaos_game};
use crate::pkg_julia::{JuliaConfig,gen_julia};
use crate::pkg_dragon_curve::{DragonConfig, gen_dragon};
use crate::pkg_barnsley_fern::{BarnsleyFernConfig, gen_barnsley_fern};
use crate::pkg_mandelbrot::{MandelbrotConfig, gen_mandelbrot};
use std::fs::{write,read_to_string};



#[derive(Serialize, Deserialize, Copy, Clone, Debug)]
pub struct ColorRGB {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub a: u8,

}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct SaveOption {
    pub scale_factor: u32,
    pub crop_h: u32,
    pub preview_downscale: f64,
    pub enable_multithreading: bool,
    pub contrast: f32,
    pub brightness: i32,
    pub path: PathBuf,
}

pub struct UniConfig{
    pub chaos_game_config: ChaosGameConfig,
    pub julia_config: JuliaConfig,
    pub dragon_config: DragonConfig,
    pub barnsley_fern_config: BarnsleyFernConfig,
    pub mandelbrot_config: MandelbrotConfig,
}

pub trait Config {
    fn get_type_no(&self) -> usize;
    fn serialize(&self) -> String;
    fn deserialize(&mut self, json_string: &str)-> Result<(), anyhow::Error>;
    fn gen_img(&mut self, save_option: &SaveOption) -> image::DynamicImage;
}


impl Config for ChaosGameConfig {
    fn get_type_no(&self)-> usize{
        0
    }
    fn serialize(&self) -> String{
        serde_json::to_string_pretty(&self).unwrap()
    }
    fn deserialize(&mut self, json_string: &str) -> Result<(), anyhow::Error>{
        let config:ChaosGameConfig =serde_json::from_str(json_string)?;
        (*self).scalar=config.scalar;
        (*self).no_point=config.no_point;
        (*self).include_mid_points=config.include_mid_points;
        (*self).include_center=config.include_center;
        (*self).distance_ratio=config.distance_ratio;
        (*self).condition_id=config.condition_id;
        (*self).condition_modifier_i=config.condition_modifier_i;
        (*self).add_transparency=config.add_transparency;
        (*self).background_type=config.background_type;
        Ok(())
    }
    fn gen_img(&mut self, save_option: &SaveOption)  -> image::DynamicImage {
        gen_chaos_game(&self,save_option)
    }

}

impl Config for JuliaConfig {
    fn get_type_no(&self)-> usize{
        1
    }
    fn serialize(&self) -> String{
        serde_json::to_string_pretty(&self).unwrap()
    }
    fn deserialize(&mut self, json_string: &str) -> Result<(), anyhow::Error> {
        let config:JuliaConfig =serde_json::from_str(json_string)?;
        (*self).scalar=config.scalar;
        (*self).c_real=config.c_real;
        (*self).c_imaginary=config.c_imaginary;
        (*self).z_pow=config.z_pow;
        (*self).background_type=config.background_type;
        Ok(())
    }
    fn gen_img(&mut self, save_option: &SaveOption)  -> image::DynamicImage {
        gen_julia(&self,save_option)
    }
}

impl Config for DragonConfig {
    fn get_type_no(&self)-> usize{
        2
    }
    fn serialize(&self) -> String{
        serde_json::to_string_pretty(&self).unwrap()
    }
    fn deserialize(&mut self, json_string: &str) -> Result<(), anyhow::Error>{
        let config: DragonConfig = serde_json::from_str(json_string)?;
        (*self).max_dragon = config.max_dragon;
        (*self).enabled = config.enabled;
        (*self).position_1 = config.position_1;
        (*self).position_2 = config.position_2;
        (*self).position_3 = config.position_3;
        (*self).rotate = config.rotate;
        (*self).flip = config.flip;
        (*self).no_iter = config.no_iter;
        (*self).stroke_width = config.stroke_width;
        (*self).add_transparency = config.add_transparency;
        (*self).add_gradient = config.add_gradient;
        (*self).lighten = config.lighten;
        (*self).background_type = config.background_type;
        Ok(())
    }
    fn gen_img(&mut self, save_option: &SaveOption)  -> image::DynamicImage {
        gen_dragon(&self,save_option)
    }
}

impl Config for BarnsleyFernConfig {
    fn get_type_no(&self)-> usize{
        3
    }
    fn serialize(&self) -> String{
        serde_json::to_string_pretty(&self).unwrap()
    }
    fn deserialize(&mut self, json_string: &str)-> Result<(), anyhow::Error> {
        let config:BarnsleyFernConfig =serde_json::from_str(&json_string)?;
        (*self).max_no_weight = config.max_no_weight;
        (*self).enabled = config.enabled;
        (*self).coefficient = config.coefficient;
        (*self).probability_score = config.probability_score;
        (*self).sample_multiplier = config.sample_multiplier;
        (*self).weight_adj = config.weight_adj;
        (*self).background_type = config.background_type;
        Ok(())
    }
    fn gen_img(&mut self, save_option: &SaveOption)  -> image::DynamicImage {
        gen_barnsley_fern(&self,save_option)
    }
}

impl Config for MandelbrotConfig {
    fn get_type_no(&self)-> usize{
        4
    }
    fn serialize(&self) -> String{
        serde_json::to_string_pretty(&self).unwrap()
    }
    fn deserialize(&mut self, json_string: &str) -> Result<(), anyhow::Error>{
        let config:MandelbrotConfig =serde_json::from_str(json_string)?;
        (*self).scalar = config.scalar;
        (*self).max_zoom = config.max_zoom;
        (*self).min_x_percentage = config.min_x_percentage;
        (*self).min_y_percentage = config.min_y_percentage;
        (*self).lock_zoom_in = config.lock_zoom_in;
        (*self).show_zoom = config.show_zoom;
        (*self).show_selector = config.show_selector;
        (*self).max_iteration = config.max_iteration;
        (*self).max_z = config.max_z;
        (*self).history = config.history;
        (*self).size_ratio = config.size_ratio;
        (*self).coloring_type = config.coloring_type;
        (*self).color_type_2_coff = config.color_type_2_coff;
        Ok(())
    }
    fn gen_img(&mut self, save_option: &SaveOption)  -> image::DynamicImage {
        gen_mandelbrot(self,save_option)
    }
}

pub fn generate_color_palette_rgb(random_state:u64, no_color:i32) -> Vec<ColorRGB>{
    let mut vec_rgb:Vec<ColorRGB> = Vec::new();
    let vec_rgb_optimal:Vec<[u8;3]>=vec![[255,179,0],[128,62,117],[255,104,0],[166,189,215],[193,0,32],[206,162,98],[129,112,102],[0,125,52],[246,118,142],[0,83,138],[255,122,92],[83,55,122],[255,142,0],[179,40,81],[244,200,0],[127,24,13],[147,170,0],[89,51,21],[241,58,19],[35,44,22]];
    for i in 0..vec_rgb_optimal.len(){
        vec_rgb.push(ColorRGB{
            r: vec_rgb_optimal[i][0],
            g: vec_rgb_optimal[i][1],
            b: vec_rgb_optimal[i][2],
            a: 255
        } )
    }


    let mut rng = StdRng::seed_from_u64(random_state);
    vec_rgb.shuffle(&mut rng);
    if no_color>20{
        let no_random_color=no_color-20;
        let mut random_color_vec:Vec<ColorRGB>= Vec::new();
        for _ in 0..no_random_color{
            let m=rng.gen_range(0..vec_rgb_optimal.len());
            let (ra,ga,ba):(i32,i32,i32)=(rng.gen_range(-6..7) as i32 +vec_rgb_optimal[m][0] as i32,rng.gen_range(-20..22) as i32 +vec_rgb_optimal[m][1] as i32,rng.gen_range(-2..3) as i32 +vec_rgb_optimal[m][2] as i32);
            let new_r= if ra>255{255}else if ra<0{0} else {ra as u8};
            let new_g= if ga>255{255}else if ga<0{0} else {ga as u8};
            let new_b= if ba>255{255}else if ga<0{0} else {ba as u8};
            random_color_vec.push(ColorRGB{
                r: new_r,
                g: new_g,
                b: new_b,
                a: 255
            })
        }
        vec_rgb.append(&mut random_color_vec);

    }

    vec_rgb
}


    /**
    hsv_val_vec:0 = min Hue
    hsv_val_vec:1 = max Hue
    hsv_val_vec:2 = min Saturation
    hsv_val_vec:3 = max Saturation
    hsv_val_vec:4 = min Value
    hsv_val_vec:5 = max Value
    **/
pub fn generate_color_palette_hsv(no_color:&u32, hsv_val_vec:&Vec<f64>) -> Vec<ColorRGB>{

    assert!((*no_color)>1);
    let hue_step=(hsv_val_vec[1]-hsv_val_vec[0])/((*no_color)-1) as f64;
    let sat_step=(hsv_val_vec[3]-hsv_val_vec[2])/((*no_color)-1) as f64;
    let val_step=(hsv_val_vec[5]-hsv_val_vec[4])/((*no_color)-1) as f64;
    let mut vec_hsv:Vec<Hsv> = Vec::new();

    for i in 0..(*no_color){
        vec_hsv.push(Hsv::new((hsv_val_vec[0]+hue_step*(i as f64)) as f32, (hsv_val_vec[2]+sat_step*(i as f64)) as f32, (hsv_val_vec[4]+val_step*(i as f64)) as f32));
    }

    let vec_rgb=hsv_rgb_vec(&vec_hsv);
    vec_rgb
}

pub fn hsv_rgb(hsv:&Hsv) -> ColorRGB {
    let srgb_color:Srgb = (*hsv).into();
    let raw: [u8; 3] = Srgb::into_raw(srgb_color.into_format());
    ColorRGB{
        r: raw[0],
        g: raw[1],
        b: raw[2],
        a: 255,
    }
}

pub fn hsl_rgb(hsl:&Hsl) -> ColorRGB {
    let srgb_color:Srgb = (*hsl).into();
    let raw: [u8; 3] = Srgb::into_raw(srgb_color.into_format());
    ColorRGB{
        r: raw[0],
        g: raw[1],
        b: raw[2],
        a: 255,
    }
}

pub fn hsv_rgb_vec(vec_hsv:&Vec<Hsv>) -> Vec<ColorRGB>{
    let mut vec_rgb:Vec<ColorRGB>=Vec::new();
    for i in 0..vec_hsv.len(){
        vec_rgb.push(hsv_rgb(&vec_hsv[i]));
    }
    vec_rgb
}


pub fn rgb_hsv(rgb:&ColorRGB) -> Hsv {
    let raw=[(*rgb).r,(*rgb).g,(*rgb).b];
    let rbg_int = Srgb::from_raw(&raw);
    let raw_float: Srgb<f32> = rbg_int.into_format();
    let hsv_color: Hsv = raw_float.into();
    hsv_color
}

pub fn rgb_hsl(rgb:&ColorRGB) -> Hsl {
    let raw=[(*rgb).r,(*rgb).g,(*rgb).b];
    let rbg_int = Srgb::from_raw(&raw);
    let raw_float: Srgb<f32> = rbg_int.into_format();
    let hsl_color: Hsl = raw_float.into();
    hsl_color
}

pub fn rgb_hsv_vec(vec_rgb:&Vec<ColorRGB>) -> Vec<Hsv>{
    let mut vec_hsv:Vec<Hsv>=Vec::new();
    for i in 0..vec_rgb.len(){
        vec_hsv.push(rgb_hsv(&vec_rgb[i]));
    }
    vec_hsv
}



pub fn get_empty_image_size(scale_factor: f64) ->  (u32,u32) {
    ((scale_factor*super::WIDTH as f64)  as u32, (scale_factor*super::HEIGHT as f64)  as u32)
}



pub fn background_colorize(img: &ImageBuffer<Rgba<u8>, Vec<u8>>, background_type:&i32, save_option: &SaveOption) ->  (ImageBuffer<Rgba<u8>,Vec<u8>>, bool) {
    let (width, height) = img.dimensions();
    let mut buffer: image::RgbaImage =ImageBuffer::new(width, height);
    let mut return_flag= true;
    let color=if *background_type==0{
        image::Rgba([0, 0, 0, 255])
    }else if *background_type==1 {
        return_flag=false;
        image::Rgba([0, 0, 0, 255])
    }else {
        if (*save_option).scale_factor ==1 {
            image::Rgba([255, 255, 255, 255])
        }else {
            image::Rgba([0, 0, 0, 0])
        }
    };
    for (to, _) in buffer.pixels_mut().zip(img.pixels()) {
        *to = color;
    }
    (buffer,return_flag)
}



pub fn combine_fg_bg(fg:ImageBuffer<Rgba<u8>,Vec<u8>>,mut bg:ImageBuffer<Rgba<u8>,Vec<u8>>)->ImageBuffer<Rgba<u8>,Vec<u8>>{

    let (img_width,img_height)=bg.dimensions();
    for x in 0..img_width{
        for y in 0..img_height{
            let fg_pixel=fg.get_pixel(x, y);
            let image::Rgba(data_fg) = *fg_pixel;
            let bg_pixel=bg.get_pixel(x, y);
            let image::Rgba(data_bg) = *bg_pixel;
            let new_pixel=if data_bg[3]>0 {
                let r: u8 = (data_fg[0] as u32 * data_fg[3] as u32 / 255) as u8 + (data_bg[0] as u32 * (255 - data_fg[3] as u32) / 255) as u8;
                let g: u8 = (data_fg[1] as u32 * data_fg[3] as u32 / 255) as u8 + (data_bg[1] as u32 * (255 - data_fg[3] as u32) / 255) as u8;
                let b: u8 = (data_fg[2] as u32 * data_fg[3] as u32 / 255) as u8 + (data_bg[2] as u32 * (255 - data_fg[3] as u32) / 255) as u8;
                image::Rgba([r,g,b,data_bg[3]])
            }else {
                *fg_pixel
            };
            bg.put_pixel(x,y,new_pixel);
        }
    }
    bg
}


pub fn gen_icon()->DynamicImage{
    let img_x = 220;
    let img_y = 220;

    let scale_x = 2.3 / img_x as f32;
    let scale_y = 2.3 / img_y as f32;


    let mut imgbuf = image::ImageBuffer::new(img_x, img_y);

    for (x, y, pixel) in imgbuf.enumerate_pixels_mut() {
        let g = (170.5 / img_x as f32 * x as f32+60.5/img_y as f32 * y as f32) as u8;
        *pixel = image::Rgb([g, g, g]);
    }

    for x in 0..img_x {
        for y in 0..img_y {
            let cx = y as f32 * scale_x - 1.15;
            let cy = x as f32 * scale_y - 1.15;

            let c = num_complex::Complex::new(0.35, -0.06);
            let mut z = num_complex::Complex::new(cx, cy);

            let mut i = 0;
            while i < 255 && z.norm() <= 2.0 {
                z = z * z + c;
                i += 5;
            }

            let pixel = imgbuf.get_pixel_mut(x, y);
            let image::Rgb(data) = *pixel;
            *pixel = if i >30 {
                image::Rgb([i as u8, data[1]/3, data[2]/3])
            }else {
                image::Rgb([data[0], data[1], data[2]])
            }
        }
    }
    //Save Icon to File
    //imgbuf.save("fractal.png").unwrap();

    DynamicImage::ImageRgb8(imgbuf)
}

