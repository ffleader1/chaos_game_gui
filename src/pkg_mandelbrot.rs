extern crate num_complex;

use image::*;
use crate::img_generator::*;
use crate::io::*;
use std::thread;
use palette::{Hsl, Hsv, Saturate, Shade};
use std::f64::consts;
use std::f64::consts::{E, PI};
use serde::{Serialize, Deserialize};


#[derive(Serialize, Deserialize, Clone,Debug)]
pub struct MandelbrotConfig {
    pub scalar: f64,
    pub max_zoom: f64,
    pub min_x_percentage: f64,
    pub min_y_percentage: f64,
    pub lock_zoom_in: bool,
    pub show_zoom:bool,
    pub show_selector:bool,
    pub max_iteration: i32,
    pub max_z: f64,

    #[serde(skip)]
    pub cache_img: Vec<image::RgbaImage>,

    pub history: Vec<(f64,f64,f64)>,
    pub size_ratio: Vec<(f64,f64,f64)>,
    pub coloring_type: i32,
    pub color_type_2_coff: Vec<f64>,

    #[serde(skip)]
    pub color_palette: Vec<ColorRGB>,

}


fn create_foreground_thread(min_x:u64, min_y:u64, count:u64, mandelbrot_config:& MandelbrotConfig, save_option:&SaveOption) ->(image::RgbaImage,u64, u64){

    let (img_width,_)= get_empty_image_size((*save_option).scale_factor as f64/(*save_option).preview_downscale);
    let sub=img_width as u64/count;
    let new=mandelbrot_config.size_ratio.len()-1;
    let big_img_size=mandelbrot_config.size_ratio[new].2 * img_width as f64;

    let cx_min = -2f64;
    let cx_max = 1f64;
    let cy_min = -1.5f64;
    let cy_max = 1.5f64;
    let scale_x = (cx_max - cx_min) / big_img_size as f64;
    let scale_y = (cy_max - cy_min) / big_img_size as f64;

    let mut img_buf = image::RgbaImage::new(sub as u32, sub as u32);
    for x in min_x..min_x+sub{
        for y in min_y..min_y+sub {
            let cx = cx_min + x as f64 * scale_x;
            let cy = cy_min + y as f64 * scale_y;

            let c = num_complex::Complex::new(cx, cy);
            let mut z = num_complex::Complex::new(0f64, 0f64);

            let mut i = 0;
            for t in 0.. mandelbrot_config.max_iteration {
                if z.norm() > mandelbrot_config.max_z {
                    break;
                }
                z = z * z + c;
                i = t;
            }

            let color=if mandelbrot_config.coloring_type==1 {
                z_based_coloring(mandelbrot_config, z.norm().powf(2.0))
            }else {
                mandelbrot_config.color_palette[i as usize]
            };

            img_buf.put_pixel((x-min_x) as u32,(y-min_y) as u32,image::Rgba([color.r,color.g,color.b,255]));
        }
    }
   (img_buf,min_x,min_y)
}

fn create_foreground_mt(mandelbrot_config:& MandelbrotConfig, save_option: &SaveOption) -> ImageBuffer<Rgba<u8>,Vec<u8>>{

    let nthreads=*super::N_THREADS as u64;
    assert_eq!(((nthreads as f64).powf(0.5) as i32).pow(2),nthreads as i32);
    let mut children = vec![];

    let (img_width,_)= get_empty_image_size((*save_option).scale_factor as f64/(*save_option).preview_downscale);

    let new=mandelbrot_config.size_ratio.len()-1;
    let (min_x, min_y)=((mandelbrot_config.size_ratio[new].0 * img_width as f64) as u64,(mandelbrot_config.size_ratio[new].1 * img_width as f64) as u64);

    let mut img_buf = <ImageBuffer<Rgba<u8>, _>>::new(img_width, img_width);

    for i in 0..nthreads as u64 {
        let mandelbrot_config_temp=mandelbrot_config.clone();
        let save_option_temp=save_option.clone();
        let nthreads_sqrt=(nthreads as f64).powf(0.5) as u64;
        let sub=img_width as u64/nthreads_sqrt ;
        let min_x_temp=min_x+sub*(i%nthreads_sqrt);
        let min_y_temp=min_y+sub*(i/nthreads_sqrt);
        children.push(thread::spawn(move || {
            create_foreground_thread( min_x_temp, min_y_temp, nthreads_sqrt,  &mandelbrot_config_temp, &save_option_temp)
        }));
    }
    for child in children {
        let (img,mx,my) = child.join().unwrap();
        let _ = img_buf.copy_from(&img, (mx-min_x) as u32, (my-min_y) as u32);
    }
    img_buf
}

fn create_foreground_st(mandelbrot_config:& MandelbrotConfig, save_option: &SaveOption) -> ImageBuffer<Rgba<u8>,Vec<u8>> {
    let (img_width,_)= get_empty_image_size((*save_option).scale_factor as f64/(*save_option).preview_downscale);
    let new=mandelbrot_config.size_ratio.len()-1;
    let (min_x, min_y)=((mandelbrot_config.size_ratio[new].0 * img_width as f64) as u64,(mandelbrot_config.size_ratio[new].1 * img_width as f64) as u64);
    let (img_buf,_,_)= create_foreground_thread(min_x, min_y, 1,  mandelbrot_config, save_option);
    img_buf
}

fn create_foreground(mandelbrot_config:& MandelbrotConfig, save_option: &SaveOption) ->ImageBuffer<Rgba<u8>,Vec<u8>>{
    if (*save_option).enable_multithreading {
        create_foreground_mt(mandelbrot_config,save_option)
    }else {
        create_foreground_st(mandelbrot_config, save_option)
    }
}

fn z_based_coloring(mandelbrot_config:& MandelbrotConfig, Z_norm_2: f64) ->ColorRGB{
    let V= Z_norm_2.ln();
    let K=  mandelbrot_config.color_type_2_coff[0];
    let x= V.ln()*K;
    let (a,b,c)=(mandelbrot_config.color_type_2_coff[1],mandelbrot_config.color_type_2_coff[2],mandelbrot_config.color_type_2_coff[3]);
    let R=255.0*(1.0+(a*x).cos())/2.0;
    let G=255.0*(1.0+(b*x).cos())/2.0;
    let B=255.0*(1.0+(c*x).cos())/2.0;
    ColorRGB{
        r: R as u8,
        g: G as u8,
        b: B as u8,
        a: 255
    }
}

fn add_selector(mut img_buf: RgbaImage,mandelbrot_config:&mut MandelbrotConfig, save_option:&SaveOption) -> RgbaImage{
    if (*save_option).scale_factor>1 {
        return img_buf
    }
    let (img_width,img_height)= img_buf.dimensions();

    let x_selector= if mandelbrot_config.min_x_percentage<0.0||mandelbrot_config.min_x_percentage>100.0 {
        img_width+1
    }else {
        ((img_width as f64-(img_width as f64/mandelbrot_config.scalar))*mandelbrot_config.min_x_percentage/100.0) as u32
    };
    let y_selector= if  mandelbrot_config.min_y_percentage<0.0||mandelbrot_config.min_y_percentage>100.0 {
        img_height+1
    }else {
        ((img_height as f64-(img_height as f64/mandelbrot_config.scalar))*mandelbrot_config.min_y_percentage/100.0) as u32
    };

    for x in 0..img_width {
        for y in 0..img_height {
            if x==x_selector||y==y_selector{
                if mandelbrot_config.show_selector {
                    img_buf.put_pixel(x, y, Rgba([255, 0, 0, 255]));
                }
            }else if x >= x_selector && y >= y_selector && x <= x_selector + (img_width as f64/mandelbrot_config.scalar) as u32 && y<= y_selector + (img_height as f64/mandelbrot_config.scalar) as u32 {
                if mandelbrot_config.show_zoom {
                    let pixel = img_buf.get_pixel(x, y);
                    let image::Rgba(data) = *pixel;
                    let mut hsl = rgb_hsl(&ColorRGB {
                        r: data[0],
                        g: data[1],
                        b: data[2],
                        a: data[3]
                    });
                    hsl = hsl.saturate(0.2).lighten(0.2);
                    let new_rgb = hsl_rgb(&hsl);
                    img_buf.put_pixel(x, y, Rgba([new_rgb.r, new_rgb.g, new_rgb.b, new_rgb.a]));
                }
            }
        }
    }
    img_buf
}

pub fn gen_mandelbrot(mandelbrot_config:&mut MandelbrotConfig, save_option:&SaveOption) -> DynamicImage  {

    let mut img_buf_foreground = if save_option.scale_factor==1 {

        if mandelbrot_config.cache_img.len() == 0  {
            //mandelbrot_config.cache_img.clear();
            let img_buf_foreground = create_foreground(mandelbrot_config, save_option);

            mandelbrot_config.cache_img.push(img_buf_foreground);
        }
        mandelbrot_config.cache_img[0].clone()
    }else{
        create_foreground(mandelbrot_config, save_option)
    };

    img_buf_foreground=add_selector(img_buf_foreground,mandelbrot_config,save_option);


    let mut dynamic_image =DynamicImage::ImageRgba8(img_buf_foreground);
    dynamic_image=dynamic_image.adjust_contrast(save_option.contrast);
    dynamic_image=dynamic_image.brighten(save_option.brightness);
    save_image(&dynamic_image, save_option, "Mandelbrot");
    dynamic_image

}