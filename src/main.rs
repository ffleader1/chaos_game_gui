#![windows_subsystem = "windows"]
#![allow(non_snake_case)]
#![allow(unused_imports)]
//#[macro_use(lazy_static)]
extern crate num_cpus;
extern crate lazy_static;


mod controls;
mod pkg_chaos_game;
mod pkg_julia;
mod pkg_dragon_curve;
mod img_generator;
mod pkg_barnsley_fern;
mod pkg_mandelbrot;
mod tooltips;
mod io;

use fltk::{
    app::{App, AppScheme, channel,redraw},
    button::*,
    text::{TextBuffer,TextDisplay},
    dialog:: {message},
    input::{IntInput,FloatInput},
    frame::Frame,
    valuator::{ScrollbarType,Slider},
    group::{Tabs,Group},
    menu::{Choice},
    prelude::*,
    window::*,
};

use open;
use std::{panic, thread, time};
use std::path::{Path,PathBuf};
use image::*;
use std::collections::VecDeque;
use chrono::prelude::*;
use ordinal::Ordinal;
use std::time::{Instant};
use std::char;
use indexmap::{IndexMap};
use std::env;
use std::fs::create_dir;

use crate::img_generator::{SaveOption, ColorRGB, UniConfig, generate_color_palette_hsv, gen_icon};
use crate::pkg_chaos_game::ChaosGameConfig;
use crate::pkg_julia::JuliaConfig;
use crate::pkg_dragon_curve::DragonConfig;
use crate::pkg_barnsley_fern::BarnsleyFernConfig;
use crate::pkg_mandelbrot::MandelbrotConfig;

use lazy_static::lazy_static;



const WIDTH: u32=320;
const HEIGHT: u32=320;


lazy_static! {
    static ref  N_THREADS:u32= {
    let n_cpus=num_cpus::get() as f64;
    if (n_cpus.powf(0.5) as i32).pow(2) == n_cpus as i32 {n_cpus as u32}
    else {((n_cpus.powf(0.5)) as u32 +1).pow(2)}

    } ;
    static ref COLOR_DATA_FOLDER_PATH:PathBuf={let path = Path::new(".");let folder_path = path.join("color_data");folder_path};
    static ref CONFIG_FOLDER_PATH:PathBuf={let path = Path::new(".");let folder_path = path.join("config");folder_path};
    static ref COLOR_DATA_FILE_PATH:Vec<PathBuf> ={
     let mut file_path:Vec<PathBuf>=Vec::new();
     file_path.push(COLOR_DATA_FOLDER_PATH.join("chaos_game.json"));
     file_path.push(COLOR_DATA_FOLDER_PATH.join("julia.json"));
     file_path.push(COLOR_DATA_FOLDER_PATH.join("dragon_curve.json"));
     file_path
    };
    static ref CONFIG_FILE_PATH:Vec<PathBuf> ={
     let mut file_path:Vec<PathBuf>=Vec::new();
     file_path.push(CONFIG_FOLDER_PATH.join("chaos_game.json"));
     file_path.push(CONFIG_FOLDER_PATH.join("julia.json"));
     file_path.push(CONFIG_FOLDER_PATH.join("dragon_curve.json"));
     file_path.push(CONFIG_FOLDER_PATH.join("barnsley_fern.json"));
     file_path.push(CONFIG_FOLDER_PATH.join("mandelbrot.json"));
     file_path
    };
    // static ref PAID:bool={
    //     let mut paid=true;
    //     for fp in CONFIG_FILE_PATH.iter(){
    //         if !fp.exists(){
    //             paid=false;
    //             break;
    //         }
    //
    //     }
    //     paid
    // };

}


#[derive(Debug, Copy, Clone)]
enum Message {

    GeneralConfigChange,

    ChaosGameInputScalarChange,
    ChaosGameSliderScalarChange,
    ChaosGameVerticesChange,
    ChaosGameInputJumpChange,
    ChaosGameSliderJumpChange,
    ChaosGameChoiceConditionChange,
    ChaosGameIntModifier1Change,
    ChaosGameColorRandomSeedChange,
    ChaosGameCustomColorUsageChange,
    ChaosGameEditColor,
    ChaosGameLoadColor,
    ChaosGameBackgroundChange,

    JuliaInputScalarChange,
    JuliaSliderScalarChange,
    JuliaInputCRealChange,
    JuliaSliderCRealChange,
    JuliaInputCImaginaryChange,
    JuliaSliderCImaginaryChange,
    JuliaInputZExponentChange,
    JuliaSliderZExponentChange,
    JuliaInputColorChange,
    JuliaSliderColorChange,
    JuliaBackgroundChange,

    DragonChoiceSelectionChange,
    DragonCheckEnableChange,
    DragonConfigChange,
    DragonDirectionChange,
    DragonInputNoIterationChange,
    DragonSliderNoIterationChange,
    DragonInputLineWidthChange,
    DragonSliderLineWidthChange,
    DragonColorRandomSeedChange,
    DragonCustomColorUsageChange,
    DragonEditColor,
    DragonLoadColor,
    DragonBackgroundChange,

    FernChoiceSelectionChange,
    FernCheckEnableChange,
    FernInputCoefficientChange,
    FernSliderCoefficientChange,
    FernInputProbabilityChange,
    FernSliderProbabilityChange,
    FernInputSampleChange,
    FernSliderSampleChange,
    FernInputColorChange,
    FernSliderColorChange,
    FernCheckWeightAdjacentChange,
    FernBackgroundChange,

    MandelbrotInputScalarChange,
    MandelbrotSliderScalarChange,
    MandelbrotInputXLocChange,
    MandelbrotSliderXLocChange,
    MandelbrotInputYLocChange,
    MandelbrotSliderYLocChange,
    MandelbrotShowChange,
    MandelbrotZoomIn,
    MandelbrotZoomOut,
    MandelbrotInputIterationChange,
    MandelbrotSliderIterationChange,
    MandelbrotInputZChange,
    MandelbrotSliderZChange,
    MandelbrotColoringTypeChange,
    MandelbrotInputColor1Change,
    MandelbrotSliderColor1Change,
    MandelbrotInputColor2Change,
    MandelbrotSliderColor2Change,

    BrightnessInpChange,
    BrightnessSliChange,
    ContrastInpChange,
    ContrastSliChange,
    SaveImage,
    OpenLocation,
    LoadConfig,
    ResetConfig,
    SaveConfig,
}

const VERSION: Option<&'static str> = option_env!("CARGO_PKG_VERSION");

//must allow : or / for path character even though not legal for an actual file name
//on my Windows 10 machine...
//embedded / will display error if it is an incorrect path
//embedded : will truncate everything after it and create a log file
//           named with the more than two characters before it
//1 character before : "The system cannot find the path specified. (os error 3)"
//x characters before : and no characters after it
//                      "The filename, directory name, or volume label syntax is incorrect. (os error 123)"


//noinspection DuplicatedCode
fn main() {


    panic::set_hook(Box::new(|panic_info| { message(200, 200, &panic_info.to_string());}));
    let myapp = App::default().with_scheme(AppScheme::Plastic);
    let v_margin:i32 = 10;
    let h_margin:i32 = 5;
    let row_height = 20;
    let mut row_y : Vec<i32>=Vec::new();
    row_y.push(v_margin);
    for i in 1..32 {
        row_y.push(row_y[i-1]+row_height+ v_margin)
    }


    let mut w = DoubleWindow::default().with_size(1130, 690).center_screen().with_label(&format!("CHAOS GAME VERSION {}", VERSION.unwrap_or("unknown")));
    let mut title = controls::Label::new(h_margin, row_y[1], 750, row_height, &format!("VERSION {}", VERSION.unwrap_or("unknown")), w.color());
    title.set_text_color(Color::Red);
    w.set_icon(Some(get_icon()));
    let mut user_name:String=unidecode::unidecode(&whoami::realname());
    user_name.push_str(&unidecode::unidecode(&whoami::distro()));
    let tooltip_map=tooltips::gen_tooltip_hashmap();
    let mut random_state_vec:Vec<u64>=Vec::new();
    let max_random_state=3;
    for i in 0..max_random_state {
        let mut random_state=0_u64;
        for c in user_name.chars() {
            let n_c=c as u64;
            if n_c % (1+i as u64)==0 {
                random_state += c as u64;
            }
        }
        random_state_vec.push(random_state);
    }

    let mut tab = Tabs::new(h_margin, row_y[2], 730, row_y[13], "");

    let mut tab_label:Vec<String>=Vec::new();
    tab_label.push("Chaos Game".to_string());
    tab_label.push("Julia".to_string());
    tab_label.push("Dragon Curve".to_string());
    tab_label.push("Barnsley Fern".to_string());
    tab_label.push("Mandelbrot".to_string());

    let mut grp:Vec<Group>=Vec::new();

    let mut current_tab_no:usize=0;

    //Tab Chaos Game
    grp.push(Group::new(h_margin, row_y[3], 730, row_y[13], &tab_label[0]));
    //<editor-fold desc="Tab Chaos Game">
    let (chs_gm_default_scalar_value, chs_gm_original_scalar_lower, chs_gm_original_scalar_upper, chs_gm_scalar_precision):(f64, f64, f64, i32) = (3.0, 2.1, 8.0, 1);

    let mut chs_gm_inp_scalar = FloatInput::new(60, row_y[4], 30, row_height, "Scale: ");
    chs_gm_inp_scalar.set_value(&format!("{}", chs_gm_default_scalar_value));
    chs_gm_inp_scalar.set_tooltip(&get_tooltips(&tooltip_map,&"chs_gm_inp_scalar"));


    let mut chs_gm_sli_scalar = Slider::new(240, row_y[4], 245, row_height, "");
    chs_gm_sli_scalar.set_type(ScrollbarType::Horizontal);
    chs_gm_sli_scalar.set_bounds(chs_gm_original_scalar_lower, chs_gm_original_scalar_upper);
    chs_gm_sli_scalar.set_precision(chs_gm_scalar_precision);
    chs_gm_sli_scalar.set_value(chs_gm_default_scalar_value);
    chs_gm_sli_scalar.set_tooltip(&get_tooltips(&tooltip_map,&"chs_gm_sli_scalar"));


    let mut chs_gm_inp_no_vertices = IntInput::new(100, row_y[5], 30, row_height, "No. Vertices: ");
    let (chs_gm_min_vertices,chs_gm_max_vertices)=(3_i32,9_i32);
    chs_gm_inp_no_vertices.set_value(&format!("{}",chs_gm_min_vertices));
    chs_gm_inp_no_vertices.set_tooltip(&get_tooltips(&tooltip_map,&"chs_gm_inp_no_vertices"));

    let mut chs_gm_check_incl_center = CheckButton::default().with_pos(368, row_y[5])
        .with_size(20, row_height).with_align(Align::Left).with_label("Include Center");
    chs_gm_check_incl_center.set_checked(false);
    chs_gm_check_incl_center.set_tooltip(&get_tooltips(&tooltip_map,&"chs_gm_check_incl_center"));

    let mut chs_gm_check_incl_mid = CheckButton::default().with_pos(600, row_y[5])
        .with_size(20, row_height).with_align(Align::Left).with_label("Include Mid Points");
    chs_gm_check_incl_mid.set_checked(false);
    chs_gm_check_incl_mid.set_tooltip(&get_tooltips(&tooltip_map,&"chs_gm_check_incl_mid"));


    let (chs_gm_original_jump_ratio, chs_gm_original_jump_lower, chs_gm_original_jump_upper, chs_gm_jump_precision):(f64, f64, f64, i32) = (0.5, 0.1, 2.0, 3);

    let mut chs_gm_inp_jump_ratio = FloatInput::new(90, row_y[6], 45, row_height, "Jump ratio: ");
    chs_gm_inp_jump_ratio.set_value(&format!("{}", chs_gm_original_jump_ratio));
    chs_gm_inp_jump_ratio.set_tooltip(&get_tooltips(&tooltip_map,&"chs_gm_inp_jump_ratio"));

    let mut chs_gm_sli_jump_ratio = Slider::new(140, row_y[6], 245, row_height, "");
    chs_gm_sli_jump_ratio.set_type(ScrollbarType::Horizontal);
    chs_gm_sli_jump_ratio.set_bounds(chs_gm_original_jump_lower, chs_gm_original_jump_upper);
    chs_gm_sli_jump_ratio.set_precision(chs_gm_jump_precision);
    chs_gm_sli_jump_ratio.set_value(chs_gm_original_jump_ratio);
    chs_gm_sli_jump_ratio.set_tooltip(&get_tooltips(&tooltip_map,&"chs_gm_sli_jump_ratio"));

    let mut chs_gm_ch_condition = Choice::new(84, row_y[7], 150, row_height, "Condition :");
    chs_gm_ch_condition.add_choice("None");
    chs_gm_ch_condition.add_choice("Avoid Iteration");
    chs_gm_ch_condition.add_choice("Ext. Recurrence");
    chs_gm_ch_condition.add_choice("Avoid Neighbor");
    chs_gm_ch_condition.set_value(0);
    chs_gm_ch_condition.set_tooltip(&get_tooltips(&tooltip_map,&"chs_gm_ch_condition"));

    let mut chs_gm_inp_modifier_i = IntInput::new(450, row_y[7], 30, row_height, "");
    chs_gm_inp_modifier_i.set_label("No. Prev. Vertices to Avoid: ");
    chs_gm_inp_modifier_i.hide();
    chs_gm_inp_modifier_i.set_tooltip(&get_tooltips(&tooltip_map,&"chs_gm_inp_modifier_i"));

    let (mut chs_gm_color_palette_random,mut chs_gm_color_palette_custom,_)=color_palette_rgb_handler(chs_gm_max_vertices*2+1, random_state_vec[0].clone(), 0);

    let _chs_gm_lab_color_config_ = controls::Label::new(12, row_y[8], 250, row_height, &format!("Color Config: "), w.color());

    let mut chs_gm_inp_color_rand_seed = IntInput::new(298, row_y[8], 70, row_height, "Color Random Seed: ");
    chs_gm_inp_color_rand_seed.set_value(&format!("{}", random_state_vec[0]));
    chs_gm_inp_color_rand_seed.set_tooltip(&get_tooltips(&tooltip_map,&"rgb_inp_color_rand_seed"));


    let mut chs_gm_check_custom_color = CheckButton::default().with_pos(283, row_y[9])
        .with_size(20, row_height).with_align(Align::Left).with_label("Use Custom Colors");
    chs_gm_check_custom_color.set_tooltip(&get_tooltips(&tooltip_map,&"rgb_check_custom_color"));

    let mut chs_gm_check_transparent_color = CheckButton::default().with_pos(283, row_y[10])
        .with_size(20, row_height).with_align(Align::Left).with_label("Add Transparency");
    chs_gm_check_transparent_color.set_checked(false);
    chs_gm_check_transparent_color.set_tooltip(&get_tooltips(&tooltip_map,&"rgb_check_transparent_color"));

    let mut chs_gm_but_edit_color = Button::default().with_pos(420, row_y[9]).with_size(60, row_height).with_label("Edit");

    let mut chs_gm_but_load_color = Button::default().with_pos(495, row_y[9]).with_size(60, row_height).with_label("Load");

    chs_gm_but_edit_color.deactivate();
    chs_gm_but_load_color.deactivate();
    chs_gm_but_edit_color.set_tooltip(&get_tooltips(&tooltip_map,&"rgb_but_edit_color"));
    chs_gm_but_load_color.set_tooltip(&get_tooltips(&tooltip_map,&"rgb_but_load_color"));

    let mut chs_gm_ch_background = Choice::new(105, row_y[11], 120, row_height, "Background : ");
    chs_gm_ch_background.add_choice("Dark");
    chs_gm_ch_background.add_choice("Colorful");
    chs_gm_ch_background.add_choice("Transparent");
    chs_gm_ch_background.set_value(1);
    chs_gm_ch_background.set_tooltip(&get_tooltips(&tooltip_map,&"chs_gm_ch_background"));

    let mut chaos_game_config:pkg_chaos_game::ChaosGameConfig = ChaosGameConfig {
        scalar: chs_gm_default_scalar_value,
        no_point: chs_gm_inp_no_vertices.value().parse::<i32>().unwrap_or(chs_gm_min_vertices),
        include_mid_points: chs_gm_check_incl_mid.is_checked(),
        include_center: chs_gm_check_incl_center.is_checked(),
        distance_ratio: chs_gm_original_jump_ratio,
        condition_id: 0,
        condition_modifier_i: 0,
        color_palette: chs_gm_color_palette_random.clone(),
        add_transparency: chs_gm_check_transparent_color.is_checked(),
        background_type: 1,
    };
    //io::save_config(&chaos_game_config);
    // io::load_config(&mut chaos_game_config);
    // println!("{:?}",chaos_game_config);
    grp[0].end();
    //</editor-fold>


    //Tab Julia
    grp.push(Group::new(h_margin, row_y[3], 730, row_y[13], &tab_label[1]));
    //<editor-fold desc="Tab Julia">

    let (julia_original_scalar_value, julia_original_scalar_lower, julia_original_scalar_upper, julia_scalar_precision):(f64, f64, f64, i32) = (3.0, 1.5, 16.0, 1);

    let mut jul_inp_scalar = FloatInput::new(60, row_y[4], 30, row_height, "Scale: ");
    jul_inp_scalar.set_value(&format!("{}", julia_original_scalar_value));
    jul_inp_scalar.set_tooltip(&get_tooltips(&tooltip_map,&"jul_inp_scalar"));

    let mut jul_sli_scalar = Slider::new(240, row_y[4], 245, row_height, "");
    jul_sli_scalar.set_type(ScrollbarType::Horizontal);
    jul_sli_scalar.set_bounds(julia_original_scalar_lower, julia_original_scalar_upper);
    jul_sli_scalar.set_precision(julia_scalar_precision);
    jul_sli_scalar.set_value(julia_original_scalar_value);
    jul_sli_scalar.set_tooltip(&get_tooltips(&tooltip_map,&"jul_sli_scalar"));


    let (julia_original_c_real_value, julia_original_c_real_lower, julia_original_c_real_upper, julia_c_real_precision):(f64, f64, f64, i32) = (-0.4, -1.628, 1.628, 2);

    let mut jul_inp_c_real = FloatInput::new(107, row_y[5], 70, row_height, "C - Real Part: ");
    jul_inp_c_real.set_value(&format!("{}",julia_original_c_real_value));
    jul_inp_c_real.set_tooltip(&get_tooltips(&tooltip_map,&"jul_inp_c_real"));

    let mut jul_sli_c_real = Slider::new(240, row_y[5], 245, row_height, "");
    jul_sli_c_real.set_type(ScrollbarType::Horizontal);
    jul_sli_c_real.set_bounds(julia_original_c_real_lower, julia_original_c_real_upper);
    jul_sli_c_real.set_precision(julia_c_real_precision);
    jul_sli_c_real.set_value(julia_original_c_real_value);
    jul_sli_c_real.set_tooltip(&get_tooltips(&tooltip_map,&"jul_sli_c_real"));


    let (julia_original_c_imaginary_value, julia_original_c_imaginary_lower, julia_original_c_imaginary_upper, julia_c_imaginary_precision):(f64, f64, f64, i32) = (0.6, -1.628, 1.628, 2);

    let mut jul_inp_c_imaginary = FloatInput::new(139, row_y[6], 70, row_height, "C - Imaginary Part: ");
    jul_inp_c_imaginary.set_value(&format!("{}",julia_original_c_imaginary_value));
    jul_inp_c_imaginary.set_tooltip(&get_tooltips(&tooltip_map,&"jul_inp_c_imaginary"));

    let mut jul_sli_c_imaginary = Slider::new(240, row_y[6], 245, row_height, "");
    jul_sli_c_imaginary.set_type(ScrollbarType::Horizontal);
    jul_sli_c_imaginary.set_bounds(julia_original_c_imaginary_lower, julia_original_c_imaginary_upper);
    jul_sli_c_imaginary.set_precision(julia_c_imaginary_precision);
    jul_sli_c_imaginary.set_value(julia_original_c_imaginary_value);
    jul_sli_c_imaginary.set_tooltip(&get_tooltips(&tooltip_map,&"jul_sli_c_imaginary"));

    let (julia_original_z_pow_value, julia_original_z_pow_lower, julia_original_z_pow_upper, julia_z_pow_precision):(f64, f64, f64, i32) = (2.0, 1.1, 7.0, 2);

    let mut jul_inp_z_pow = FloatInput::new(106, row_y[7], 70, row_height, "Z - Exponent: ");
    jul_inp_z_pow.set_value(&format!("{}",julia_original_z_pow_value));
    jul_inp_z_pow.set_tooltip(&get_tooltips(&tooltip_map,&"jul_inp_z_pow"));

    let mut jul_sli_z_pow = Slider::new(240, row_y[7], 245, row_height, "");
    jul_sli_z_pow.set_type(ScrollbarType::Horizontal);
    jul_sli_z_pow.set_bounds(julia_original_z_pow_lower, julia_original_z_pow_upper);
    jul_sli_z_pow.set_precision(julia_z_pow_precision);
    jul_sli_z_pow.set_value(julia_original_z_pow_value);
    jul_sli_z_pow.set_tooltip(&get_tooltips(&tooltip_map,&"jul_sli_z_pow"));

    let _jul_lab_color_config_ = controls::Label::new(12, row_y[8], 250, row_height, &format!("Color Config: "), w.color());


    let mut _jul_lab_hue_ = controls::Label::new(150, row_y[8], 250, row_height, &format!("Hue "), w.color());

    let mut _jul_lab_sat_ = controls::Label::new(150, row_y[9], 250, row_height, &format!("Saturation "), w.color());

    let mut _jul_lab_val_ = controls::Label::new(150, row_y[10], 250, row_height, &format!("Value "), w.color());

    let mut jul_inp_color_config_vec:Vec<FloatInput>=Vec::new();
    jul_inp_color_config_vec.push( FloatInput::new(286, row_y[8], 50, row_height, "From: "));
    jul_inp_color_config_vec.push( FloatInput::new(536, row_y[8], 50, row_height, "To: "));
    jul_inp_color_config_vec.push( FloatInput::new(286, row_y[9], 50, row_height, "From: "));
    jul_inp_color_config_vec.push( FloatInput::new(536, row_y[9], 50, row_height, "To: "));
    jul_inp_color_config_vec.push( FloatInput::new(286, row_y[10], 50, row_height, "From: "));
    jul_inp_color_config_vec.push( FloatInput::new(536, row_y[10], 50, row_height, "To: "));

    jul_inp_color_config_vec[0].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_hue_from"));
    jul_inp_color_config_vec[1].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_hue_to"));
    jul_inp_color_config_vec[2].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_sat_from"));
    jul_inp_color_config_vec[3].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_sat_to"));
    jul_inp_color_config_vec[4].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_val_from"));
    jul_inp_color_config_vec[5].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_val_to"));

    let mut jul_sli_color_config_vec:Vec<Slider>=Vec::new();
    jul_sli_color_config_vec.push( Slider::new(350, row_y[8], 120, row_height, ""));
    jul_sli_color_config_vec.push( Slider::new(600, row_y[8], 120, row_height, ""));
    jul_sli_color_config_vec.push( Slider::new(350, row_y[9], 120, row_height, ""));
    jul_sli_color_config_vec.push( Slider::new(600, row_y[9], 120, row_height, ""));
    jul_sli_color_config_vec.push( Slider::new(350, row_y[10], 120, row_height, ""));
    jul_sli_color_config_vec.push( Slider::new(600, row_y[10], 120, row_height, ""));

    jul_sli_color_config_vec[0].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_hue_from"));
    jul_sli_color_config_vec[1].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_hue_to"));
    jul_sli_color_config_vec[2].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_sat_from"));
    jul_sli_color_config_vec[3].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_sat_to"));
    jul_sli_color_config_vec[4].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_val_from"));
    jul_sli_color_config_vec[5].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_val_to"));


    let jul_color_config_original_vec=vec![105.0,130.0,0.3,0.8,0.3,0.8];
    let jul_color_config_lower_vec=vec![0.0,0.0,0.0,0.0,0.0,0.0];
    let jul_color_config_upper_vec=vec![360.0,360.0,1.0,1.0,1.0,1.0];
    let jul_color_config_precision_vec=vec![1,1,2,2,2,2];


    for i in 0..6 {
        jul_inp_color_config_vec[i].set_value(&format!("{}", jul_color_config_original_vec[i]));
        jul_sli_color_config_vec[i].set_type(ScrollbarType::Horizontal);
        jul_sli_color_config_vec[i].set_bounds(jul_color_config_lower_vec[i],  jul_color_config_upper_vec[i]);
        jul_sli_color_config_vec[i].set_precision(jul_color_config_precision_vec[i]);
        jul_sli_color_config_vec[i].set_value(jul_color_config_original_vec[i]);
    }

    let jul_no_color=256;

    let mut jul_ch_background = Choice::new(105, row_y[11], 120, row_height, "Background : ");
    jul_ch_background.add_choice("Dark");
    jul_ch_background.add_choice("Pattern");
    jul_ch_background.add_choice("Transparent");
    jul_ch_background.set_value(1);
    jul_ch_background.set_tooltip(&get_tooltips(&tooltip_map,&"jul_ch_background"));

    let mut julia_config:pkg_julia::JuliaConfig = JuliaConfig{
        scalar: julia_original_scalar_value.clone(),
        c_real: julia_original_c_real_value.clone(),
        c_imaginary: julia_original_c_imaginary_value.clone(),
        z_pow: julia_original_z_pow_value.clone(),
        color_palette: img_generator::generate_color_palette_hsv(&jul_no_color, &jul_color_config_original_vec),
        background_type:jul_ch_background.value(),
    };

    grp[1].end();
    //</editor-fold>

    //Tab Dragon Curve
    grp.push(Group::new(h_margin, row_y[3], 730, row_y[13], &tab_label[2]));
    //<editor-fold desc="Tab Dragon Curve">
    let dragon_max_no_drg = 6;
    let mut dragon_current_select =0_usize;

    let mut dragon_choice_current = Choice::new(119, row_y[4], 60, row_height, "Select Dragon : ");
    for i in 0..dragon_max_no_drg {
        dragon_choice_current.add_choice(&format!("{}", Ordinal(i+1).to_string()));
    };
    dragon_choice_current.set_tooltip(&get_tooltips(&tooltip_map,&"dragon_choice_current"));
    dragon_choice_current.set_value(dragon_current_select as i32);


    let mut dragon_check_enabled_vec:Vec<CheckButton>=Vec::new();
    let mut dragon_ch_position_1_vec:Vec<Choice>=Vec::new();
    let mut dragon_ch_position_2_vec:Vec<Choice>=Vec::new();
    let mut dragon_ch_position_3_vec:Vec<Choice>=Vec::new();
    let mut dragon_ch_rotate_vec:Vec<Choice>=Vec::new();
    let mut dragon_ch_flip_vec:Vec<Choice>=Vec::new();
    let mut dragon_inp_no_iter_vec:Vec<IntInput>=Vec::new();
    let mut dragon_sli_no_iter_vec:Vec<Slider>=Vec::new();
    let mut dragon_inp_stroke_width_vec:Vec<IntInput>=Vec::new();
    let mut dragon_sli_stroke_width_vec:Vec<Slider>=Vec::new();

    let mut dragon_original_enabled_vec:Vec<bool>=Vec::new();
    let mut dragon_original_position_1_vec:Vec<i32>=Vec::new();
    let mut dragon_original_position_2_vec:Vec<i32>=Vec::new();
    let mut dragon_original_position_3_vec:Vec<i32>=Vec::new();
    let mut dragon_original_rotate_vec:Vec<i32>=Vec::new();
    let mut dragon_original_flip_vec:Vec<i32>=Vec::new();
    let mut dragon_original_no_iter_vec:Vec<i32>=Vec::new();
    let mut dragon_original_stroke_width_vec:Vec<f64>=Vec::new();

    let (dragon_original_no_iter, dragon_no_iter_lower, dragon_no_iter_upper, dragon_no_iter_precision):(f64, f64, f64, i32) = (11.0, 8.0, 14.0, 0);
    let (dragon_original_stroke_width, dragon_stroke_width_lower, dragon_stroke_width_upper, dragon_stroke_width_precision):(f64, f64, f64, i32) = (1.0, 1.0, 4.0, 0);

    let dragon_pre_activate=2;

    for i in 0..dragon_max_no_drg {
        dragon_check_enabled_vec.push(CheckButton::default().with_pos(184, row_y[5])
            .with_size(20, row_height).with_align(Align::Left).with_label(&format!("Enable the {} Dragon",Ordinal(i+1).to_string())));
        dragon_original_enabled_vec.push(if i<dragon_pre_activate {
            true
        }else {
            false
        });
        dragon_check_enabled_vec[i].set_checked(dragon_original_enabled_vec[i]);
        dragon_check_enabled_vec[i].set_tooltip(&get_tooltips(&tooltip_map,&"dragon_check_enabled"));

        dragon_ch_position_1_vec.push(Choice::new(152, row_y[6], 90, row_height, &format!("{} Dragon starts ", Ordinal(i+1).to_string())));
        dragon_ch_position_1_vec[i].add_choice("same as");
        dragon_ch_position_1_vec[i].add_choice("above");
        dragon_ch_position_1_vec[i].add_choice("under");
        dragon_ch_position_1_vec[i].add_choice("opposite");
        dragon_original_position_1_vec.push(0);
        dragon_ch_position_1_vec[i].set_value(dragon_original_position_1_vec[i]);
        dragon_ch_position_1_vec[i].set_tooltip(&get_tooltips(&tooltip_map,&"dragon_ch_position"));


        dragon_ch_position_2_vec.push(Choice::new(277, row_y[6], 60, row_height, " the "));
        dragon_ch_position_2_vec[i].add_choice("root");
        dragon_ch_position_2_vec[i].add_choice("head");
        dragon_original_position_2_vec.push(0);
        dragon_ch_position_2_vec[i].set_value(dragon_original_position_2_vec[i]);
        dragon_ch_position_2_vec[i].set_tooltip(&get_tooltips(&tooltip_map,&"dragon_ch_position"));


        dragon_ch_position_3_vec.push(Choice::new(388, row_y[6], 60, row_height, " of the "));
        for j in 0..dragon_max_no_drg {
            dragon_ch_position_3_vec[i].add_choice(&format!("{}", Ordinal(j+1).to_string()));

        }
        dragon_original_position_3_vec.push(0);
        dragon_ch_position_3_vec[i].set_value(dragon_original_position_3_vec[i]);
        dragon_ch_position_3_vec[i].set_tooltip(&get_tooltips(&tooltip_map,&"dragon_ch_position"));

        dragon_ch_rotate_vec.push(Choice::new(89, row_y[7], 60, row_height, &format!("Rotate: ", )));
        dragon_ch_rotate_vec[i].add_choice("0°");
        dragon_ch_rotate_vec[i].add_choice("90°");
        dragon_ch_rotate_vec[i].add_choice("180°");
        dragon_ch_rotate_vec[i].add_choice("270°");
        dragon_original_rotate_vec.push(0);
        dragon_ch_rotate_vec[i].set_value(dragon_original_rotate_vec[i]);
        dragon_ch_rotate_vec[i].set_tooltip(&get_tooltips(&tooltip_map,&"dragon_ch_rotate"));

        dragon_ch_flip_vec.push(Choice::new(205, row_y[7], 90, row_height, &format!("Flip: ", )));
        dragon_ch_flip_vec[i].add_choice("None");
        dragon_ch_flip_vec[i].add_choice("Vertical");
        dragon_ch_flip_vec[i].add_choice("Horizontal");
        dragon_ch_flip_vec[i].add_choice("Both");
        dragon_original_flip_vec.push(0);
        dragon_ch_flip_vec[i].set_value(dragon_original_flip_vec[i]);
        dragon_ch_flip_vec[i].set_tooltip(&get_tooltips(&tooltip_map,&"dragon_ch_flip"));


        dragon_inp_no_iter_vec.push( IntInput::new(233, row_y[8], 45, row_height, "No. iteration: 2 to the power of "));
        dragon_inp_no_iter_vec[i].set_value(&format!("{}", dragon_original_no_iter));
        dragon_original_no_iter_vec.push(dragon_original_no_iter as i32);
        dragon_inp_no_iter_vec[i].set_tooltip(&get_tooltips(&tooltip_map,&"dragon_inp_no_iter"));


        dragon_sli_no_iter_vec.push(Slider::new(281, row_y[8], 100, row_height, ""));
        dragon_sli_no_iter_vec[i].set_type(ScrollbarType::Horizontal);
        dragon_sli_no_iter_vec[i].set_bounds(dragon_no_iter_lower, dragon_no_iter_upper);
        dragon_sli_no_iter_vec[i].set_precision(dragon_no_iter_precision);
        dragon_sli_no_iter_vec[i].set_value(dragon_original_no_iter);
        dragon_sli_no_iter_vec[i].set_tooltip(&get_tooltips(&tooltip_map,&"dragon_sli_no_iter"));


        dragon_inp_stroke_width_vec.push( IntInput::new(495, row_y[8], 40, row_height, "Line width: "));
        dragon_inp_stroke_width_vec[i].set_value(&format!("{}", dragon_original_stroke_width));
        dragon_original_stroke_width_vec.push(dragon_original_stroke_width);
        dragon_inp_stroke_width_vec[i].set_tooltip(&get_tooltips(&tooltip_map,&"dragon_inp_stroke_width"));


        dragon_sli_stroke_width_vec.push(Slider::new(540, row_y[8], 100, row_height, ""));
        dragon_sli_stroke_width_vec[i].set_type(ScrollbarType::Horizontal);
        dragon_sli_stroke_width_vec[i].set_bounds(dragon_stroke_width_lower, dragon_stroke_width_upper);
        dragon_sli_stroke_width_vec[i].set_precision(dragon_stroke_width_precision);
        dragon_sli_stroke_width_vec[i].set_value(dragon_original_stroke_width);
        dragon_sli_stroke_width_vec[i].set_tooltip(&get_tooltips(&tooltip_map,&"dragon_sli_stroke_width"));


        if !dragon_check_enabled_vec[i].is_checked() {
            dragon_ch_position_1_vec[i].deactivate();
            dragon_ch_position_2_vec[i].deactivate();
            dragon_ch_position_3_vec[i].deactivate();
            dragon_ch_rotate_vec[i].deactivate();
            dragon_ch_flip_vec[i].deactivate();
            dragon_inp_no_iter_vec[i].deactivate();
            dragon_sli_no_iter_vec[i].deactivate();
            dragon_inp_stroke_width_vec[i].deactivate();
            dragon_sli_stroke_width_vec[i].deactivate();
        }
        if i!=dragon_current_select {
            dragon_check_enabled_vec[i].hide();
            dragon_ch_position_1_vec[i].hide();
            dragon_ch_position_2_vec[i].hide();
            dragon_ch_position_3_vec[i].hide();
            dragon_ch_rotate_vec[i].hide();
            dragon_ch_flip_vec[i].hide();
            dragon_inp_no_iter_vec[i].hide();
            dragon_sli_no_iter_vec[i].hide();
            dragon_inp_stroke_width_vec[i].hide();
            dragon_sli_stroke_width_vec[i].hide();
        }

    }

    {
        dragon_check_enabled_vec[0].deactivate();
        dragon_ch_position_1_vec[0].deactivate();
        dragon_ch_position_2_vec[0].deactivate();
        dragon_ch_position_3_vec[0].deactivate();

        dragon_ch_rotate_vec[1].set_value(2);
        dragon_original_rotate_vec[1]=2;
    }

    let mut dragon_redundant_label = controls::Label::new(455, row_y[6], 250, row_height, &format!("Dragon."), w.color());
    dragon_redundant_label.deactivate();
    for i in 0..dragon_max_no_drg {
        for j in 0..dragon_max_no_drg {
            if i<=j || !dragon_check_enabled_vec[j].is_checked() {
                if let Some(mut item) = dragon_ch_position_3_vec[i].find_item(&format!("{}", Ordinal(j + 1).to_string())) {
                    item.hide();
                }
            }
        }
    }

    let (mut dragon_color_palette_random,mut dragon_color_palette_custom,_)=color_palette_rgb_handler(dragon_max_no_drg as i32, random_state_vec[1].clone(), 2);

    let _dragon_lab_color_config_ = controls::Label::new(12, row_y[9], 250, row_height, &format!("Color Config: "), w.color());

    let mut dragon_inp_color_rand_seed = IntInput::new(298, row_y[9], 70, row_height, "Color Random Seed: ");
    dragon_inp_color_rand_seed.set_value(&format!("{}", random_state_vec[1]));
    dragon_inp_color_rand_seed.set_tooltip(&get_tooltips(&tooltip_map,&"rgb_inp_color_rand_seed"));


    let mut dragon_ck_custom_color = CheckButton::default().with_pos(283, row_y[10])
        .with_size(20, row_height).with_align(Align::Left).with_label("Use Custom Colors");
    dragon_ck_custom_color.set_tooltip(&get_tooltips(&tooltip_map,&"rgb_check_custom_color"));

    let mut dragon_but_edit_color = Button::default().with_pos(420, row_y[10]).with_size(60, row_height).with_label("Edit");

    let mut dragon_but_load_color = Button::default().with_pos(495, row_y[10]).with_size(60, row_height).with_label("Load");

    dragon_but_edit_color.deactivate();
    dragon_but_load_color.deactivate();
    dragon_but_edit_color.set_tooltip(&get_tooltips(&tooltip_map,&"rgb_but_edit_color"));
    dragon_but_load_color.set_tooltip(&get_tooltips(&tooltip_map,&"rgb_but_load_color"));

    let mut dragon_ck_transparent_color = CheckButton::default().with_pos(283, row_y[11])
        .with_size(20, row_height).with_align(Align::Left).with_label("Add Transparency");
    dragon_ck_transparent_color.set_checked(false);
    dragon_ck_transparent_color.set_tooltip(&get_tooltips(&tooltip_map,&"rgb_check_transparent_color"));


    let mut dragon_ck_gradient_color = CheckButton::default().with_pos(483, row_y[11])
        .with_size(20, row_height).with_align(Align::Left).with_label("Add Gradient");
    dragon_ck_gradient_color.set_checked(true);
    dragon_ck_gradient_color.set_tooltip(&get_tooltips(&tooltip_map,&"rgb_check_gradient_color"));

    let mut dragon_ck_lighten = CheckButton::default().with_pos(683, row_y[11])
        .with_size(20, row_height).with_align(Align::Left).with_label("Lighten");
    dragon_ck_lighten.set_checked(true);
    dragon_ck_lighten.set_tooltip(&get_tooltips(&tooltip_map,&"rgb_check_lighten"));


    let mut dragon_ch_background = Choice::new(105, row_y[12], 120, row_height, "Background : ");
    dragon_ch_background.add_choice("Dark");
    dragon_ch_background.add_choice("Color Grid");
    dragon_ch_background.add_choice("Transparent");
    dragon_ch_background.set_value(1);
    dragon_ch_background.set_tooltip(&get_tooltips(&tooltip_map,&"dragon_ch_background"));


    let mut dragon_config= DragonConfig{
        max_dragon: dragon_max_no_drg,
        enabled:  dragon_original_enabled_vec.clone(),
        position_1: dragon_original_position_1_vec.clone(),
        position_2: dragon_original_position_2_vec.clone(),
        position_3: dragon_original_position_3_vec.clone(),
        rotate: dragon_original_rotate_vec.clone(),
        flip: dragon_original_flip_vec.clone(),
        no_iter: dragon_original_no_iter_vec.clone(),
        stroke_width: dragon_original_stroke_width_vec.clone(),
        color_palette:  dragon_color_palette_random.clone(),
        add_transparency: dragon_ck_transparent_color.is_checked(),
        add_gradient: dragon_ck_gradient_color.is_checked(),
        lighten: dragon_ck_lighten.is_checked(),
        background_type: dragon_ch_background.value()
    };

    grp[2].end();
    //</editor-fold>


    //Barnsley Fern
    grp.push(Group::new(h_margin, row_y[3], 730, row_y[13], &tab_label[3]));
    //<editor-fold desc="Tab Barnsley Fern">
    let fern_max_no_weight = 6;
    let fern_no_coefficient =6;
    let fern_pre_activate =4;
    let mut fern_current_select =0_usize;

    let mut fern_choice_current = Choice::new(128, row_y[4], 60, row_height, "Select Equation : ");
    for i in 0..fern_max_no_weight {
        fern_choice_current.add_choice(&format!("{}", Ordinal(i+1).to_string()));
    };
    fern_choice_current.set_value(fern_current_select as i32);
    fern_choice_current.set_tooltip(&get_tooltips(&tooltip_map,&"fern_choice_current"));

    let mut fern_check_enable_vec:Vec<CheckButton>=Vec::new();
    let mut fern_inp_co:Vec<Vec<FloatInput>>=Vec::new();
    let mut fern_sli_co:Vec<Vec<Slider>>=Vec::new();
    let mut fern_inp_prob:Vec<IntInput>=Vec::new();
    let mut fern_sli_prob:Vec<Slider>=Vec::new();

    let mut fern_original_enable_vec:Vec<bool>=Vec::new();


    let mut fern_original_co_vec:Vec<Vec<f64>>=Vec::new();
    fern_original_co_vec.push(vec![0.0,0.0,0.0,0.16,0.0,0.0]);
    fern_original_co_vec.push(vec![0.85,0.04,-0.04,0.85,0.0,1.60]);
    fern_original_co_vec.push(vec![0.20,-0.26,0.23,0.22,0.0,1.60,0.07]);
    fern_original_co_vec.push(vec![-0.15,0.28,0.26,0.24,0.0,0.44,0.07]);
    fern_original_co_vec.push(vec![0.0,0.0,0.0,0.0,0.0,0.0]);
    fern_original_co_vec.push(vec![0.0,0.0,0.0,0.0,0.0,0.0]);

    let (fern_co_lower,fern_co_upper,fern_co_precision)=(-1.0,1.0,2);

    let fern_original_prob_vec=vec![1,85,7,7,0,0];

    let (fern_prob_lower,fern_prob_upper,fern_prob_precision)=(0.0,100.0,0);

    for i in 0..fern_max_no_weight{

        fern_check_enable_vec.push(CheckButton::default().with_pos(184, row_y[5])
            .with_size(20, row_height).with_align(Align::Left).with_label(&format!("Enable the {} Weight",Ordinal(i+1).to_string())));
        fern_original_enable_vec.push(if i<fern_pre_activate {
            true
        }else {
            false
        });
        fern_check_enable_vec[i].set_checked(fern_original_enable_vec[i]);
        fern_check_enable_vec[i].set_tooltip(&get_tooltips(&tooltip_map,&"fern_check_enabled"));

        let mut fern_inp_co_temp:Vec<FloatInput>=Vec::new();
        let mut fern_sli_co_temp:Vec<Slider>=Vec::new();
        for j in 0..fern_no_coefficient {

            fern_inp_co_temp.push(FloatInput::new(if j<2 {80}else if j<4 {300}else {520}, row_y[6+j%2], 45, row_height, &format!("C - {}: ",((97+j) as u8) as char)));
            fern_inp_co_temp[j].set_value(&format!("{}", fern_original_co_vec[i][j]));
            fern_inp_co_temp[j].set_tooltip(&get_tooltips(&tooltip_map,&"fern_inp_co_temp"));

            fern_sli_co_temp.push(Slider::new(if j<2 {140}else if j<4 {360}else {580}, row_y[6+j%2], 100, row_height, ""));
            fern_sli_co_temp[j].set_type(ScrollbarType::Horizontal);
            fern_sli_co_temp[j].set_bounds(fern_co_lower,fern_co_upper);
            fern_sli_co_temp[j].set_precision(fern_co_precision);
            fern_sli_co_temp[j].set_value(fern_original_co_vec[i][j]);
            fern_sli_co_temp[j].set_tooltip(&get_tooltips(&tooltip_map,&"fern_sli_co_temp"));

        }
        fern_inp_co.push(fern_inp_co_temp);
        fern_sli_co.push(fern_sli_co_temp);

        fern_inp_prob.push(IntInput::new(148, row_y[8], 40, row_height, "Probability Score: " ));
        fern_inp_prob[i].set_value(&format!("{}", fern_original_prob_vec[i]));
        fern_inp_prob[i].set_tooltip(&get_tooltips(&tooltip_map,&"fern_inp_prob"));

        fern_sli_prob.push(Slider::new(208, row_y[8], 100, row_height, ""));
        fern_sli_prob[i].set_type(ScrollbarType::Horizontal);
        fern_sli_prob[i].set_bounds(fern_prob_lower, fern_prob_upper);
        fern_sli_prob[i].set_precision(fern_prob_precision);
        fern_sli_prob[i].set_value(fern_original_prob_vec[i] as f64);
        fern_sli_prob[i].set_tooltip(&get_tooltips(&tooltip_map,&"fern_inp_prob"));

        if i!=fern_current_select {
            for j in 0..fern_no_coefficient {
                fern_inp_co[i][j].hide();
                fern_sli_co[i][j].hide();
            }
            fern_check_enable_vec[i].hide();
            fern_inp_prob[i].hide();
            fern_sli_prob[i].hide();
        }
        if !fern_original_enable_vec[i] {
            for j in 0..fern_no_coefficient {
                fern_inp_co[i][j].deactivate();
                fern_sli_co[i][j].deactivate();
            }
            fern_inp_prob[i].deactivate();
            fern_sli_prob[i].deactivate();
        }

    }

    fern_check_enable_vec[0].deactivate();

    let (fern_original_sample_value, fern_original_sample_lower, fern_original_sample_upper, fern_sample_precision):(f64, f64, f64, i32) = (9.0, 1.0, 48.0, 0);

    let mut fern_inp_sample = FloatInput::new(94, row_y[9],40, row_height, "No Sample: ");
    fern_inp_sample.set_value(&format!("{}", fern_original_sample_value));
    fern_inp_sample.set_tooltip(&get_tooltips(&tooltip_map,&"fern_inp_sample"));

    let mut fern_sli_sample = Slider::new(140, row_y[9], 100, row_height, "");
    fern_sli_sample.set_type(ScrollbarType::Horizontal);
    fern_sli_sample.set_bounds(fern_original_sample_lower, fern_original_sample_upper);
    fern_sli_sample.set_precision(fern_sample_precision);
    fern_sli_sample.set_value(fern_original_sample_value);
    fern_sli_sample.set_tooltip(&get_tooltips(&tooltip_map,&"fern_sli_sample"));


    let mut fern_check_weight_adj =CheckButton::default().with_pos(560, row_y[9])
        .with_size(20, row_height).with_align(Align::Left).with_label(&format!("Weight adjacent points "));
    fern_check_weight_adj.set_checked(true);
    fern_check_weight_adj.set_tooltip(&get_tooltips(&tooltip_map,&"fern_check_weight_adj"));

    let _fern_lab_color_config_ = controls::Label::new(12, row_y[10], 250, row_height, &format!("Color Config: "), w.color());


    let mut _fern_lab_hue_ = controls::Label::new(150, row_y[10], 250, row_height, &format!("Hue "), w.color());

    let mut _fern_lab_sat_ = controls::Label::new(150, row_y[11], 250, row_height, &format!("Saturation "), w.color());

    let mut _fern_lab_val_ = controls::Label::new(150, row_y[12], 250, row_height, &format!("Value "), w.color());

    let mut fern_inp_color_config_vec:Vec<FloatInput>=Vec::new();
    fern_inp_color_config_vec.push( FloatInput::new(286, row_y[10], 50, row_height, "From: "));
    fern_inp_color_config_vec.push( FloatInput::new(536, row_y[10], 50, row_height, "To: "));
    fern_inp_color_config_vec.push( FloatInput::new(286, row_y[11], 50, row_height, "From: "));
    fern_inp_color_config_vec.push( FloatInput::new(536, row_y[11], 50, row_height, "To: "));
    fern_inp_color_config_vec.push( FloatInput::new(286, row_y[12], 50, row_height, "From: "));
    fern_inp_color_config_vec.push( FloatInput::new(536, row_y[12], 50, row_height, "To: "));

    fern_inp_color_config_vec[0].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_hue_from"));
    fern_inp_color_config_vec[1].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_hue_to"));
    fern_inp_color_config_vec[2].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_sat_from"));
    fern_inp_color_config_vec[3].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_sat_to"));
    fern_inp_color_config_vec[4].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_val_from"));
    fern_inp_color_config_vec[5].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_val_to"));

    let mut fern_sli_color_config_vec:Vec<Slider>=Vec::new();
    fern_sli_color_config_vec.push( Slider::new(350, row_y[10], 120, row_height, ""));
    fern_sli_color_config_vec.push( Slider::new(600, row_y[10], 120, row_height, ""));
    fern_sli_color_config_vec.push( Slider::new(350, row_y[11], 120, row_height, ""));
    fern_sli_color_config_vec.push( Slider::new(600, row_y[11], 120, row_height, ""));
    fern_sli_color_config_vec.push( Slider::new(350, row_y[12], 120, row_height, ""));
    fern_sli_color_config_vec.push( Slider::new(600, row_y[12], 120, row_height, ""));

    fern_sli_color_config_vec[0].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_hue_from"));
    fern_sli_color_config_vec[1].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_hue_to"));
    fern_sli_color_config_vec[2].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_sat_from"));
    fern_sli_color_config_vec[3].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_sat_to"));
    fern_sli_color_config_vec[4].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_val_from"));
    fern_sli_color_config_vec[5].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_val_to"));


    let fern_color_config_original_vec=vec![120.0,0.0,0.9,1.0,0.9,1.0];
    let fern_color_config_upper_vec =vec![360.0,360.0,1.0,1.0,1.0,1.0];
    let fern_color_config_lower_vec =vec![0.0,0.0,0.0,0.0,0.0,0.0];
    let fern_color_config_precision_vec = vec![1,1,2,2,2,2];


    for i in 0..6 {
        fern_inp_color_config_vec[i].set_value(&format!("{}", fern_color_config_original_vec[i]));
        fern_sli_color_config_vec[i].set_type(ScrollbarType::Horizontal);
        fern_sli_color_config_vec[i].set_bounds(fern_color_config_lower_vec[i],  fern_color_config_upper_vec[i]);
        fern_sli_color_config_vec[i].set_precision(fern_color_config_precision_vec[i]);
        fern_sli_color_config_vec[i].set_value(fern_color_config_original_vec[i]);
    }

    let fern_no_color=32;

    let mut fern_ch_background = Choice::new(105, row_y[13], 132, row_height, "Background : ");
    fern_ch_background.add_choice("Dark");
    fern_ch_background.add_choice("Rainbow Shadow");
    fern_ch_background.add_choice("Transparent");
    fern_ch_background.set_value(1);
    fern_ch_background.set_tooltip(&get_tooltips(&tooltip_map,&"fern_ch_background"));

    let mut barnsley_fern_config = BarnsleyFernConfig{
        max_no_weight: fern_max_no_weight,
        enabled: fern_original_enable_vec.clone(),
        coefficient: fern_original_co_vec.clone(),
        probability_score: fern_original_prob_vec.clone(),
        sample_multiplier: fern_original_sample_value as u32,
        weight_adj: fern_check_weight_adj.is_checked(),
        color_palette: img_generator::generate_color_palette_hsv(&fern_no_color, &fern_color_config_original_vec),
        background_type: fern_ch_background.value(),
    };

    grp[3].end();
    //</editor-fold>

    //Mandelbrot
    grp.push(Group::new(h_margin, row_y[3], 730, row_y[13], &tab_label[4]));
    //<editor-fold desc="Tab Mandelbrot">

    let (mandelbrot_default_scalar_value, mandelbrot_original_scalar_lower, mandelbrot_original_scalar_upper, mandelbrot_scalar_precision):(f64, f64, f64, i32) = (2.5, 1.5, 6.0, 1);

    let mut mandelbrot_inp_scalar = FloatInput::new(60, row_y[4], 30, row_height, "Scale: ");
    mandelbrot_inp_scalar.set_value(&format!("{}", mandelbrot_default_scalar_value));
    mandelbrot_inp_scalar.set_tooltip(&get_tooltips(&tooltip_map,&"mandelbrot_inp_scalar"));

    let mut mandelbrot_sli_scalar = Slider::new(240, row_y[4], 245, row_height, "");
    mandelbrot_sli_scalar.set_type(ScrollbarType::Horizontal);
    mandelbrot_sli_scalar.set_bounds(mandelbrot_original_scalar_lower, mandelbrot_original_scalar_upper);
    mandelbrot_sli_scalar.set_precision(mandelbrot_scalar_precision);
    mandelbrot_sli_scalar.set_value(mandelbrot_default_scalar_value);
    mandelbrot_sli_scalar.set_tooltip(&get_tooltips(&tooltip_map,&"mandelbrot_sli_scalar"));

    let mut mandelbrot_ck_show_zoom = CheckButton::default().with_pos(683, row_y[4])
        .with_size(20, row_height).with_align(Align::Left).with_label("Show Zoom Area");
    mandelbrot_ck_show_zoom.set_checked(true);
    mandelbrot_ck_show_zoom.set_tooltip(&get_tooltips(&tooltip_map,&"mandelbrot_ck_show_zoom"));


    let mut _mandelbrot_selector_label = controls::Label::new(12, row_y[5], 250, row_height, &format!("Selector: "), w.color());

    let (mandelbrot_default_x_value, mandelbrot_original_x_lower, mandelbrot_original_x_upper, mandelbrot_x_precision):(f64, f64, f64, i32) = (0.0, 0.0, 100.0, 1);

    let mut mandelbrot_inp_x_loc = FloatInput::new(120, row_y[5], 50, row_height, "X: ");
    mandelbrot_inp_x_loc.set_value(&format!("{}", mandelbrot_default_x_value));
    mandelbrot_inp_x_loc.set_tooltip(&get_tooltips(&tooltip_map,&"mandelbrot_inp_x_loc"));

    let mut mandelbrot_sli_x_loc = Slider::new(240, row_y[5], 245, row_height, "");
    mandelbrot_sli_x_loc.set_type(ScrollbarType::Horizontal);
    mandelbrot_sli_x_loc.set_bounds(mandelbrot_original_x_lower, mandelbrot_original_x_upper);
    mandelbrot_sli_x_loc.set_precision(mandelbrot_x_precision);
    mandelbrot_sli_x_loc.set_value(mandelbrot_default_x_value);
    mandelbrot_inp_x_loc.set_tooltip(&get_tooltips(&tooltip_map,&"mandelbrot_inp_x_loc"));

    let (mandelbrot_default_y_value, mandelbrot_original_y_lower, mandelbrot_original_y_upper, mandelbrot_y_precision):(f64, f64, f64, i32) = (0.0, 0.0, 100.0, 1);

    let mut mandelbrot_inp_y_loc = FloatInput::new(120, row_y[6], 50, row_height, "Y: ");
    mandelbrot_inp_y_loc.set_value(&format!("{}", mandelbrot_default_y_value));
    mandelbrot_inp_y_loc.set_tooltip(&get_tooltips(&tooltip_map,&"mandelbrot_inp_y_loc"));

    let mut mandelbrot_sli_y_loc = Slider::new(240, row_y[6], 245, row_height, "");
    mandelbrot_sli_y_loc.set_type(ScrollbarType::Horizontal);
    mandelbrot_sli_y_loc.set_bounds(mandelbrot_original_y_lower, mandelbrot_original_y_upper);
    mandelbrot_sli_y_loc.set_precision(mandelbrot_y_precision);
    mandelbrot_sli_y_loc.set_value(mandelbrot_default_y_value);
    mandelbrot_inp_y_loc.set_tooltip(&get_tooltips(&tooltip_map,&"mandelbrot_inp_y_loc"));

    let mut mandelbrot_ck_show_selector = CheckButton::default().with_pos(683, row_y[6])
        .with_size(20, row_height).with_align(Align::Left).with_label("Show Selection Lines");
    mandelbrot_ck_show_selector.set_checked(true);
    mandelbrot_ck_show_selector.set_tooltip(&get_tooltips(&tooltip_map,&"mandelbrot_ck_show_selector"));


    let mut mandelbrot_but_zoom_in = Button::default().with_pos(100, row_y[7]).with_size(100, row_height).with_label("Zoom In");

    let mandelbrot_max_zoom =6_f64.powf(15.0);
    mandelbrot_but_zoom_in.set_tooltip(&get_tooltips(&tooltip_map,&"mandelbrot_but_zoom_in"));

    let mut mandelbrot_but_zoom_out = Button::default().with_pos(260, row_y[7]).with_size(100, row_height).with_label("Zoom Out");
    mandelbrot_but_zoom_out.deactivate();
    mandelbrot_but_zoom_out.set_tooltip(&get_tooltips(&tooltip_map,&"mandelbrot_but_zoom_out"));

    let (mandelbrot_default_max_inter_value, mandelbrot_original_max_inter_lower, mandelbrot_original_max_inter_upper, mandelbrot_max_inter_precision):(f64, f64, f64, i32) = (256.0, 8.0, 1024.0, 0);

    let mut mandelbrot_inp_max_inter = FloatInput::new(107, row_y[8], 50, row_height, "Max Iteration: ");
    mandelbrot_inp_max_inter.set_value(&format!("{}", mandelbrot_default_max_inter_value));
    mandelbrot_inp_max_inter.set_tooltip(&get_tooltips(&tooltip_map,&"mandelbrot_inp_max_inter"));

    let mut mandelbrot_sli_max_inter = Slider::new(177, row_y[8], 175, row_height, "");
    mandelbrot_sli_max_inter.set_type(ScrollbarType::Horizontal);
    mandelbrot_sli_max_inter.set_bounds(mandelbrot_original_max_inter_lower, mandelbrot_original_max_inter_upper);
    mandelbrot_sli_max_inter.set_precision(mandelbrot_max_inter_precision);
    mandelbrot_sli_max_inter.set_value(mandelbrot_default_max_inter_value);
    mandelbrot_sli_max_inter.set_tooltip(&get_tooltips(&tooltip_map,&"mandelbrot_sli_max_inter"));

    let (mandelbrot_default_max_z_value, mandelbrot_original_max_z_lower, mandelbrot_original_max_z_upper, mandelbrot_max_z_precision):(f64, f64, f64, i32) = (2.0, 2.0, 1024.0, 0);

    let mut mandelbrot_inp_max_z = FloatInput::new(447, row_y[8], 50, row_height, "Max Z: ");
    mandelbrot_inp_max_z.set_value(&format!("{}", mandelbrot_default_max_z_value));
    mandelbrot_inp_max_z.set_tooltip(&get_tooltips(&tooltip_map,&"mandelbrot_inp_max_z"));

    let mut mandelbrot_sli_max_z = Slider::new(517, row_y[8], 175, row_height, "");
    mandelbrot_sli_max_z.set_type(ScrollbarType::Horizontal);
    mandelbrot_sli_max_z.set_bounds(mandelbrot_original_max_z_lower, mandelbrot_original_max_z_upper);
    mandelbrot_sli_max_z.set_precision(mandelbrot_max_z_precision);
    mandelbrot_sli_max_z.set_value(mandelbrot_default_max_z_value);
    mandelbrot_inp_max_z.set_tooltip(&get_tooltips(&tooltip_map,&"mandelbrot_inp_max_z"));

    let mut mandelbrot_ch_coloring = Choice::new(131, row_y[9], 162, row_height, "Coloring Method: ");
    mandelbrot_ch_coloring.add_choice("Iteration value based");
    mandelbrot_ch_coloring.add_choice("Z Value based");
    mandelbrot_ch_coloring.set_value(0);
    mandelbrot_ch_coloring.set_tooltip(&get_tooltips(&tooltip_map,&"mandelbrot_ch_coloring"));


    let _mandelbrot_lab_color_config_ = controls::Label::new(14, row_y[10], 250, row_height, &format!("Color Config: "), w.color());

    let mut _mandelbrot_lab_hue_ = controls::Label::new(150, row_y[10], 250, row_height, &format!("Hue "), w.color());

    let mut _mandelbrot_lab_sat_ = controls::Label::new(150, row_y[11], 250, row_height, &format!("Saturation "), w.color());

    let mut _mandelbrot_lab_val_ = controls::Label::new(150, row_y[12], 250, row_height, &format!("Value "), w.color());

    let mut mandelbrot_inp_color_1_config_vec:Vec<FloatInput>=Vec::new();
    mandelbrot_inp_color_1_config_vec.push( FloatInput::new(286, row_y[10], 50, row_height, "From: "));
    mandelbrot_inp_color_1_config_vec.push( FloatInput::new(536, row_y[10], 50, row_height, "To: "));
    mandelbrot_inp_color_1_config_vec.push( FloatInput::new(286, row_y[11], 50, row_height, "From: "));
    mandelbrot_inp_color_1_config_vec.push( FloatInput::new(536, row_y[11], 50, row_height, "To: "));
    mandelbrot_inp_color_1_config_vec.push( FloatInput::new(286, row_y[12], 50, row_height, "From: "));
    mandelbrot_inp_color_1_config_vec.push( FloatInput::new(536, row_y[12], 50, row_height, "To: "));

    mandelbrot_inp_color_1_config_vec[0].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_hue_from"));
    mandelbrot_inp_color_1_config_vec[1].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_hue_to"));
    mandelbrot_inp_color_1_config_vec[2].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_sat_from"));
    mandelbrot_inp_color_1_config_vec[3].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_sat_to"));
    mandelbrot_inp_color_1_config_vec[4].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_val_from"));
    mandelbrot_inp_color_1_config_vec[5].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_val_to"));

    let mut mandelbrot_sli_color_1_config_vec:Vec<Slider>=Vec::new();
    mandelbrot_sli_color_1_config_vec.push( Slider::new(350, row_y[10], 120, row_height, ""));
    mandelbrot_sli_color_1_config_vec.push( Slider::new(600, row_y[10], 120, row_height, ""));
    mandelbrot_sli_color_1_config_vec.push( Slider::new(350, row_y[11], 120, row_height, ""));
    mandelbrot_sli_color_1_config_vec.push( Slider::new(600, row_y[11], 120, row_height, ""));
    mandelbrot_sli_color_1_config_vec.push( Slider::new(350, row_y[12], 120, row_height, ""));
    mandelbrot_sli_color_1_config_vec.push( Slider::new(600, row_y[12], 120, row_height, ""));

    mandelbrot_sli_color_1_config_vec[0].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_hue_from"));
    mandelbrot_sli_color_1_config_vec[1].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_hue_to"));
    mandelbrot_sli_color_1_config_vec[2].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_sat_from"));
    mandelbrot_sli_color_1_config_vec[3].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_sat_to"));
    mandelbrot_sli_color_1_config_vec[4].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_val_from"));
    mandelbrot_sli_color_1_config_vec[5].set_tooltip(&get_tooltips(&tooltip_map,&"hsv_val_to"));


    let mandelbrot_color_1_config_original_vec =vec![276.0, 27.0, 0.5, 1.0, 0.5, 1.0];
    let mandelbrot_color_1_config_upper_vec =vec![360.0, 360.0, 1.0, 1.0, 1.0, 1.0];
    let mandelbrot_color_1_config_lower_vec =vec![0.0, 0.0, 0.0, 0.0, 0.0, 0.0];
    let mandelbrot_color_1_config_precision_vec = vec![1, 1, 2, 2, 2, 2];

    for i in 0..6 {
        mandelbrot_inp_color_1_config_vec[i].set_value(&format!("{}", mandelbrot_color_1_config_original_vec[i]));
        mandelbrot_sli_color_1_config_vec[i].set_type(ScrollbarType::Horizontal);
        mandelbrot_sli_color_1_config_vec[i].set_bounds(mandelbrot_color_1_config_lower_vec[i], mandelbrot_color_1_config_upper_vec[i]);
        mandelbrot_sli_color_1_config_vec[i].set_precision(mandelbrot_color_1_config_precision_vec[i]);
        mandelbrot_sli_color_1_config_vec[i].set_value(mandelbrot_color_1_config_original_vec[i]);
    }


    let mut mandelbrot_inp_color_2_config_vec:Vec<FloatInput>=Vec::new();
    mandelbrot_inp_color_2_config_vec.push( FloatInput::new(196, row_y[10], 50, row_height, "C - ln(Z): "));
    mandelbrot_inp_color_2_config_vec.push( FloatInput::new(176, row_y[11], 40, row_height, "C - R: "));
    mandelbrot_inp_color_2_config_vec.push( FloatInput::new(376, row_y[11], 40, row_height, "C - G: "));
    mandelbrot_inp_color_2_config_vec.push( FloatInput::new(576, row_y[11], 40, row_height, "C - B: "));

    mandelbrot_inp_color_2_config_vec[0].set_tooltip(&get_tooltips(&tooltip_map,&"rgb_cof_ln"));
    mandelbrot_inp_color_2_config_vec[1].set_tooltip(&get_tooltips(&tooltip_map,&"rgb_cof_r"));
    mandelbrot_inp_color_2_config_vec[2].set_tooltip(&get_tooltips(&tooltip_map,&"rgb_cof_g"));
    mandelbrot_inp_color_2_config_vec[3].set_tooltip(&get_tooltips(&tooltip_map,&"rgb_cof_b"));


    let mut mandelbrot_sli_color_2_config_vec:Vec<Slider>=Vec::new();
    mandelbrot_sli_color_2_config_vec.push( Slider::new(266, row_y[10], 120, row_height, ""));
    mandelbrot_sli_color_2_config_vec.push( Slider::new(226, row_y[11], 80, row_height, ""));
    mandelbrot_sli_color_2_config_vec.push( Slider::new(426, row_y[11], 80, row_height, ""));
    mandelbrot_sli_color_2_config_vec.push( Slider::new(626, row_y[11], 80, row_height, ""));

    mandelbrot_sli_color_2_config_vec[0].set_tooltip(&get_tooltips(&tooltip_map,&"rgb_cof_ln"));
    mandelbrot_sli_color_2_config_vec[1].set_tooltip(&get_tooltips(&tooltip_map,&"rgb_cof_r"));
    mandelbrot_sli_color_2_config_vec[2].set_tooltip(&get_tooltips(&tooltip_map,&"rgb_cof_g"));
    mandelbrot_sli_color_2_config_vec[3].set_tooltip(&get_tooltips(&tooltip_map,&"rgb_cof_b"));


    let mandelbrot_color_2_config_original_vec =vec![30.0, 1.44, 0.34, 0.18];
    let mandelbrot_color_2_config_upper_vec =vec![100.0, 6.28, 6.28, 6.28];
    let mandelbrot_color_2_config_lower_vec =vec![0.01, 0.0, 0.0, 0.0];
    let mandelbrot_color_2_config_precision_vec = vec![2, 2, 2, 2];

    for i in 0..4 {
        mandelbrot_inp_color_2_config_vec[i].set_value(&format!("{}", mandelbrot_color_2_config_original_vec[i]));
        mandelbrot_sli_color_2_config_vec[i].set_type(ScrollbarType::Horizontal);
        mandelbrot_sli_color_2_config_vec[i].set_bounds(mandelbrot_color_2_config_lower_vec[i], mandelbrot_color_2_config_upper_vec[i]);
        mandelbrot_sli_color_2_config_vec[i].set_precision(mandelbrot_color_2_config_precision_vec[i]);
        mandelbrot_sli_color_2_config_vec[i].set_value(mandelbrot_color_2_config_original_vec[i]);
        mandelbrot_inp_color_2_config_vec[i].hide();
        mandelbrot_sli_color_2_config_vec[i].hide();
    }



    let mut mandelbrot_config = MandelbrotConfig{
        scalar: mandelbrot_sli_scalar.value(),
        max_zoom: mandelbrot_max_zoom,
        min_x_percentage: mandelbrot_sli_x_loc.value(),
        min_y_percentage: mandelbrot_sli_y_loc.value(),
        lock_zoom_in: false,
        show_zoom: mandelbrot_ck_show_zoom.is_checked(),
        show_selector: mandelbrot_ck_show_selector.is_checked(),
        max_iteration: mandelbrot_default_max_inter_value as i32,
        max_z: mandelbrot_default_max_z_value,
        cache_img: vec![],
        history: vec![(0.0,0.0,1.0)],
        size_ratio: vec![(0.0, 0.0, 1.0)],
        coloring_type: mandelbrot_ch_coloring.value(),
        color_type_2_coff: mandelbrot_color_2_config_original_vec.clone(),
        color_palette: generate_color_palette_hsv(&256, &mandelbrot_color_1_config_original_vec),
    };
    grp[4].end();
    //</editor-fold>

    tab.end();


    //Right Panel
    let uni_config=UniConfig{
        chaos_game_config:chaos_game_config.clone(),
        julia_config:julia_config.clone(),
        dragon_config:dragon_config.clone(),
        barnsley_fern_config:barnsley_fern_config.clone(),
        mandelbrot_config:mandelbrot_config.clone(),
    };

    let mut text_display = TextDisplay::default().with_pos(v_margin, row_y[16]).with_size(720, 162);
    let text_display_buf:TextBuffer =TextBuffer::default();
    text_display.set_buffer(Some(text_display_buf));
    text_display.set_text_size(12);
    text_display.insert(&format!("Chaos Game VERSION {}",VERSION.unwrap_or("unknown")));
    let mut text_display_history:VecDeque<String>= VecDeque::new();


    let mut check_smooth_preview = CheckButton::default().with_pos(879, row_y[15])
        .with_size(20, row_height).with_align(Align::Left).with_label("Smoother Preview");
    check_smooth_preview.set_checked(false);
    check_smooth_preview.set_tooltip(&get_tooltips(&tooltip_map,&"check_smooth_preview"));

    let mut check_enable_smt = CheckButton::default().with_pos(1095, row_y[15])
        .with_size(20, row_height).with_align(Align::Left).with_label("Multithreading Optimization");
    check_enable_smt.set_checked(false);
    check_enable_smt.set_tooltip(&get_tooltips(&tooltip_map,&"check_enable_smt"));



    let mut frame = Frame::new(770,row_y[4],320,320,"");

    let mut ch_export_resolution = Choice::new(880, row_y[16], 120, row_height, "Export Resolution :");

    ch_export_resolution.add_choice("1280x720");
    ch_export_resolution.add_choice("1920x1080");
    ch_export_resolution.add_choice("2560x1440");
    ch_export_resolution.add_choice("3840x2160");
    ch_export_resolution.set_value(1);
    ch_export_resolution.set_tooltip(&get_tooltips(&tooltip_map, &"ch_export_resolution"));

    let mut check_un_crop = CheckButton::default().with_pos(1082, row_y[16])
        .with_size(20, row_height).with_align(Align::Left).with_label("Un-crop");
    check_un_crop.set_checked(false);
    check_un_crop.set_tooltip(&get_tooltips(&tooltip_map, &"check_un_crop"));

    let (contrast_default_value, contrast_min_value, contrast_max_value, contrast_precision) =(0.0_f64,-100.0,100.0,1);

    let mut adj_contrast_inp = FloatInput::new(860, row_y[17], 50, row_height, "Adj. Contrast: ");
    adj_contrast_inp.set_value(&format!("{}",contrast_default_value));
    adj_contrast_inp.set_tooltip(&get_tooltips(&tooltip_map, &"adj_contrast_inp"));

    let mut adj_contrast_sli = Slider::new(960, row_y[17], 150, row_height, "");
    adj_contrast_sli.set_type(ScrollbarType::Horizontal);
    adj_contrast_sli.set_bounds(contrast_min_value, contrast_max_value);
    adj_contrast_sli.set_precision(contrast_precision);
    adj_contrast_sli.set_value(contrast_default_value);
    adj_contrast_sli.set_tooltip(&get_tooltips(&tooltip_map, &"adj_contrast_sli"));

    let (brightness_default_value, brightness_min_value, brightness_max_value, brightness_precision) =(0.0, -100.0, 100.0, 0);

    let mut adj_brightness_inp = IntInput::new(860, row_y[18], 50, row_height, "Adj. Brightness: ");
    adj_brightness_inp.set_value(&format!("{}", brightness_default_value));
    adj_brightness_inp.set_tooltip(&get_tooltips(&tooltip_map, &"adj_brightness_inp"));

    let mut adj_brightness_sli = Slider::new(960, row_y[18], 150, row_height, "");
    adj_brightness_sli.set_type(ScrollbarType::Horizontal);
    adj_brightness_sli.set_bounds(brightness_min_value, brightness_max_value);
    adj_brightness_sli.set_precision(brightness_precision);
    adj_brightness_sli.set_value(brightness_default_value);
    adj_brightness_sli.set_tooltip(&get_tooltips(&tooltip_map, &"adj_brightness_sli"));

    let mut chaos_game_no_save:img_generator::SaveOption = SaveOption {
        scale_factor:1,
        crop_h:0,
        preview_downscale: if check_smooth_preview.is_checked() {2.0}else {1.0},
        enable_multithreading: check_enable_smt.is_checked(),
        contrast: contrast_default_value.clone() as f32,
        brightness: brightness_default_value.clone() as i32,
        path: Path::new(".").join("output"),
    };

    if !chaos_game_no_save.path.exists(){
        create_dir(&chaos_game_no_save.path).unwrap();
    }
    frame.set_image(Some(fltk_image_gen(&mut chaos_game_config, &chaos_game_no_save)));

    let mut _label_save_load_config_ = controls::Label::new(758, row_y[19], 250, row_height, &format!("Save/Load Config :"), w.color());

    let mut but_save_config = Button::default().with_pos(890, row_y[19]).with_size(60, row_height).with_label("Save");
    but_save_config.set_tooltip(&get_tooltips(&tooltip_map, &"but_save_config"));

    let mut but_load_config = Button::default().with_pos(960, row_y[19]).with_size(60, row_height).with_label("Load");
    but_load_config.set_tooltip(&get_tooltips(&tooltip_map, &"but_load_config"));

    let mut but_reset_config = Button::default().with_pos(1030, row_y[19]).with_size(60, row_height).with_label("Reset");
    but_reset_config.set_tooltip(&get_tooltips(&tooltip_map, &"but_reset_config"));

    let mut but_export_image = Button::default().with_pos(805, row_y[20]).with_size(100, row_height).with_label("Export Image");
    but_export_image.set_tooltip(&get_tooltips(&tooltip_map, &"but_export_image"));

    let mut but_locate_image = Button::default().with_pos(975, row_y[20]).with_size(100, row_height).with_label("Locate Folder");
    but_locate_image.set_tooltip(&get_tooltips(&tooltip_map, &"but_locate_image"));

    w.make_resizable(true);
    w.end();
    w.show();

    let (s, r) = channel::<Message>();

    tab.set_trigger(CallbackTrigger::Changed);
    tab.emit(s,Message::GeneralConfigChange);

    let mut accommodate_feature =true;
    for fp in CONFIG_FILE_PATH.iter() {
        if !fp.exists() {
            accommodate_feature = false;
            break;
        }
    };

    for argument in env::args_os() {
        let arg_str = argument.into_string().unwrap();
        if arg_str.contains("full_feature") {
            accommodate_feature = true;
            for i in 0..CONFIG_FILE_PATH.len() {
                if !CONFIG_FILE_PATH[i].exists() {
                    let save_result = match i {
                        1 => {
                            io::save_config(&mut uni_config.julia_config.clone())
                        }
                        2 => {
                            io::save_config(&mut uni_config.dragon_config.clone())
                        }
                        3 => {
                            io::save_config(&mut uni_config.barnsley_fern_config.clone())
                        }
                        4 => {
                            io::save_config(&mut uni_config.mandelbrot_config.clone())
                        }
                        0 | _ => {
                            io::save_config(&mut uni_config.chaos_game_config.clone())
                        }
                    };
                    match save_result {
                        Ok(..) => {
                        }
                        Err(..) => {
                            accommodate_feature = false;
                        }
                    }
                }
            }
        }
    }


    if !accommodate_feature {
        check_enable_smt.hide();
        _label_save_load_config_.hide();
        but_save_config.hide();
        but_load_config.hide();
        but_reset_config.hide();
        if let Some(mut item) = ch_export_resolution.find_item(&format!("2560x1440")) {
            item.hide();
        }
        if let Some(mut item) = ch_export_resolution.find_item(&format!("3840x2160")) {
            item.hide();
        }
    }

    //<editor-fold desc="Emit & Trigger: Chaos Game">
    chs_gm_inp_scalar.set_trigger(CallbackTrigger::Changed);
    chs_gm_inp_scalar.emit(s, Message::ChaosGameInputScalarChange);
    chs_gm_sli_scalar.set_trigger(CallbackTrigger::Changed);
    chs_gm_sli_scalar.emit(s, Message::ChaosGameSliderScalarChange);

    chs_gm_inp_no_vertices.set_trigger(CallbackTrigger::Changed);
    chs_gm_inp_no_vertices.emit(s, Message::ChaosGameVerticesChange);
    chs_gm_check_incl_center.set_trigger(CallbackTrigger::Changed);
    chs_gm_check_incl_center.emit(s, Message::ChaosGameVerticesChange);
    chs_gm_check_incl_mid.set_trigger(CallbackTrigger::Changed);
    chs_gm_check_incl_mid.emit(s, Message::ChaosGameVerticesChange);

    chs_gm_sli_jump_ratio.set_trigger(CallbackTrigger::Changed);
    chs_gm_sli_jump_ratio.emit(s, Message::ChaosGameSliderJumpChange);

    chs_gm_inp_jump_ratio.set_trigger(CallbackTrigger::Changed);
    chs_gm_inp_jump_ratio.emit(s, Message::ChaosGameInputJumpChange);

    chs_gm_ch_condition.set_trigger(CallbackTrigger::Changed);
    chs_gm_ch_condition.emit(s, Message::ChaosGameChoiceConditionChange);

    chs_gm_inp_modifier_i.set_trigger(CallbackTrigger::Changed);
    chs_gm_inp_modifier_i.emit(s, Message::ChaosGameIntModifier1Change);

    chs_gm_inp_color_rand_seed.set_trigger(CallbackTrigger::Changed);
    chs_gm_inp_color_rand_seed.emit(s, Message::ChaosGameColorRandomSeedChange);

    chs_gm_check_transparent_color.set_trigger(CallbackTrigger::Changed);
    chs_gm_check_transparent_color.emit(s, Message::ChaosGameCustomColorUsageChange);
    chs_gm_check_custom_color.set_trigger(CallbackTrigger::Changed);
    chs_gm_check_custom_color.emit(s, Message::ChaosGameCustomColorUsageChange);

    chs_gm_but_edit_color.emit(s, Message::ChaosGameEditColor);

    chs_gm_but_load_color.emit(s, Message::ChaosGameLoadColor);

    chs_gm_ch_background.set_trigger(CallbackTrigger::Changed);
    chs_gm_ch_background.emit(s, Message::ChaosGameBackgroundChange);

    //</editor-fold>

    //<editor-fold desc="Emit & Trigger: Julia">
    jul_inp_scalar.set_trigger(CallbackTrigger::Changed);
    jul_inp_scalar.emit(s,Message::JuliaInputScalarChange);
    jul_sli_scalar.set_trigger(CallbackTrigger::Changed);
    jul_sli_scalar.emit(s,Message::JuliaSliderScalarChange);

    jul_inp_c_real.set_trigger(CallbackTrigger::Changed);
    jul_inp_c_real.emit(s,Message::JuliaInputCRealChange);
    jul_sli_c_real.set_trigger(CallbackTrigger::Changed);
    jul_sli_c_real.emit(s,Message::JuliaSliderCRealChange);

    jul_inp_c_imaginary.set_trigger(CallbackTrigger::Changed);
    jul_inp_c_imaginary.emit(s,Message::JuliaInputCImaginaryChange);
    jul_sli_c_imaginary.set_trigger(CallbackTrigger::Changed);
    jul_sli_c_imaginary.emit(s,Message::JuliaSliderCImaginaryChange);

    jul_inp_z_pow.set_trigger(CallbackTrigger::Changed);
    jul_inp_z_pow.emit(s,Message::JuliaInputZExponentChange);
    jul_sli_z_pow.set_trigger(CallbackTrigger::Changed);
    jul_sli_z_pow.emit(s,Message::JuliaSliderZExponentChange);

    for i in 0..6 {
        jul_inp_color_config_vec[i].set_trigger(CallbackTrigger::Changed);
        jul_inp_color_config_vec[i].emit(s,Message::JuliaInputColorChange);
        jul_sli_color_config_vec[i].set_trigger(CallbackTrigger::Changed);
        jul_sli_color_config_vec[i].emit(s,Message::JuliaSliderColorChange);
    }

    jul_ch_background.set_trigger(CallbackTrigger::Changed);
    jul_ch_background.emit(s,Message::JuliaBackgroundChange);

    //</editor-fold>

    //<editor-fold desc="Message: Emit & Trigger: Dragon">

    dragon_choice_current.set_trigger(CallbackTrigger::Changed);
    dragon_choice_current.emit(s, Message::DragonChoiceSelectionChange);

    for i in 0..dragon_max_no_drg{
        dragon_check_enabled_vec[i].set_trigger(CallbackTrigger::Changed);
        dragon_check_enabled_vec[i].emit(s, Message::DragonCheckEnableChange);

        dragon_ch_position_1_vec[i].set_trigger(CallbackTrigger::Changed);
        dragon_ch_position_1_vec[i].emit(s, Message::DragonConfigChange);

        dragon_ch_position_2_vec[i].set_trigger(CallbackTrigger::Changed);
        dragon_ch_position_2_vec[i].emit(s, Message::DragonConfigChange);

        dragon_ch_position_3_vec[i].set_trigger(CallbackTrigger::Changed);
        dragon_ch_position_3_vec[i].emit(s, Message::DragonConfigChange);

        dragon_ch_rotate_vec[i].set_trigger(CallbackTrigger::Changed);
        dragon_ch_rotate_vec[i].emit(s, Message::DragonDirectionChange);

        dragon_ch_flip_vec[i].set_trigger(CallbackTrigger::Changed);
        dragon_ch_flip_vec[i].emit(s, Message::DragonDirectionChange);

        dragon_inp_no_iter_vec[i].set_trigger(CallbackTrigger::Changed);
        dragon_inp_no_iter_vec[i].emit(s, Message::DragonInputNoIterationChange);

        dragon_sli_no_iter_vec[i].set_trigger(CallbackTrigger::Changed);
        dragon_sli_no_iter_vec[i].emit(s, Message::DragonSliderNoIterationChange);

        dragon_inp_stroke_width_vec[i].set_trigger(CallbackTrigger::Changed);
        dragon_inp_stroke_width_vec[i].emit(s, Message::DragonInputLineWidthChange);

        dragon_sli_stroke_width_vec[i].set_trigger(CallbackTrigger::Changed);
        dragon_sli_stroke_width_vec[i].emit(s, Message::DragonSliderLineWidthChange);
    }


    dragon_inp_color_rand_seed.set_trigger(CallbackTrigger::Changed);
    dragon_inp_color_rand_seed.emit(s, Message::DragonColorRandomSeedChange);

    dragon_ck_transparent_color.set_trigger(CallbackTrigger::Changed);
    dragon_ck_transparent_color.emit(s, Message::DragonCustomColorUsageChange);

    dragon_ck_custom_color.set_trigger(CallbackTrigger::Changed);
    dragon_ck_custom_color.emit(s, Message::DragonCustomColorUsageChange);

    dragon_ck_gradient_color.set_trigger(CallbackTrigger::Changed);
    dragon_ck_gradient_color.emit(s, Message::DragonCustomColorUsageChange);

    dragon_ck_lighten.set_trigger(CallbackTrigger::Changed);
    dragon_ck_lighten.emit(s, Message::DragonCustomColorUsageChange);

    dragon_but_edit_color.emit(s, Message::DragonEditColor);

    dragon_but_load_color.emit(s, Message::DragonLoadColor);

    dragon_ch_background.set_trigger(CallbackTrigger::Changed);
    dragon_ch_background.emit(s, Message::DragonBackgroundChange);

    //</editor-fold>

    //<editor-fold desc="Message: Emit & Trigger: Fern">
    fern_choice_current.emit(s, Message::FernChoiceSelectionChange);
    fern_choice_current.set_trigger(CallbackTrigger::Changed);

    for i in 0..fern_max_no_weight{
        fern_check_enable_vec[i].emit(s, Message::FernCheckEnableChange);
        fern_check_enable_vec[i].set_trigger(CallbackTrigger::Changed);
        for j in 0..fern_no_coefficient {
            fern_inp_co[i][j].emit(s, Message::FernInputCoefficientChange);
            fern_inp_co[i][j].set_trigger(CallbackTrigger::Changed);

            fern_sli_co[i][j].emit(s, Message::FernSliderCoefficientChange);
            fern_sli_co[i][j].set_trigger(CallbackTrigger::Changed);
        }
        fern_inp_prob[i].emit(s, Message::FernInputProbabilityChange);
        fern_inp_prob[i].set_trigger(CallbackTrigger::Changed);

        fern_sli_prob[i].emit(s, Message::FernSliderProbabilityChange);
        fern_sli_prob[i].set_trigger(CallbackTrigger::Changed);
    }

    fern_inp_sample.set_trigger(CallbackTrigger::Changed);
    fern_inp_sample.emit(s, Message::FernInputSampleChange);

    fern_sli_sample.set_trigger(CallbackTrigger::Changed);
    fern_sli_sample.emit(s, Message::FernSliderSampleChange);

    fern_check_weight_adj.set_trigger(CallbackTrigger::Changed);
    fern_check_weight_adj.emit(s, Message::FernCheckWeightAdjacentChange);

    fern_ch_background.set_trigger(CallbackTrigger::Changed);
    fern_ch_background.emit(s, Message::FernBackgroundChange);

    for i in 0..6 {
        fern_inp_color_config_vec[i].set_trigger(CallbackTrigger::Changed);
        fern_inp_color_config_vec[i].emit(s,Message::FernInputColorChange);
        fern_sli_color_config_vec[i].set_trigger(CallbackTrigger::Changed);
        fern_sli_color_config_vec[i].emit(s,Message::FernSliderColorChange);
    }

    //</editor-fold>

    //<editor-fold desc="Message: Emit & Trigger: Mandelbrot">
    mandelbrot_inp_scalar.set_trigger(CallbackTrigger::Changed);
    mandelbrot_inp_scalar.emit(s,Message::MandelbrotInputScalarChange);
    mandelbrot_sli_scalar.set_trigger(CallbackTrigger::Changed);
    mandelbrot_sli_scalar.emit(s,Message::MandelbrotSliderScalarChange);

    mandelbrot_inp_x_loc.set_trigger(CallbackTrigger::Changed);
    mandelbrot_inp_x_loc.emit(s,Message::MandelbrotInputXLocChange);
    mandelbrot_sli_x_loc.set_trigger(CallbackTrigger::Changed);
    mandelbrot_sli_x_loc.emit(s,Message::MandelbrotSliderXLocChange);

    mandelbrot_inp_y_loc.set_trigger(CallbackTrigger::Changed);
    mandelbrot_inp_y_loc.emit(s,Message::MandelbrotInputYLocChange);
    mandelbrot_sli_y_loc.set_trigger(CallbackTrigger::Changed);
    mandelbrot_sli_y_loc.emit(s,Message::MandelbrotSliderYLocChange);

    mandelbrot_ck_show_zoom.set_trigger(CallbackTrigger::Changed);
    mandelbrot_ck_show_zoom.emit(s,Message::MandelbrotShowChange);

    mandelbrot_ck_show_selector.set_trigger(CallbackTrigger::Changed);
    mandelbrot_ck_show_selector.emit(s,Message::MandelbrotShowChange);

    mandelbrot_but_zoom_in.emit(s,Message::MandelbrotZoomIn);
    mandelbrot_but_zoom_out.emit(s,Message::MandelbrotZoomOut);

    mandelbrot_inp_max_inter.set_trigger(CallbackTrigger::Changed);
    mandelbrot_inp_max_inter.emit(s, Message::MandelbrotInputIterationChange);
    mandelbrot_sli_max_inter.set_trigger(CallbackTrigger::Changed);
    mandelbrot_sli_max_inter.emit(s, Message::MandelbrotSliderIterationChange);

    mandelbrot_inp_max_z.set_trigger(CallbackTrigger::Changed);
    mandelbrot_inp_max_z.emit(s, Message::MandelbrotInputZChange);
    mandelbrot_sli_max_z.set_trigger(CallbackTrigger::Changed);
    mandelbrot_sli_max_z.emit(s, Message::MandelbrotSliderZChange);

    mandelbrot_ch_coloring.set_trigger(CallbackTrigger::Changed);
    mandelbrot_ch_coloring.emit(s, Message::MandelbrotColoringTypeChange);


    for i in 0..mandelbrot_inp_color_1_config_vec.len() {
        mandelbrot_inp_color_1_config_vec[i].set_trigger(CallbackTrigger::Changed);
        mandelbrot_inp_color_1_config_vec[i].emit(s, Message::MandelbrotInputColor1Change);
        mandelbrot_sli_color_1_config_vec[i].set_trigger(CallbackTrigger::Changed);
        mandelbrot_sli_color_1_config_vec[i].emit(s, Message::MandelbrotSliderColor1Change);
    }

    for i in 0..mandelbrot_inp_color_2_config_vec.len() {
        mandelbrot_inp_color_2_config_vec[i].set_trigger(CallbackTrigger::Changed);
        mandelbrot_inp_color_2_config_vec[i].emit(s, Message::MandelbrotInputColor2Change);
        mandelbrot_sli_color_2_config_vec[i].set_trigger(CallbackTrigger::Changed);
        mandelbrot_sli_color_2_config_vec[i].emit(s, Message::MandelbrotSliderColor2Change);
    }
    //</editor-fold>


    check_smooth_preview.set_trigger(CallbackTrigger::Changed);
    check_smooth_preview.emit(s, Message::GeneralConfigChange);

    check_enable_smt.set_trigger(CallbackTrigger::Changed);
    check_enable_smt.emit(s, Message::GeneralConfigChange);

    adj_brightness_inp.set_trigger(CallbackTrigger::Changed);
    adj_brightness_inp.emit(s, Message::BrightnessInpChange);
    adj_brightness_sli.set_trigger(CallbackTrigger::Changed);
    adj_brightness_sli.emit(s, Message::BrightnessSliChange);

    adj_contrast_inp.set_trigger(CallbackTrigger::Changed);
    adj_contrast_inp.emit(s, Message::ContrastInpChange);
    adj_contrast_sli.set_trigger(CallbackTrigger::Changed);
    adj_contrast_sli.emit(s, Message::ContrastSliChange);

    but_export_image.emit(s, Message::SaveImage);
    but_locate_image.emit(s, Message::OpenLocation);
    but_save_config.emit(s, Message::SaveConfig);
    but_load_config.emit(s, Message::LoadConfig);
    but_reset_config.emit(s, Message::ResetConfig);

    tab.set_trigger(CallbackTrigger::Changed);
    tab.emit(s,Message::GeneralConfigChange);

    while myapp.wait() {
        match r.recv() {
            Some(msg) => match msg {
                Message::GeneralConfigChange | Message::BrightnessInpChange | Message::BrightnessSliChange | Message::ContrastInpChange | Message::ContrastSliChange=> {
                    match msg  {
                        Message::BrightnessInpChange => {
                            let brightness=str_float_round(&adj_brightness_inp.value(), &brightness_default_value, &brightness_precision);
                            chaos_game_no_save.brightness= if brightness <brightness_min_value || brightness >brightness_max_value {
                                brightness_default_value
                            }else {brightness} as i32;
                            adj_brightness_sli.set_value(chaos_game_no_save.brightness as f64)
                        }
                        Message::BrightnessSliChange => {
                            adj_brightness_inp.set_value(&format!("{}", adj_brightness_sli.value()));
                            chaos_game_no_save.brightness= adj_brightness_sli.value() as i32;
                        }
                        Message::ContrastInpChange => {
                            let contrast=str_float_round(&adj_contrast_inp.value(), &contrast_default_value, &contrast_precision);
                            chaos_game_no_save.contrast= if contrast <contrast_min_value || contrast >contrast_max_value {
                                contrast_default_value
                            }else {contrast} as f32;
                            adj_contrast_sli.set_value(chaos_game_no_save.contrast as f64);
                        }
                        Message::ContrastSliChange => {
                            adj_contrast_inp.set_value(&format!("{}", adj_contrast_sli.value()));
                            chaos_game_no_save.contrast= adj_contrast_sli.value() as f32;
                        }
                        _ => {}
                    };
                    for i in 0..tab_label.len(){
                        if  tab.value().unwrap().label()== tab_label[i]{
                            current_tab_no=i;
                            break;
                        }
                    }
                    chaos_game_no_save.preview_downscale = if check_smooth_preview.is_checked(){
                        2.0
                    }else {
                        1.0
                    };

                    chaos_game_no_save.enable_multithreading = check_enable_smt.is_checked();

                    match current_tab_no {
                        0 =>{                            
                            frame.set_image(Some(fltk_image_gen(&mut chaos_game_config, &chaos_game_no_save)));
                        }
                        1 =>{
                            frame.set_image(Some(fltk_image_gen(&mut julia_config, &chaos_game_no_save)));
                        }
                        2=> {
                            frame.set_image(Some(fltk_image_gen(&mut dragon_config, &chaos_game_no_save)));
                        }
                        3=>{
                            frame.set_image(Some(fltk_image_gen(&mut barnsley_fern_config, &chaos_game_no_save)));
                        }
                        4 =>{
                            mandelbrot_config.cache_img.clear();                 
                            frame.set_image(Some(fltk_image_gen(&mut mandelbrot_config, &chaos_game_no_save)));
                        }
                        _ => {}
                    }
                    redraw();
                },
                //<editor-fold desc="Message: Chaos Game">
                Message::ChaosGameInputScalarChange =>{
                    let scalar=str_float_round(&chs_gm_inp_scalar.value(), &chs_gm_default_scalar_value, &chs_gm_scalar_precision);
                    chaos_game_config.scalar = if scalar < chs_gm_original_scalar_lower || scalar > chs_gm_original_scalar_upper {
                        chs_gm_default_scalar_value
                    } else {scalar};
                    chs_gm_sli_scalar.set_value(chaos_game_config.scalar);
                    frame.set_image(Some(fltk_image_gen(&mut chaos_game_config, &chaos_game_no_save)));
                    redraw();
                },
                Message::ChaosGameSliderScalarChange =>{
                    chs_gm_inp_scalar.set_value(&format!("{}", chs_gm_sli_scalar.value()));
                    chaos_game_config.scalar= chs_gm_sli_scalar.value();
                    frame.set_image(Some(fltk_image_gen(&mut chaos_game_config, &chaos_game_no_save)));
                    redraw();
                },
                Message::ChaosGameVerticesChange => {

                    let mut no_vertices: i32 = chs_gm_inp_no_vertices.value().parse::<i32>().unwrap_or(chs_gm_min_vertices);
                    if no_vertices < chs_gm_min_vertices || no_vertices > chs_gm_max_vertices {
                        no_vertices = chs_gm_min_vertices;
                        chs_gm_inp_no_vertices.set_value(&format!("{}", no_vertices));
                    }
                    if chs_gm_ch_condition.value() == 1 {
                        let mut temp_mod:i32= chs_gm_inp_modifier_i.value().parse::<i32>().unwrap_or(-1);
                        if temp_mod as f64 >= no_vertices as f64 * 0.628 {
                            temp_mod=1;
                            chs_gm_inp_modifier_i.set_value(&format!("{}", temp_mod));
                        }
                        chaos_game_config.condition_modifier_i= temp_mod;
                    }
                    chaos_game_config.no_point = no_vertices;
                    chaos_game_config.include_center = chs_gm_check_incl_center.is_checked();
                    chaos_game_config.include_mid_points = chs_gm_check_incl_mid.is_checked();
                    frame.set_image(Some(fltk_image_gen(&mut chaos_game_config, &chaos_game_no_save)));
                    redraw();
                }
                Message::ChaosGameInputJumpChange => {
                    let jump_ratio=str_float_round(&chs_gm_inp_jump_ratio.value(), &chs_gm_original_jump_lower, &chs_gm_jump_precision);
                    chaos_game_config.distance_ratio = if jump_ratio < chs_gm_original_jump_lower || jump_ratio > chs_gm_original_jump_upper {
                        chs_gm_original_jump_ratio
                    } else {jump_ratio};
                    chs_gm_sli_jump_ratio.set_value(chaos_game_config.distance_ratio);
                    frame.set_image(Some(fltk_image_gen(&mut chaos_game_config, &chaos_game_no_save)));
                    redraw();
                },
                Message::ChaosGameSliderJumpChange => {
                    chs_gm_inp_jump_ratio.set_value(&format!("{}", chs_gm_sli_jump_ratio.value()));
                    chaos_game_config.distance_ratio= chs_gm_sli_jump_ratio.value();
                    frame.set_image(Some(fltk_image_gen(&mut chaos_game_config, &chaos_game_no_save)));
                    redraw();
                },
                Message::ChaosGameChoiceConditionChange => {
                    if chs_gm_ch_condition.value() == 0 ||  chs_gm_ch_condition.value() == 3{
                        chs_gm_inp_modifier_i.hide();
                    }else if chs_gm_ch_condition.value() == 1 {
                        chs_gm_inp_modifier_i.set_label("No. Prev. Vertices to Avoid: ");
                        chs_gm_inp_modifier_i.set_value(&format!("1"));
                        chaos_game_config.condition_modifier_i= chs_gm_inp_modifier_i.value().parse::<i32>().unwrap_or(1);
                        chs_gm_inp_modifier_i.show();
                    }else if chs_gm_ch_condition.value() == 2 {
                        chs_gm_inp_modifier_i.set_label("Percentage of Reselection: ");
                        chs_gm_inp_modifier_i.set_value(&format!("75"));
                        chaos_game_config.condition_modifier_i= chs_gm_inp_modifier_i.value().parse::<i32>().unwrap_or(75);
                        chs_gm_inp_modifier_i.show();
                    }
                    chaos_game_config.condition_id= chs_gm_ch_condition.value();
                    frame.set_image(Some(fltk_image_gen(&mut chaos_game_config, &chaos_game_no_save)));
                    redraw();
                },
                Message::ChaosGameIntModifier1Change => {
                    if chs_gm_ch_condition.value() == 1 {
                        let mut temp_mod:i32= chs_gm_inp_modifier_i.value().parse::<i32>().unwrap_or(-1);
                        if temp_mod<0 || temp_mod as f64 >= chs_gm_inp_no_vertices.value().parse::<f64>().unwrap_or(-1.0) * 0.628 {
                            temp_mod=1;
                            chs_gm_inp_modifier_i.set_value(&format!("{}", temp_mod));
                        }
                        chaos_game_config.condition_modifier_i= temp_mod;
                    }else if chs_gm_ch_condition.value() == 2 {
                        let mut temp_mod:i32= chs_gm_inp_modifier_i.value().parse::<i32>().unwrap_or(-1);
                        if temp_mod<0 || temp_mod > 100 {
                            temp_mod=75;
                            chs_gm_inp_modifier_i.set_value(&format!("{}", temp_mod));
                        }
                        chaos_game_config.condition_modifier_i= temp_mod;
                    }
                    frame.set_image(Some(fltk_image_gen(&mut chaos_game_config, &chaos_game_no_save)));
                    redraw();
                },
                Message::ChaosGameColorRandomSeedChange => {
                    random_state_vec[0] = chs_gm_inp_color_rand_seed.value().parse::<u64>().unwrap_or(1);
                    if random_state_vec[0] >9999999 {
                        random_state_vec[0] =0;
                        chs_gm_inp_color_rand_seed.set_value(&format!("{}", random_state_vec[0]));
                    }
                    chs_gm_color_palette_random =img_generator::generate_color_palette_rgb(random_state_vec[0].clone(), chs_gm_max_vertices*2+1);
                    chaos_game_config.color_palette= chs_gm_color_palette_random.clone();
                    frame.set_image(Some(fltk_image_gen(&mut chaos_game_config, &chaos_game_no_save)));
                    redraw();
                },
                Message::ChaosGameBackgroundChange => {
                    chaos_game_config.background_type= chs_gm_ch_background.value();
                    frame.set_image(Some(fltk_image_gen(&mut chaos_game_config, &chaos_game_no_save)));
                    redraw();
                },
                //</editor-fold>
                //<editor-fold desc="Message: Julia">
                Message::JuliaInputScalarChange => {
                    let scalar =str_float_round(&jul_inp_scalar.value(), &julia_original_scalar_value, &julia_scalar_precision);
                    julia_config.scalar=if scalar <julia_original_scalar_lower || scalar >julia_original_scalar_upper {
                        julia_original_scalar_value
                    }else { scalar };
                    jul_sli_scalar.set_value(julia_config.scalar);
                    frame.set_image(Some(fltk_image_gen(&mut julia_config, &chaos_game_no_save)));
                    redraw();
                },
                Message::JuliaSliderScalarChange => {
                    jul_inp_scalar.set_value(&format!("{}", jul_sli_scalar.value()));
                    julia_config.scalar= jul_sli_scalar.value();
                    frame.set_image(Some(fltk_image_gen(&mut julia_config, &chaos_game_no_save)));
                    redraw();
                }
                Message::JuliaInputCRealChange => {
                    let c_real =str_float_round(&jul_inp_c_real.value(), & julia_original_c_real_value, &julia_c_real_precision);
                    julia_config.c_real=if c_real <julia_original_c_real_lower || c_real >julia_original_c_real_upper {
                        julia_original_c_real_value
                    }else { c_real };
                    jul_sli_c_real.set_value(julia_config.c_real);
                    frame.set_image(Some(fltk_image_gen(&mut julia_config, &chaos_game_no_save)));
                    redraw();
                },
                Message::JuliaSliderCRealChange => {
                    jul_inp_c_real.set_value(&format!("{}", jul_sli_c_real.value()));
                    julia_config.c_real= jul_sli_c_real.value();
                    frame.set_image(Some(fltk_image_gen(&mut julia_config, &chaos_game_no_save)));
                    redraw();
                },
                Message::JuliaInputCImaginaryChange => {
                    let c_imaginary =str_float_round(&jul_inp_c_imaginary.value(), &julia_original_c_imaginary_value, &julia_c_imaginary_precision);
                    julia_config.c_imaginary= if c_imaginary <julia_original_c_imaginary_lower || c_imaginary >julia_original_c_imaginary_upper {
                        julia_original_c_imaginary_value
                    }else { c_imaginary };
                    jul_sli_c_imaginary.set_value(julia_config.c_imaginary);
                    frame.set_image(Some(fltk_image_gen(&mut julia_config, &chaos_game_no_save)));
                    redraw();
                },
                Message::JuliaSliderCImaginaryChange => {
                    jul_inp_c_imaginary.set_value(&format!("{}", jul_sli_c_imaginary.value()));
                    julia_config.c_imaginary= jul_sli_c_imaginary.value();
                    frame.set_image(Some(fltk_image_gen(&mut julia_config, &chaos_game_no_save)));
                    redraw();
                },
                Message::JuliaInputZExponentChange => {
                    let z_pow =str_float_round(&jul_inp_z_pow.value(), &julia_original_z_pow_value, &julia_z_pow_precision);
                    julia_config.z_pow=   if z_pow <julia_original_z_pow_lower || z_pow >julia_original_z_pow_upper {
                        julia_original_z_pow_value
                    }else { z_pow };
                    jul_sli_z_pow.set_value(julia_config.z_pow);
                    frame.set_image(Some(fltk_image_gen(&mut julia_config, &chaos_game_no_save)));
                    redraw();
                },
                Message::JuliaSliderZExponentChange => {
                    jul_inp_z_pow.set_value(&format!("{}", jul_sli_z_pow.value()));
                    julia_config.z_pow= jul_sli_z_pow.value();
                    frame.set_image(Some(fltk_image_gen(&mut julia_config, &chaos_game_no_save)));
                    redraw();
                },
                Message::JuliaBackgroundChange => {
                    julia_config.background_type= jul_ch_background.value();
                    frame.set_image(Some(fltk_image_gen(&mut julia_config, &chaos_game_no_save)));
                    redraw();
                }
                //</editor-fold>
                //<editor-fold desc="Message: Dragon">
                Message::DragonChoiceSelectionChange => {
                    dragon_current_select=dragon_choice_current.value() as usize;
                    for i in 0..dragon_max_no_drg {
                        if i!=dragon_current_select {
                            dragon_check_enabled_vec[i].hide();
                            dragon_ch_position_1_vec[i].hide();
                            dragon_ch_position_2_vec[i].hide();
                            dragon_ch_position_3_vec[i].hide();
                            dragon_ch_rotate_vec[i].hide();
                            dragon_ch_flip_vec[i].hide();
                            dragon_inp_no_iter_vec[i].hide();
                            dragon_sli_no_iter_vec[i].hide();
                            dragon_inp_stroke_width_vec[i].hide();
                            dragon_sli_stroke_width_vec[i].hide();
                        }else {
                            dragon_check_enabled_vec[i].show();
                            dragon_ch_position_1_vec[i].show();
                            dragon_ch_position_2_vec[i].show();
                            dragon_ch_position_3_vec[i].show();
                            dragon_ch_rotate_vec[i].show();
                            dragon_ch_flip_vec[i].show();
                            dragon_inp_no_iter_vec[i].show();
                            dragon_sli_no_iter_vec[i].show();
                            dragon_inp_stroke_width_vec[i].show();
                            dragon_sli_stroke_width_vec[i].show();
                        }
                    }
                    if dragon_check_enabled_vec[dragon_current_select].is_checked()&&dragon_current_select!=0{
                        dragon_redundant_label.activate()
                    }else {
                        dragon_redundant_label.deactivate()
                    }
                    redraw()
                }
                Message::DragonCheckEnableChange => {
                    let mut dragon_value_enabled_vec:Vec<bool>=Vec::new();
                    for i in 0..dragon_max_no_drg {
                        dragon_value_enabled_vec.push(dragon_check_enabled_vec[i].is_checked())
                    }
                    for i in 1..dragon_max_no_drg {

                        if !dragon_check_enabled_vec[i].is_checked() {
                            dragon_ch_position_1_vec[i].deactivate();
                            dragon_ch_position_2_vec[i].deactivate();
                            dragon_ch_position_3_vec[i].deactivate();
                            dragon_ch_rotate_vec[i].deactivate();
                            dragon_ch_flip_vec[i].deactivate();
                            dragon_inp_no_iter_vec[i].deactivate();
                            dragon_sli_no_iter_vec[i].deactivate();
                            dragon_inp_stroke_width_vec[i].deactivate();
                            dragon_sli_stroke_width_vec[i].deactivate();
                        } else {
                            dragon_ch_position_1_vec[i].activate();
                            dragon_ch_position_2_vec[i].activate();
                            dragon_ch_position_3_vec[i].activate();
                            dragon_ch_rotate_vec[i].activate();
                            dragon_ch_flip_vec[i].activate();
                            dragon_inp_no_iter_vec[i].activate();
                            dragon_sli_no_iter_vec[i].activate();
                            dragon_inp_stroke_width_vec[i].activate();
                            dragon_sli_stroke_width_vec[i].activate();
                        }
                    }
                    let mut dragon_value_position_3_vec:Vec<i32>=Vec::new();
                    for i in 0..dragon_max_no_drg {
                        dragon_value_position_3_vec.push(dragon_ch_position_3_vec[i].value());
                    }
                    for i in 1..dragon_max_no_drg {
                        for j in 0..dragon_max_no_drg {
                            if i<=j || !dragon_check_enabled_vec[j].is_checked() {
                                if let Some(mut item) = dragon_ch_position_3_vec[i].find_item(&format!("{}", Ordinal(j + 1).to_string())) {
                                    item.hide();
                                }
                            }else {
                                if let Some(mut item) = dragon_ch_position_3_vec[i].find_item(&format!("{}", Ordinal(j + 1).to_string())) {
                                    item.show();
                                }
                            }
                        }
                        if !dragon_check_enabled_vec[dragon_value_position_3_vec[i] as usize].is_checked(){
                            dragon_value_position_3_vec[i]=0;
                            dragon_ch_position_3_vec[i].set_value(0);

                        }
                    }
                    if dragon_check_enabled_vec[dragon_current_select].is_checked()&&dragon_current_select!=0{
                        dragon_redundant_label.activate()
                    }else {
                        dragon_redundant_label.deactivate()
                    }

                    let mut dragon_value_position_3_vec:Vec<i32>=Vec::new();
                    for i in 0..dragon_max_no_drg {
                        dragon_value_position_3_vec.push(dragon_ch_position_3_vec[i].value());
                    }
                    dragon_config.enabled=dragon_value_enabled_vec;
                    dragon_config.position_3= dragon_value_position_3_vec;
                    frame.set_image(Some(fltk_image_gen(&mut dragon_config, &chaos_game_no_save)));
                    redraw()
                }
                Message::DragonConfigChange => {
                    let mut dragon_value_position_1_vec:Vec<i32>=Vec::new();
                    let mut dragon_value_position_2_vec:Vec<i32>=Vec::new();
                    let mut dragon_value_position_3_vec:Vec<i32>=Vec::new();
                    for i in 0..dragon_max_no_drg {
                        dragon_value_position_1_vec.push(dragon_ch_position_1_vec[i].value());
                        dragon_value_position_2_vec.push(dragon_ch_position_2_vec[i].value());
                        dragon_value_position_3_vec.push(dragon_ch_position_3_vec[i].value());
                    }

                    dragon_config.position_1= dragon_value_position_1_vec;
                    dragon_config.position_2= dragon_value_position_2_vec;
                    dragon_config.position_3= dragon_value_position_3_vec;
                    frame.set_image(Some(fltk_image_gen(&mut dragon_config, &chaos_game_no_save)));
                    redraw()
                }
                Message::DragonDirectionChange => {
                    let mut dragon_value_rotate_vec:Vec<i32>=Vec::new();
                    let mut dragon_value_flip_vec:Vec<i32>=Vec::new();
                    for i in 0..dragon_max_no_drg {
                        dragon_value_rotate_vec.push(dragon_ch_rotate_vec[i].value());
                        dragon_value_flip_vec.push(dragon_ch_flip_vec[i].value());
                    }
                    dragon_config.rotate= dragon_value_rotate_vec;
                    dragon_config.flip= dragon_value_flip_vec;
                    frame.set_image(Some(fltk_image_gen(&mut dragon_config, &chaos_game_no_save)));
                    redraw()
                }
                Message::DragonInputNoIterationChange => {
                    let mut dragon_value_no_iter_vec:Vec<i32>=Vec::new();
                    for i in 0..6 {
                        let no_iteration =str_float_round(&dragon_inp_no_iter_vec[i].value(), &dragon_original_no_iter, &dragon_no_iter_precision);
                        dragon_value_no_iter_vec.push(if no_iteration < dragon_no_iter_lower || no_iteration > dragon_no_iter_upper {
                            dragon_original_no_iter as i32
                        }else{ no_iteration as i32});
                        dragon_sli_no_iter_vec[i].set_value(dragon_value_no_iter_vec[i] as f64);
                    }
                    dragon_config.no_iter = dragon_value_no_iter_vec;
                    frame.set_image(Some(fltk_image_gen(&mut dragon_config, &chaos_game_no_save)));
                    redraw()
                }
                Message::DragonSliderNoIterationChange => {
                    let mut dragon_value_no_iter_vec:Vec<i32>=Vec::new();
                    for i in 0..6 {
                        let no_iteration =dragon_sli_no_iter_vec[i].value();
                        dragon_inp_no_iter_vec[i].set_value(&format!("{}", no_iteration));
                        dragon_value_no_iter_vec.push(no_iteration as i32);
                    }
                    dragon_config.no_iter= dragon_value_no_iter_vec;
                    frame.set_image(Some(fltk_image_gen(&mut dragon_config, &chaos_game_no_save)));
                    redraw()
                }
                Message::DragonInputLineWidthChange => {
                    let mut dragon_value_stroke_width_vec:Vec<f64>=Vec::new();
                    for i in 0..6 {
                        let line_width =str_float_round(&dragon_inp_stroke_width_vec[i].value(), &dragon_original_stroke_width, &dragon_stroke_width_precision);
                        dragon_value_stroke_width_vec.push(if line_width < dragon_stroke_width_lower || line_width > dragon_stroke_width_upper {
                            dragon_original_no_iter
                        }else{ line_width});
                        dragon_sli_stroke_width_vec[i].set_value(dragon_value_stroke_width_vec[i]);
                    }
                    dragon_config.stroke_width= dragon_value_stroke_width_vec;
                    frame.set_image(Some(fltk_image_gen(&mut dragon_config, &chaos_game_no_save)));
                    redraw()
                },
                Message::DragonSliderLineWidthChange => {
                    let mut dragon_value_stroke_width_vec:Vec<f64>=Vec::new();
                    for i in 0..6 {
                        let line_width =dragon_sli_stroke_width_vec[i].value();
                        dragon_inp_stroke_width_vec[i].set_value(&format!("{}", line_width));
                        dragon_value_stroke_width_vec.push(line_width);
                    }
                    dragon_config.stroke_width= dragon_value_stroke_width_vec;
                    frame.set_image(Some(fltk_image_gen(&mut dragon_config, &chaos_game_no_save)));
                    redraw()
                }
                Message::DragonColorRandomSeedChange => {
                    random_state_vec[2] = dragon_inp_color_rand_seed.value().parse::<u64>().unwrap_or(1);
                    if random_state_vec[2] >9999999 {
                        random_state_vec[2] =2;
                        dragon_inp_color_rand_seed.set_value(&format!("{}", random_state_vec[2]));
                    }
                    dragon_color_palette_random =img_generator::generate_color_palette_rgb(random_state_vec[2].clone(), dragon_max_no_drg as i32);
                    dragon_config.color_palette= dragon_color_palette_random.clone();
                    frame.set_image(Some(fltk_image_gen(&mut dragon_config, &chaos_game_no_save)));
                    redraw();
                },
                Message::DragonBackgroundChange => {
                    dragon_config.background_type= dragon_ch_background.value();
                    frame.set_image(Some(fltk_image_gen(&mut dragon_config, &chaos_game_no_save)));
                    redraw()

                },
                //</editor-fold>

                //<editor-fold desc="Message: Fern">
                Message::FernChoiceSelectionChange => {
                    fern_current_select=fern_choice_current.value() as usize;
                    for i in 0..fern_max_no_weight {
                        for j in 0..fern_no_coefficient {
                            if i!=fern_current_select {
                                fern_inp_co[i][j].hide();
                                fern_sli_co[i][j].hide();
                            }else {
                                fern_inp_co[i][j].show();
                                fern_sli_co[i][j].show();
                            }
                        }
                        if i!=fern_current_select {
                            fern_check_enable_vec[i].hide();
                            fern_inp_prob[i].hide();
                            fern_sli_prob[i].hide();
                        }else {
                            fern_check_enable_vec[i].show();
                            fern_inp_prob[i].show();
                            fern_sli_prob[i].show();
                        }

                    }
                    redraw()
                }
                Message::FernCheckEnableChange => {
                    let mut fern_value_enable_vec:Vec<bool>=Vec::new();
                    for i in 0..fern_max_no_weight {
                        fern_value_enable_vec.push(fern_check_enable_vec[i].is_checked());
                    }
                    for i in 1..fern_max_no_weight {

                        for j in 0..fern_no_coefficient {
                            if !fern_check_enable_vec[i].is_checked()  {
                                fern_inp_co[i][j].deactivate();
                                fern_sli_co[i][j].deactivate();
                            }else {
                                fern_inp_co[i][j].activate();
                                fern_sli_co[i][j].activate();
                            }
                        }

                        if !fern_check_enable_vec[i].is_checked() {
                            fern_inp_prob[i].deactivate();
                            fern_sli_prob[i].deactivate();
                        }else {
                            fern_inp_prob[i].activate();
                            fern_sli_prob[i].activate();
                        }

                    }
                    barnsley_fern_config.enabled=fern_value_enable_vec;
                    frame.set_image(Some(fltk_image_gen(&mut barnsley_fern_config, &chaos_game_no_save)));
                    redraw()
                }
                Message::FernInputCoefficientChange => {

                    let mut fern_value_co:Vec<Vec<f64>>=Vec::new();
                    for i in 0..fern_max_no_weight {
                        let mut fern_value_co_temp:Vec<f64>=Vec::new();
                        for j in 0..fern_no_coefficient {
                            let fern_co=str_float_round(&fern_inp_co[i][j].value(), &fern_original_co_vec[i][j], &fern_co_precision);
                            fern_value_co_temp.push(if fern_co < fern_co_lower || fern_co>fern_co_upper {
                                fern_original_co_vec[i][j]
                            }else {
                                fern_co
                            });
                            fern_sli_co[i][j].set_value(fern_value_co_temp[j]);
                        }
                        fern_value_co.push(fern_value_co_temp);
                    }
                    barnsley_fern_config.coefficient=fern_value_co;
                    frame.set_image(Some(fltk_image_gen(&mut barnsley_fern_config, &chaos_game_no_save)));
                    redraw();

                }
                Message::FernSliderCoefficientChange => {
                    let mut fern_value_co:Vec<Vec<f64>>=Vec::new();
                    for i in 0..fern_max_no_weight {
                        let mut fern_value_co_temp:Vec<f64>=Vec::new();
                        for j in 0..fern_no_coefficient {
                            fern_inp_co[i][j].set_value(&format!("{}", fern_sli_co[i][j].value()));
                            fern_value_co_temp.push(fern_sli_co[i][j].value());
                        }
                        fern_value_co.push(fern_value_co_temp);
                    }
                    barnsley_fern_config.coefficient=fern_value_co;
                    frame.set_image(Some(fltk_image_gen(&mut barnsley_fern_config, &chaos_game_no_save)));
                    redraw()

                }
                Message::FernInputProbabilityChange => {
                    let mut fern_value_prob:Vec<i32>=Vec::new();
                    for i in 0..fern_max_no_weight {
                        let fern_pro =str_float_round(&fern_inp_prob[i].value(), &(fern_original_prob_vec[i] as f64), &fern_prob_precision);
                        fern_value_prob.push(if fern_pro < fern_prob_lower || fern_pro >fern_prob_upper {
                            fern_original_prob_vec[i]
                        }else {
                            fern_pro as i32
                        });
                        fern_sli_prob[i].set_value(fern_value_prob[i] as f64);
                    }
                    barnsley_fern_config.probability_score=fern_value_prob;
                    frame.set_image(Some(fltk_image_gen(&mut barnsley_fern_config, &chaos_game_no_save)));
                    redraw();
                }
                Message::FernSliderProbabilityChange  => {
                    let mut fern_value_prob:Vec<i32>=Vec::new();
                    for i in 0..fern_max_no_weight {
                        fern_inp_prob[i].set_value(&format!("{}", fern_sli_prob[i].value()));
                        fern_value_prob.push(fern_sli_prob[i].value() as i32);
                    }
                    barnsley_fern_config.probability_score=fern_value_prob;
                    frame.set_image(Some(fltk_image_gen(&mut barnsley_fern_config, &chaos_game_no_save)));
                    redraw()
                }
                Message::FernInputSampleChange => {
                    let sample =str_float_round(&fern_inp_sample.value(), &fern_original_sample_value, & fern_sample_precision);
                    barnsley_fern_config.sample_multiplier =if sample <fern_original_sample_lower || sample >fern_original_sample_upper{
                        fern_original_sample_value
                    }else { sample  } as u32;
                    fern_sli_sample.set_value(barnsley_fern_config.sample_multiplier as f64);
                    frame.set_image(Some(fltk_image_gen(&mut barnsley_fern_config, &chaos_game_no_save)));
                    redraw()
                },
                Message::FernSliderSampleChange => {
                    fern_inp_sample.set_value(&format!("{}", fern_sli_sample.value()));
                    barnsley_fern_config.sample_multiplier = fern_sli_sample.value() as u32;
                    frame.set_image(Some(fltk_image_gen(&mut barnsley_fern_config, &chaos_game_no_save)));
                    redraw()
                }
                Message::FernCheckWeightAdjacentChange => {
                    barnsley_fern_config.weight_adj= fern_check_weight_adj.is_checked();
                    frame.set_image(Some(fltk_image_gen(&mut barnsley_fern_config, &chaos_game_no_save)));
                    redraw()
                }
                Message::FernBackgroundChange =>{
                    barnsley_fern_config.background_type= fern_ch_background.value();
                    frame.set_image(Some(fltk_image_gen(&mut barnsley_fern_config, &chaos_game_no_save)));
                    redraw()
                }
                //</editor-fold>

                //<editor-fold desc="Message: Mandelbrot Action">
                Message::MandelbrotInputScalarChange => {
                    let scalar=str_float_round(&mandelbrot_inp_scalar.value(), &mandelbrot_default_scalar_value, &mandelbrot_scalar_precision);
                    mandelbrot_config.scalar = if scalar < mandelbrot_original_scalar_lower || scalar > mandelbrot_original_scalar_upper {
                        mandelbrot_default_scalar_value
                    } else {scalar};
                    mandelbrot_sli_scalar.set_value(mandelbrot_config.scalar);
                    frame.set_image(Some(fltk_image_gen(&mut mandelbrot_config, &chaos_game_no_save)));
                    redraw();
                }
                Message::MandelbrotSliderScalarChange => {
                    mandelbrot_inp_scalar.set_value(&format!("{}", mandelbrot_sli_scalar.value()));
                    mandelbrot_config.scalar= mandelbrot_sli_scalar.value();
                    frame.set_image(Some(fltk_image_gen(&mut mandelbrot_config, &chaos_game_no_save)));
                    redraw();
                }
                Message::MandelbrotInputXLocChange => {
                    let x_loc =str_float_round(&mandelbrot_inp_x_loc.value(), &mandelbrot_default_x_value, &mandelbrot_x_precision);
                    mandelbrot_config.min_x_percentage = if x_loc < mandelbrot_original_x_lower || x_loc > mandelbrot_original_x_upper {
                        mandelbrot_default_x_value
                    } else { x_loc };
                    mandelbrot_sli_x_loc.set_value(mandelbrot_config.min_x_percentage);
                    frame.set_image(Some(fltk_image_gen(&mut mandelbrot_config, &chaos_game_no_save)));
                    redraw();
                }
                Message::MandelbrotSliderXLocChange => {
                    mandelbrot_inp_x_loc.set_value(&format!("{}", mandelbrot_sli_x_loc.value()));
                    mandelbrot_config.min_x_percentage= mandelbrot_sli_x_loc.value();
                    frame.set_image(Some(fltk_image_gen(&mut mandelbrot_config, &chaos_game_no_save)));
                    redraw();
                }
                Message::MandelbrotInputYLocChange => {
                    let y_loc =str_float_round(&mandelbrot_inp_y_loc.value(), &mandelbrot_default_y_value, &mandelbrot_y_precision);
                    mandelbrot_config.min_y_percentage = if y_loc < mandelbrot_original_y_lower || y_loc > mandelbrot_original_y_upper {
                        mandelbrot_default_y_value
                    } else { y_loc };
                    mandelbrot_sli_y_loc.set_value(mandelbrot_config.min_y_percentage);
                    frame.set_image(Some(fltk_image_gen(&mut mandelbrot_config, &chaos_game_no_save)));
                    redraw();
                }
                Message::MandelbrotSliderYLocChange => {
                    mandelbrot_inp_y_loc.set_value(&format!("{}", mandelbrot_sli_y_loc.value()));
                    mandelbrot_config.min_y_percentage = mandelbrot_sli_y_loc.value();
                    frame.set_image(Some(fltk_image_gen(&mut mandelbrot_config, &chaos_game_no_save)));
                    redraw();
                }
                Message::MandelbrotShowChange => {
                    mandelbrot_config.show_zoom= mandelbrot_ck_show_zoom.is_checked();
                    mandelbrot_config.show_selector= mandelbrot_ck_show_selector.is_checked();
                    frame.set_image(Some(fltk_image_gen(&mut mandelbrot_config, &chaos_game_no_save)));
                    redraw();
                }
                Message::MandelbrotZoomIn => {
                    mandelbrot_config.cache_img.clear();

                    let img_width_original_ratio =1_f64;
                    let (min_x,min_y, big_img_size)=if mandelbrot_config.history.len()==0 {
                        mandelbrot_config.history.push((0.0,0.0,1.0));
                        (0_f64, 0_f64, img_width_original_ratio)

                    }else {
                        let mut total_scalar =1.0;

                        if !mandelbrot_config.lock_zoom_in{
                            mandelbrot_config.history.push((mandelbrot_config.min_x_percentage, mandelbrot_config.min_y_percentage, mandelbrot_config.scalar));
                        }

                        //let mut over_break=false;
                        let (mut total_x_vec,mut total_y_vec)=(Vec::new(),Vec::new());

                        for i in 0..mandelbrot_config.history.len(){
                            let mut multiplier=mandelbrot_config.history[i].2;
                            if !mandelbrot_config.lock_zoom_in {
                                if mandelbrot_config.max_zoom / mandelbrot_config.history[i].2 <= total_scalar {
                                    multiplier = mandelbrot_config.max_zoom  / total_scalar;
                                    mandelbrot_config.lock_zoom_in=true;
                                }
                            }

                            total_scalar *= multiplier;

                            if i==total_x_vec.len(){
                                let min_x_now=img_width_original_ratio as f64*(1.0-1.0/multiplier)*(mandelbrot_config.history[i].0)/100.0;
                                let min_y_now=img_width_original_ratio as f64*(1.0-1.0/multiplier)*(mandelbrot_config.history[i].1)/100.0;

                                total_x_vec.push(min_x_now);
                                total_y_vec.push(min_y_now);
                            }

                            for j in 0..total_x_vec.len(){
                                total_x_vec[j]=total_x_vec[j] as f64*multiplier;
                                total_y_vec[j]=total_y_vec[j] as f64*multiplier;
                            }

                        }

                        let (mut min_x,mut min_y)=(0.0,0.0);
                        for i in 0..total_x_vec.len(){
                            min_x+=total_x_vec[i];
                            min_y+=total_y_vec[i];
                        }

                        let size=img_width_original_ratio as f64* total_scalar;

                        (min_x , min_y, size )
                    };
                    mandelbrot_config.size_ratio.push((min_x,min_y, big_img_size));
                    if mandelbrot_config.lock_zoom_in {
                        mandelbrot_but_zoom_in.deactivate();
                    }
                    mandelbrot_but_zoom_out.activate();

                    frame.set_image(Some(fltk_image_gen(&mut mandelbrot_config, &chaos_game_no_save)));
                    redraw();
                },
                Message::MandelbrotZoomOut => {
                    if mandelbrot_config.history.len()>1 {
                        mandelbrot_config.cache_img.clear();
                        let h_len = mandelbrot_config.history.len() - 1;
                        mandelbrot_config.min_x_percentage = mandelbrot_config.history[h_len].0;
                        mandelbrot_config.min_y_percentage = mandelbrot_config.history[h_len].1;
                        mandelbrot_config.scalar = mandelbrot_config.history[h_len].2;
                        mandelbrot_config.history.pop();
                        mandelbrot_config.size_ratio.pop();

                        mandelbrot_inp_scalar.set_value(&format!("{}", mandelbrot_config.scalar));
                        mandelbrot_sli_scalar.set_value(mandelbrot_config.scalar);

                        mandelbrot_inp_x_loc.set_value(&format!("{}", mandelbrot_config.min_x_percentage));
                        mandelbrot_sli_x_loc.set_value(mandelbrot_config.min_x_percentage);

                        mandelbrot_inp_y_loc.set_value(&format!("{}", mandelbrot_config.min_y_percentage));
                        mandelbrot_sli_y_loc.set_value(mandelbrot_config.min_y_percentage);
                        frame.set_image(Some(fltk_image_gen(&mut mandelbrot_config, &chaos_game_no_save)));
                        redraw();
                    }
                    if mandelbrot_config.lock_zoom_in {
                        mandelbrot_config.lock_zoom_in=false;
                        mandelbrot_but_zoom_in.activate();
                    }
                    if mandelbrot_config.history.len()==1 {
                        mandelbrot_but_zoom_out.deactivate();
                    }

                },
                Message::MandelbrotInputIterationChange => {
                    mandelbrot_config.cache_img.clear();
                    let max_iteration =str_float_round(&mandelbrot_inp_max_inter.value(), &mandelbrot_default_max_inter_value, &mandelbrot_max_inter_precision);
                    mandelbrot_config.max_iteration = if max_iteration < mandelbrot_original_max_inter_lower || max_iteration > mandelbrot_original_max_inter_upper {
                        mandelbrot_default_max_inter_value
                    } else { max_iteration  }as i32;
                    mandelbrot_sli_max_inter.set_value(mandelbrot_config.max_iteration as f64);
                    let mut mandelbrot_color_config_vec=Vec::new();
                    for i in 0..6{
                        mandelbrot_color_config_vec.push(mandelbrot_sli_color_1_config_vec[i].value())
                    }
                    mandelbrot_config.color_palette=img_generator::generate_color_palette_hsv(&(mandelbrot_sli_max_inter.value().round() as u32), &mandelbrot_color_config_vec);
                    frame.set_image(Some(fltk_image_gen(&mut mandelbrot_config, &chaos_game_no_save)));
                    redraw();
                }
                Message::MandelbrotSliderIterationChange=>{
                    mandelbrot_config.cache_img.clear();
                    mandelbrot_inp_max_inter.set_value(&format!("{}", mandelbrot_sli_max_inter.value()));
                    mandelbrot_config.max_iteration = mandelbrot_sli_max_inter.value() as i32;
                    let mut mandelbrot_color_config_vec=Vec::new();
                    for i in 0..6{
                        mandelbrot_color_config_vec.push(mandelbrot_sli_color_1_config_vec[i].value())
                    }
                    mandelbrot_config.color_palette=img_generator::generate_color_palette_hsv(&(mandelbrot_sli_max_inter.value().round() as u32), &mandelbrot_color_config_vec);
                    frame.set_image(Some(fltk_image_gen(&mut mandelbrot_config, &chaos_game_no_save)));
                    redraw();
                }
                Message::MandelbrotInputZChange => {
                    mandelbrot_config.cache_img.clear();
                    let max_z =str_float_round(&mandelbrot_inp_max_z.value(), &mandelbrot_default_max_z_value, &mandelbrot_max_z_precision);
                    mandelbrot_config.max_z = if max_z < mandelbrot_original_max_z_lower || max_z > mandelbrot_original_max_z_upper {
                        mandelbrot_default_max_z_value
                    } else { max_z };
                    mandelbrot_sli_max_z.set_value(mandelbrot_config.max_z as f64);
                    frame.set_image(Some(fltk_image_gen(&mut mandelbrot_config, &chaos_game_no_save)));
                    redraw();
                }
                Message::MandelbrotSliderZChange=>{
                    mandelbrot_config.cache_img.clear();
                    mandelbrot_inp_max_z.set_value(&format!("{}", mandelbrot_sli_max_z.value()));
                    mandelbrot_config.max_z = mandelbrot_sli_max_z.value();
                    frame.set_image(Some(fltk_image_gen(&mut mandelbrot_config, &chaos_game_no_save)));
                    redraw();
                }
                Message::MandelbrotColoringTypeChange=>{
                    mandelbrot_config.cache_img.clear();
                    mandelbrot_config.coloring_type=mandelbrot_ch_coloring.value();
                    if mandelbrot_config.coloring_type==1 {
                        _mandelbrot_lab_hue_.hide();
                        _mandelbrot_lab_sat_.hide();
                        _mandelbrot_lab_val_.hide();
                        for i in 0..mandelbrot_inp_color_1_config_vec.len(){
                            mandelbrot_inp_color_1_config_vec[i].hide();
                            mandelbrot_sli_color_1_config_vec[i].hide();
                        }

                        for i in 0..mandelbrot_inp_color_2_config_vec.len() {
                            mandelbrot_inp_color_2_config_vec[i].show();
                            mandelbrot_sli_color_2_config_vec[i].show();
                        }
                    }else {
                        _mandelbrot_lab_hue_.show();
                        _mandelbrot_lab_sat_.show();
                        _mandelbrot_lab_val_.show();
                        for i in 0..mandelbrot_inp_color_1_config_vec.len(){
                            mandelbrot_inp_color_1_config_vec[i].show();
                            mandelbrot_sli_color_1_config_vec[i].show();
                        }

                        for i in 0..mandelbrot_inp_color_2_config_vec.len() {
                            mandelbrot_inp_color_2_config_vec[i].hide();
                            mandelbrot_sli_color_2_config_vec[i].hide();
                        }
                    }
                    frame.set_image(Some(fltk_image_gen(&mut mandelbrot_config, &chaos_game_no_save)));
                    redraw();
                }
                Message::MandelbrotInputColor2Change=>{
                    mandelbrot_config.cache_img.clear();
                    for i in 0..mandelbrot_inp_color_2_config_vec.len() {
                        let color_2_val = str_float_round(&mandelbrot_inp_color_2_config_vec[i].value(), &mandelbrot_color_2_config_original_vec[i], &mandelbrot_color_2_config_precision_vec[i]);
                        mandelbrot_config.color_type_2_coff[i] = if color_2_val < mandelbrot_color_2_config_lower_vec[i] || color_2_val > mandelbrot_color_2_config_upper_vec[i] {
                            mandelbrot_color_2_config_original_vec[i]
                        } else { color_2_val };
                        mandelbrot_sli_color_2_config_vec[i].set_value( mandelbrot_config.color_type_2_coff[i]);
                    }
                    frame.set_image(Some(fltk_image_gen(&mut mandelbrot_config, &chaos_game_no_save)));
                    redraw();
                }
                Message::MandelbrotSliderColor2Change=>{
                    mandelbrot_config.cache_img.clear();
                    for i in 0..mandelbrot_inp_color_2_config_vec.len() {
                        mandelbrot_inp_color_2_config_vec[i].set_value(&format!("{}", mandelbrot_sli_color_2_config_vec[i].value()));
                        mandelbrot_config.color_type_2_coff[i] = mandelbrot_sli_color_2_config_vec[i].value();
                    }
                    frame.set_image(Some(fltk_image_gen(&mut mandelbrot_config, &chaos_game_no_save)));
                    redraw();
                }
                //</editor-fold>

                //<editor-fold desc="Message: Color Change Functions">
                Message::ChaosGameCustomColorUsageChange | Message::DragonCustomColorUsageChange => {
                    let (check_custom_color,inp_color_rand_seed,but_edit_color, but_load_color)=
                        match msg {
                            Message::DragonCustomColorUsageChange => {
                                (&mut dragon_ck_custom_color, &mut dragon_inp_color_rand_seed, &mut dragon_but_edit_color, &mut dragon_but_load_color)
                            }
                            Message::ChaosGameCustomColorUsageChange | _ => {
                                (&mut chs_gm_check_custom_color,&mut chs_gm_inp_color_rand_seed,&mut chs_gm_but_edit_color,&mut chs_gm_but_load_color)
                            }
                        };
                    if (*check_custom_color).is_checked(){
                        (*inp_color_rand_seed).deactivate();
                        (*but_edit_color).activate();
                        (*but_load_color).activate();

                    }else{
                        (*inp_color_rand_seed).activate();
                        (*but_edit_color).deactivate();
                        (*but_load_color).deactivate();

                    }
                    match msg {
                        Message::DragonCustomColorUsageChange => {
                            if (*check_custom_color).is_checked(){
                                dragon_config.color_palette= dragon_color_palette_custom.clone();
                            }else {
                                dragon_config.color_palette= dragon_color_palette_random.clone();
                            }
                            dragon_config.add_transparency= dragon_ck_transparent_color.is_checked();
                            dragon_config.add_gradient= dragon_ck_gradient_color.is_checked();
                            dragon_config.lighten = dragon_ck_lighten.is_checked();
                            frame.set_image(Some(fltk_image_gen(&mut dragon_config, &chaos_game_no_save)));
                            redraw();
                        }
                        Message::ChaosGameCustomColorUsageChange | _ => {
                            if (*check_custom_color).is_checked(){
                                chaos_game_config.color_palette= chs_gm_color_palette_custom.clone();
                            }else {
                                chaos_game_config.color_palette= chs_gm_color_palette_random.clone();
                            }
                            chaos_game_config.add_transparency= chs_gm_check_transparent_color.is_checked();
                            frame.set_image(Some(fltk_image_gen(&mut chaos_game_config, &chaos_game_no_save)));
                        }
                    };

                    redraw();

                },
                Message::ChaosGameEditColor | Message::DragonEditColor => {
                    let path_no =
                        match msg {
                            Message::DragonEditColor => {2}
                            Message::ChaosGameEditColor|_ => {0}
                        };

                    let open_color_data=open::that(&*COLOR_DATA_FILE_PATH[path_no]);
                    match open_color_data {
                        Ok(_) =>{
                            text_display_history=append_history(text_display_history,"Open Custom Color Data File");
                            text_display.buffer().unwrap().set_text(&text_display_gen(&text_display_history));
                        }
                        Err(_) => {
                            text_display_history=append_history(text_display_history,"Unable to Open Custom Color Data File");
                            text_display.buffer().unwrap().set_text(&text_display_gen(&text_display_history));
                        }
                    }

                },
                Message::ChaosGameLoadColor | Message::DragonLoadColor =>{
                    let (no_color,seed,path)=
                        match msg {
                            Message::DragonLoadColor => {(dragon_max_no_drg as i32,random_state_vec[2].clone(),2)}
                            Message::ChaosGameLoadColor|_ => {(chs_gm_max_vertices*2+1, random_state_vec[0].clone(), 0)}
                        };
                    let (color_palette_random,color_palette_custom,log_string)=color_palette_rgb_handler(no_color,seed,path);
                    if log_string!= ""{
                        text_display_history=append_history(text_display_history, &*log_string);
                        text_display.buffer().unwrap().set_text(&text_display_gen(&text_display_history));
                    }
                    match msg {
                        Message::ChaosGameLoadColor => {
                            chs_gm_color_palette_random =color_palette_random;
                            chs_gm_color_palette_custom =color_palette_custom;
                            chaos_game_config.color_palette= chs_gm_color_palette_custom.clone();
                            frame.set_image(Some(fltk_image_gen(&mut chaos_game_config, &chaos_game_no_save)));

                        }
                        Message::DragonLoadColor => {
                            dragon_color_palette_random =color_palette_random;
                            dragon_color_palette_custom =color_palette_custom;
                            dragon_config.color_palette= dragon_color_palette_custom.clone();
                            frame.set_image(Some(fltk_image_gen(&mut dragon_config, &chaos_game_no_save)));
                        }
                        _ =>{}
                    }
                    redraw();
                },
                Message::JuliaInputColorChange | Message::FernInputColorChange | Message::MandelbrotInputColor1Change => {
                    let mut color_config_val_vec:Vec<f64>=Vec::new();
                    let (input_color_config_vec, color_config_original_vec,color_config_lower_vec,color_config_upper_vec,color_config_precision_vec)=
                        match msg {
                            Message::FernInputColorChange => {(&fern_inp_color_config_vec,&fern_color_config_original_vec,&fern_color_config_lower_vec,&fern_color_config_upper_vec,&fern_color_config_precision_vec)}
                            Message::MandelbrotInputColor1Change => {(&mandelbrot_inp_color_1_config_vec, &mandelbrot_color_1_config_original_vec, &mandelbrot_color_1_config_lower_vec, &mandelbrot_color_1_config_upper_vec, &mandelbrot_color_1_config_precision_vec)}
                            Message::JuliaInputColorChange | _ => {(&jul_inp_color_config_vec,&jul_color_config_original_vec,&jul_color_config_lower_vec,&jul_color_config_upper_vec,&jul_color_config_precision_vec)}
                        };
                    for i in 0..6 {
                        let color_config_val=str_float_round(&input_color_config_vec[i].value(), &color_config_original_vec[i], &color_config_precision_vec[i]);
                        color_config_val_vec.push(if color_config_val <(*color_config_lower_vec)[i] || color_config_val >(*color_config_upper_vec)[i] {
                            color_config_original_vec[i]
                        }else{color_config_val});
                        jul_sli_color_config_vec[i].set_value(color_config_val_vec[i]);
                    }
                    match msg {
                        Message::FernInputColorChange => {
                            barnsley_fern_config.color_palette=img_generator::generate_color_palette_hsv(&fern_no_color, &color_config_val_vec);
                            frame.set_image(Some(fltk_image_gen(&mut barnsley_fern_config, &chaos_game_no_save)));
                        }
                        Message::MandelbrotInputColor1Change => {
                            mandelbrot_config.cache_img.clear();
                            mandelbrot_config.color_palette=img_generator::generate_color_palette_hsv(&(mandelbrot_sli_max_inter.value().round() as u32), &color_config_val_vec);
                            frame.set_image(Some(fltk_image_gen(&mut mandelbrot_config, &chaos_game_no_save)));
                        }
                        Message::JuliaInputColorChange | _ => {
                            julia_config.color_palette=img_generator::generate_color_palette_hsv(&jul_no_color, &color_config_val_vec);
                            frame.set_image(Some(fltk_image_gen(&mut julia_config, &chaos_game_no_save)));
                        }
                    };

                    redraw()
                },
                Message::JuliaSliderColorChange | Message::FernSliderColorChange | Message::MandelbrotSliderColor1Change=> {
                    let mut color_config_val_vec:Vec<f64>=Vec::new();
                    let (input_color_config_vec, sli_color_config_vec) =
                        match msg {
                            Message::FernSliderColorChange => {(&fern_inp_color_config_vec,&fern_sli_color_config_vec)}
                            Message::MandelbrotSliderColor1Change => {(&mandelbrot_inp_color_1_config_vec, &mandelbrot_sli_color_1_config_vec)}
                            Message::JuliaSliderColorChange | _ => {(&jul_inp_color_config_vec,&jul_sli_color_config_vec)}
                        };
                    for i in 0..6 {
                        let color_config_val:f64=sli_color_config_vec[i].value();
                        input_color_config_vec[i].set_value(&format!("{}", color_config_val));
                        color_config_val_vec.push(color_config_val);
                    }
                    match msg {
                        Message::FernSliderColorChange => {
                            barnsley_fern_config.color_palette=img_generator::generate_color_palette_hsv(&fern_no_color, &color_config_val_vec);
                            frame.set_image(Some(fltk_image_gen(&mut barnsley_fern_config, &chaos_game_no_save)));
                        }
                        Message::MandelbrotSliderColor1Change => {
                            mandelbrot_config.cache_img.clear();
                            mandelbrot_config.color_palette=img_generator::generate_color_palette_hsv(&(mandelbrot_sli_max_inter.value().round() as u32), &color_config_val_vec);
                            frame.set_image(Some(fltk_image_gen(&mut mandelbrot_config, &chaos_game_no_save)));
                        }
                        Message::JuliaSliderColorChange | _ => {
                            julia_config.color_palette=img_generator::generate_color_palette_hsv(&jul_no_color, &color_config_val_vec);
                            frame.set_image(Some(fltk_image_gen(&mut julia_config, &chaos_game_no_save)));
                        }
                    };

                    redraw()
                },
                //</editor-fold>

                //<editor-fold desc="Message: Save Image">
                Message::SaveImage => {

                    match ch_export_resolution.choice() {
                        Some(res) => {
                            let start = Instant::now();
                            let splits = res.split("x");
                            let vec_str:Vec<&str>= splits.collect();
                            let w=vec_str[0].parse::<u32>().unwrap_or(1920);
                            let h=vec_str[1].parse::<u32>().unwrap_or(1080);
                            let chaos_game_save_option = img_generator::SaveOption {
                                scale_factor: w/WIDTH,
                                crop_h: if check_un_crop.is_checked() {
                                    0
                                }else {
                                    h
                                },
                                preview_downscale: 1.0,
                                enable_multithreading: chaos_game_no_save.enable_multithreading,
                                contrast: adj_contrast_sli.value() as f32,
                                brightness: adj_brightness_sli.value() as i32,
                                path: chaos_game_no_save.path.clone()
                            };

                            match current_tab_no {
                                0 => {
                                    pkg_chaos_game::gen_chaos_game(&chaos_game_config, &chaos_game_save_option);
                                }
                                1 => {
                                    pkg_julia::gen_julia(&julia_config, &chaos_game_save_option);
                                }
                                2 => {
                                    pkg_dragon_curve::gen_dragon(&dragon_config, &chaos_game_save_option);
                                }
                                3 => {
                                    pkg_barnsley_fern::gen_barnsley_fern(&barnsley_fern_config, &chaos_game_save_option);
                                }
                                4 =>{
                                    pkg_mandelbrot::gen_mandelbrot(&mut mandelbrot_config, &chaos_game_save_option);
                                }
                                _ => {}
                            }
                            let duration = start.elapsed();
                            text_display_history=append_history(text_display_history,&format!("Saving completed in {:?}", duration));
                            text_display.buffer().unwrap().set_text(&text_display_gen(&text_display_history));
                        }
                        None => {
                            panic!("Insufficient resolution!")
                        }

                    }
                },
                Message::LoadConfig | Message::ResetConfig => {
                    let reset = match msg {
                        Message::ResetConfig => {
                            true
                        }
                        _ => {
                            false
                        }
                    };
                    let load_result = match current_tab_no {
                        1 => {
                            if reset {
                                julia_config = uni_config.julia_config.clone();
                                Ok(())
                            } else {
                                io::load_config(&mut julia_config)
                            }
                        }
                        2 => {
                            if reset {
                                dragon_config = uni_config.dragon_config.clone();
                                Ok(())
                            } else {
                                io::load_config(&mut dragon_config)
                            }
                        }
                        3 => {
                            if reset {
                                barnsley_fern_config = uni_config.barnsley_fern_config.clone();
                                Ok(())
                            } else {
                                io::load_config(&mut barnsley_fern_config)
                            }
                        }
                        4 => {
                            mandelbrot_config.cache_img.clear();
                            if reset {
                                mandelbrot_config = uni_config.mandelbrot_config.clone();
                                Ok(())
                            } else {
                                io::load_config(&mut mandelbrot_config)
                            }
                        }
                        0 | _ => {
                            if reset {
                                chaos_game_config = uni_config.chaos_game_config.clone();
                                Ok(())
                            } else {
                                io::load_config(&mut chaos_game_config)
                            }
                        }
                    };
                    match load_result {
                        Err(e) => {
                            text_display_history=append_history(text_display_history,&format!("{:?}", e));
                            text_display.buffer().unwrap().set_text(&text_display_gen(&text_display_history));
                        }
                        Ok(..) => {
                            match current_tab_no {
                                0 => {
                                    text_display_history=append_history(text_display_history,&format!("Loaded Chaos Game Config"));
                                    chs_gm_sli_scalar.set_value(chaos_game_config.scalar);
                                    chs_gm_inp_scalar.set_value(&format!("{}", chaos_game_config.scalar));
                                    chs_gm_check_incl_center.set_checked(chaos_game_config.include_center);
                                    chs_gm_check_incl_mid.set_checked(chaos_game_config.include_mid_points);
                                    chs_gm_sli_jump_ratio.set_value(chaos_game_config.distance_ratio);
                                    chs_gm_inp_jump_ratio.set_value(&format!("{}", chaos_game_config.distance_ratio));
                                    chs_gm_ch_condition.set_value(chaos_game_config.condition_id);
                                    chs_gm_inp_modifier_i.set_value(&format!("{}", chaos_game_config.condition_modifier_i));
                                    chs_gm_check_transparent_color.set_checked(chaos_game_config.add_transparency);
                                    chs_gm_ch_background.set_value(chaos_game_config.background_type);
                                    frame.set_image(Some(fltk_image_gen(&mut chaos_game_config, &chaos_game_no_save)));
                                }
                                1 => {
                                    text_display_history=append_history(text_display_history,&format!("Loaded Julia Set Config"));
                                    jul_sli_scalar.set_value(julia_config.scalar);
                                    jul_inp_scalar.set_value(&format!("{}", julia_config.scalar));
                                    jul_sli_c_real.set_value(julia_config.c_real);
                                    jul_inp_c_real.set_value(&format!("{}", julia_config.c_real));
                                    jul_sli_c_imaginary.set_value(julia_config.c_imaginary);
                                    jul_inp_c_imaginary.set_value(&format!("{}", julia_config.c_imaginary));
                                    jul_sli_z_pow.set_value(julia_config.z_pow);
                                    jul_inp_z_pow.set_value(&format!("{}", julia_config.z_pow));
                                    jul_ch_background.set_value(julia_config.background_type);
                                    frame.set_image(Some(fltk_image_gen(&mut julia_config, &chaos_game_no_save)));
                                }
                                2 => {
                                    text_display_history=append_history(text_display_history,&format!("Loaded Dragon Curve Config"));
                                    for i in 0..dragon_config.enabled.len() {
                                        dragon_check_enabled_vec[i].set_checked(dragon_config.enabled[i]);
                                    }
                                    for i in 1..dragon_max_no_drg {
                                        if !dragon_check_enabled_vec[i].is_checked() {
                                            dragon_ch_position_1_vec[i].deactivate();
                                            dragon_ch_position_2_vec[i].deactivate();
                                            dragon_ch_position_3_vec[i].deactivate();
                                            dragon_ch_rotate_vec[i].deactivate();
                                            dragon_ch_flip_vec[i].deactivate();
                                            dragon_inp_no_iter_vec[i].deactivate();
                                            dragon_sli_no_iter_vec[i].deactivate();
                                            dragon_inp_stroke_width_vec[i].deactivate();
                                            dragon_sli_stroke_width_vec[i].deactivate();
                                        } else {
                                            dragon_ch_position_1_vec[i].activate();
                                            dragon_ch_position_2_vec[i].activate();
                                            dragon_ch_position_3_vec[i].activate();
                                            dragon_ch_rotate_vec[i].activate();
                                            dragon_ch_flip_vec[i].activate();
                                            dragon_inp_no_iter_vec[i].activate();
                                            dragon_sli_no_iter_vec[i].activate();
                                            dragon_inp_stroke_width_vec[i].activate();
                                            dragon_sli_stroke_width_vec[i].activate();
                                        }
                                    }

                                    for i in 0..dragon_max_no_drg {
                                        dragon_ch_position_1_vec[i].set_value(dragon_config.position_1[i]);
                                        dragon_ch_position_2_vec[i].set_value(dragon_config.position_2[i]);
                                        dragon_ch_position_3_vec[i].set_value(dragon_config.position_3[i]);
                                        dragon_ch_rotate_vec[i].set_value(dragon_config.rotate[i]);
                                        dragon_ch_flip_vec[i].set_value(dragon_config.flip[i]);
                                        dragon_sli_no_iter_vec[i].set_value(dragon_config.no_iter[i] as f64);
                                        dragon_inp_no_iter_vec[i].set_value(&format!("{}", dragon_config.no_iter[i]));
                                        dragon_sli_stroke_width_vec[i].set_value(dragon_config.stroke_width[i] as f64);
                                        dragon_inp_stroke_width_vec[i].set_value(&format!("{}", dragon_config.stroke_width[i]));
                                    }
                                    dragon_ch_background.set_value(dragon_config.background_type);
                                    for i in 1..dragon_max_no_drg {
                                        for j in 0..dragon_max_no_drg {
                                            if i <= j || !dragon_check_enabled_vec[j].is_checked() {
                                                if let Some(mut item) = dragon_ch_position_3_vec[i].find_item(&format!("{}", Ordinal(j + 1).to_string())) {
                                                    item.hide();
                                                }
                                            } else {
                                                if let Some(mut item) = dragon_ch_position_3_vec[i].find_item(&format!("{}", Ordinal(j + 1).to_string())) {
                                                    item.show();
                                                }
                                            }
                                        }
                                    }

                                    frame.set_image(Some(fltk_image_gen(&mut dragon_config, &chaos_game_no_save)));
                                }
                                3 => {
                                    text_display_history=append_history(text_display_history,&format!("Loaded Barnsley Fern Config"));
                                    for i in 0..barnsley_fern_config.enabled.len() {
                                        fern_check_enable_vec[i].set_checked(barnsley_fern_config.enabled[i])
                                    }
                                    for i in 1..fern_max_no_weight {
                                        for j in 0..fern_no_coefficient {
                                            if !fern_check_enable_vec[i].is_checked() {
                                                fern_inp_co[i][j].deactivate();
                                                fern_sli_co[i][j].deactivate();
                                            } else {
                                                fern_inp_co[i][j].activate();
                                                fern_sli_co[i][j].activate();
                                            }
                                        }

                                        if !fern_check_enable_vec[i].is_checked() {
                                            fern_inp_prob[i].deactivate();
                                            fern_sli_prob[i].deactivate();
                                        } else {
                                            fern_inp_prob[i].activate();
                                            fern_sli_prob[i].activate();
                                        }
                                    }

                                    for i in 0..fern_max_no_weight {
                                        for j in 0..fern_no_coefficient {
                                            fern_inp_co[i][j].set_value(&format!("{}", barnsley_fern_config.coefficient[i][j]));
                                            fern_sli_co[i][j].set_value(barnsley_fern_config.coefficient[i][j]);
                                        }
                                        fern_sli_prob[i].set_value(barnsley_fern_config.probability_score[i] as f64);
                                        fern_inp_prob[i].set_value(&format!("{}", barnsley_fern_config.probability_score[i]));
                                    }
                                    fern_sli_sample.set_value(barnsley_fern_config.sample_multiplier as f64);
                                    fern_inp_sample.set_value(&format!("{}", barnsley_fern_config.sample_multiplier));
                                    fern_check_weight_adj.set_checked(barnsley_fern_config.weight_adj);
                                    fern_ch_background.set_value(barnsley_fern_config.background_type);
                                    frame.set_image(Some(fltk_image_gen(&mut barnsley_fern_config, &chaos_game_no_save)));
                                }
                                4 => {
                                    text_display_history=append_history(text_display_history,&format!("Loaded Mandelbrot Set Config"));
                                    mandelbrot_sli_scalar.set_value(mandelbrot_config.scalar);
                                    mandelbrot_inp_scalar.set_value(&format!("{}", mandelbrot_config.scalar));
                                    mandelbrot_sli_x_loc.set_value(mandelbrot_config.min_x_percentage);
                                    mandelbrot_inp_x_loc.set_value(&format!("{}", mandelbrot_config.min_x_percentage));
                                    mandelbrot_sli_y_loc.set_value(mandelbrot_config.min_y_percentage);
                                    mandelbrot_inp_y_loc.set_value(&format!("{}", mandelbrot_config.min_y_percentage));
                                    mandelbrot_ck_show_zoom.set_checked(mandelbrot_config.show_zoom);
                                    mandelbrot_ck_show_selector.set_checked(mandelbrot_config.show_selector);
                                    mandelbrot_sli_max_inter.set_value(mandelbrot_config.max_iteration as f64);
                                    mandelbrot_inp_max_inter.set_value(&format!("{}", mandelbrot_config.max_iteration));
                                    mandelbrot_sli_max_z.set_value(mandelbrot_config.max_z as f64);
                                    mandelbrot_inp_max_z.set_value(&format!("{}", mandelbrot_config.max_z));
                                    mandelbrot_but_zoom_out.activate();
                                    mandelbrot_but_zoom_out.activate();
                                    if mandelbrot_config.lock_zoom_in {
                                        mandelbrot_but_zoom_in.deactivate();
                                    }
                                    if mandelbrot_config.history.len() <= 1 {
                                        mandelbrot_but_zoom_out.deactivate();
                                    }
                                    mandelbrot_ch_coloring.set_value(mandelbrot_config.coloring_type);

                                    let mut mandelbrot_color_config_vec = Vec::new();
                                    for i in 0..mandelbrot_sli_color_1_config_vec.len() {
                                        mandelbrot_color_config_vec.push(mandelbrot_sli_color_1_config_vec[i].value())
                                    }
                                    mandelbrot_config.color_palette = img_generator::generate_color_palette_hsv(&(mandelbrot_config.max_iteration as u32), &mandelbrot_color_config_vec);

                                    for i in 0..mandelbrot_inp_color_2_config_vec.len() {
                                        mandelbrot_sli_color_2_config_vec[i].set_value(mandelbrot_config.color_type_2_coff[i]);
                                        mandelbrot_inp_color_2_config_vec[i].set_value(&format!("{}", mandelbrot_config.color_type_2_coff[i]));
                                    }

                                    if mandelbrot_config.coloring_type == 1 {
                                        _mandelbrot_lab_hue_.hide();
                                        _mandelbrot_lab_sat_.hide();
                                        _mandelbrot_lab_val_.hide();
                                        for i in 0..mandelbrot_inp_color_1_config_vec.len() {
                                            mandelbrot_inp_color_1_config_vec[i].hide();
                                            mandelbrot_sli_color_1_config_vec[i].hide();
                                        }

                                        for i in 0..mandelbrot_inp_color_2_config_vec.len() {
                                            mandelbrot_inp_color_2_config_vec[i].show();
                                            mandelbrot_sli_color_2_config_vec[i].show();
                                        }
                                    } else {
                                        _mandelbrot_lab_hue_.show();
                                        _mandelbrot_lab_sat_.show();
                                        _mandelbrot_lab_val_.show();
                                        for i in 0..mandelbrot_inp_color_1_config_vec.len() {
                                            mandelbrot_inp_color_1_config_vec[i].show();
                                            mandelbrot_sli_color_1_config_vec[i].show();
                                        }

                                        for i in 0..mandelbrot_inp_color_2_config_vec.len() {
                                            mandelbrot_inp_color_2_config_vec[i].hide();
                                            mandelbrot_sli_color_2_config_vec[i].hide();
                                        }
                                    }
                                    mandelbrot_config.cache_img.clear();
                                    frame.set_image(Some(fltk_image_gen(&mut mandelbrot_config, &chaos_game_no_save)));
                                }
                                _ => {}
                            }
                            text_display.buffer().unwrap().set_text(&text_display_gen(&text_display_history));
                            redraw();
                        }
                    }
                }
                Message::SaveConfig => {
                    let save_result= match current_tab_no {
                        1 => {
                            text_display_history=append_history(text_display_history,&format!("Saved Julia Set Config"));
                            io::save_config(&mut julia_config)
                        }
                        2 => {
                            text_display_history=append_history(text_display_history,&format!("Saved Dragon Curve Config"));
                            io::save_config(&mut dragon_config)
                        }
                        3 => {
                            text_display_history=append_history(text_display_history,&format!("Saved Barnsley Fern Config"));
                            io::save_config(&mut barnsley_fern_config)
                        }
                        4 =>{
                            text_display_history=append_history(text_display_history,&format!("Saved Mandelbrot Set Config"));
                            mandelbrot_config.cache_img.clear();
                            io::save_config(&mut mandelbrot_config)
                        }
                        0 | _ => {
                            text_display_history=append_history(text_display_history,&format!("Saved Chaos Game Config"));
                            io::save_config(&mut chaos_game_config)
                        }
                    };
                    match save_result {
                        Ok(..) => {
                        }
                        Err(e) => {
                            text_display_history=append_history(text_display_history,&format!("{:?}",e));
                        }
                    }
                    text_display.buffer().unwrap().set_text(&text_display_gen(&text_display_history));
                }
                Message::OpenLocation => {
                    let open_result = open::that(&chaos_game_no_save.path);
                    match open_result {
                        Ok(..) => {
                        }
                        Err(e) => {
                            text_display_history=append_history(text_display_history,&format!("{:?}",e));
                        }
                    }
                    text_display.buffer().unwrap().set_text(&text_display_gen(&text_display_history));
                }
                //</editor-fold>

            }
            None => {}
        }
        thread::sleep(time::Duration::from_millis(16));
    }

}

fn str_float_round(inp_string: &str, default: &f64, precision: &i32) ->f64{
    (inp_string.parse::<f64>().unwrap_or(*default)*10.0_f64.powf((*precision) as f64).round())/10.0_f64.powf((*precision) as f64)
}


fn fltk_image_gen<T: img_generator::Config>(config: &mut T, save_option:&img_generator::SaveOption) -> fltk::image::RgbImage {
    let dynamic_img= config.gen_img(save_option);
    let mut t_rgb =fltk::image::RgbImage::new(
        &dynamic_img.to_bytes(), (WIDTH as f64 /(*save_option).preview_downscale) as u32, (HEIGHT as f64 /(*save_option).preview_downscale) as u32, ColorDepth::Rgba8)
        .unwrap();
    t_rgb.scale(WIDTH as i32,HEIGHT as i32,true,true);
    t_rgb
}


fn append_history(mut text_display_history: VecDeque<String>, action: &str) -> VecDeque<String> {
    let time_as_str=format!("{}",Local::now());
    let v: Vec<&str> =time_as_str.split('.').collect();
    text_display_history.push_back(format!("{}:\t{}\n",v[0],action));
    text_display_history

}

fn text_display_gen(text_display_history:&VecDeque<String>) -> String{
    let mut display_string: String = "".to_owned();
    for i in (0..text_display_history.len()).rev(){
        display_string.push_str(&text_display_history[i]);
        display_string.push_str(&format!("\n"));
    }
    display_string

}

pub fn color_palette_rgb_handler(no_color:i32,seed: u64, path: usize) -> (Vec<ColorRGB>,Vec<ColorRGB>,String){
    let load_colors=io::load_color_palette(path);
    let (custom_palette,random_palette):(Vec<ColorRGB>, Vec<ColorRGB>);
    random_palette =img_generator::generate_color_palette_rgb(seed, no_color);
    let return_string:String;
    match load_colors {
        Ok(loaded_cb)=>{
            custom_palette =loaded_cb;
            if random_palette.len()!= custom_palette.len() {
                return_string= "Custom Color Data File Reset!".to_string();
                io::save_color_palette(&random_palette, 0).unwrap();
            }else {
                return_string= "Loaded Color Data File".to_string();
            }
        }
        Err(_)=>{
            return_string= "Unable to Load Custom Color Data".to_string();
            let save_colors=io::save_color_palette(&random_palette, path);
            if save_colors.is_err(){
                panic!("Unable to Save Color Data");
            }
            custom_palette = random_palette.clone();
        }
    }
    (random_palette, custom_palette, return_string)
}

fn get_tooltips(tooltip_map: &IndexMap<String,String>, key: &str) -> String{
    let get_val= tooltip_map.get(key);
    match get_val {
        Some(tooltip) =>{(**tooltip).to_string()}
        None => {"".to_string()}
    }
}

fn get_icon()-> fltk::image::RgbImage{
    let img = gen_icon();
    let t_rgb =fltk::image::RgbImage::new(
        &img.to_bytes(), 220, 220, ColorDepth::Rgb8)
        .unwrap();
    t_rgb
}
