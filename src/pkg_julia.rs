use image::*;
use crate::img_generator::*;
use crate::io::*;
use std::thread;
use palette::ConvertInto;
extern crate num_complex;
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Clone,Debug)]
pub struct JuliaConfig {
    pub scalar: f64,
    pub c_real:f64,
    pub c_imaginary: f64,
    pub z_pow: f64,
    #[serde(skip)]
    pub color_palette: Vec<ColorRGB>,
    pub background_type: i32,
}

fn create_foreground_thread(min_x:u32, sub_width:u32, min_y:u32, sub_height:u32, count:u32, julia_config:&JuliaConfig, save_option: &SaveOption) ->(ImageBuffer<Rgba<u8>,Vec<u8>>, u32, u32) {


    let max_x=min_x+sub_width;
    let max_y=min_y+sub_height;

    let (img_width,img_height):(u32,u32)= get_empty_image_size(save_option.scale_factor as f64/save_option.preview_downscale);
    let mut img_buf = <ImageBuffer<Rgba<u8>, _>>::new(img_width/count, img_height/count);


    let scale_ratio= if julia_config.color_palette.len()!=0 {1_f64}else{0.382_f64};
    let scalar=(julia_config).scalar* scale_ratio;

    let scalar_x = scalar / img_width as f64 ;
    let scalar_y = scalar / img_height as f64;
    let c = num_complex::Complex::new(julia_config.c_real, julia_config.c_imaginary);
    let step= if julia_config.color_palette.len()!=0 {256/(julia_config.color_palette.len()-1)}else{8};
    let step_max= if julia_config.color_palette.len()!=0 {step*(julia_config.color_palette.len()-1)}else{256};
    let z_norm_max=2.0;


    for x in min_x..max_x {
        for y in min_y..max_y {

            let cx = y as f64 * scalar_x - (scalar /2.0);
            let cy = x as f64 * scalar_y - (scalar /2.0);

            let mut z = num_complex::Complex::new(cx, cy);

            let mut i = 0;
            while i < step_max && z.norm() <= z_norm_max {
                z = z.powf(julia_config.z_pow) + c ;
                i += step;
            }
            if i>=step_max {
                i = step_max-1;
            }
            if julia_config.color_palette.len()!=0{
                img_buf.put_pixel(x - min_x, y - min_y, image::Rgba([julia_config.color_palette[i / step].r, julia_config.color_palette[i / step].g, julia_config.color_palette[i / step].b, i as u8]));
            }else {
                img_buf.put_pixel(x - min_x, y - min_y, image::Rgba([i as u8, i as u8, i as u8,255 as u8]));
            }
        }
    }
    (img_buf,min_x,min_y)

}

fn create_foreground_mt(julia_config:& JuliaConfig, save_option: &SaveOption) -> ImageBuffer<Rgba<u8>,Vec<u8>>{

    let nthreads:u32=*super::N_THREADS;
    assert!(julia_config.color_palette.len()<=256);
    assert_eq!(((nthreads as f64).powf(0.5) as i32).pow(2),nthreads as i32);
    let mut children = vec![];
    let (img_width,img_height):(u32,u32)= get_empty_image_size((*save_option).scale_factor as f64/(*save_option).preview_downscale);
    let mut img_buf = <ImageBuffer<Rgba<u8>, _>>::new(img_width, img_height);

    for i in 0..nthreads {
        let julia_config_temp=julia_config.clone();
        let save_option_temp=save_option.clone();
        let nthreads_sqrt=(nthreads as f64).powf(0.5) as u32;
        let sub_width=img_width/nthreads_sqrt;
        let sub_height=img_height/nthreads_sqrt;
        let min_x=sub_width*(i%nthreads_sqrt);
        let min_y=sub_height*(i/nthreads_sqrt);
        children.push(thread::spawn(move || {
            create_foreground_thread(min_x, sub_width, min_y, sub_height, nthreads_sqrt, &julia_config_temp, &save_option_temp)
        }));
    }
    for child in children {
        let (img,mx,my) = child.join().unwrap();
        let _ = img_buf.copy_from(&img, mx, my);
    }
    img_buf
}

fn create_foreground_st(julia_config:& JuliaConfig, save_option: &SaveOption) -> ImageBuffer<Rgba<u8>,Vec<u8>> {
    assert!(julia_config.color_palette.len()<=256);
    let (img_width,img_height):(u32,u32)= get_empty_image_size((*save_option).scale_factor as f64/(*save_option).preview_downscale);
    let (img_buf,_,_)= create_foreground_thread(0, img_width, 0, img_height, 1, julia_config, save_option);
    img_buf
}

fn merge_background_thread(min_x:u32, sub_width:u32, min_y:u32, sub_height:u32, count:u32, rgb_image_buffer: &image::ImageBuffer<Luma<u8>,Vec<u8>>, julia_config:&JuliaConfig, save_option: &SaveOption) -> (ImageBuffer<Rgba<u8>,Vec<u8>>, u32, u32) {

    let (img_width,img_height):(u32,u32)= get_empty_image_size(save_option.scale_factor as f64/save_option.preview_downscale);
    let (mut img_buf,return_flag) =  background_colorize(&RgbaImage::new(img_width/count, img_height/count),&julia_config.background_type ,save_option);
    if return_flag {
        return  (img_buf,min_x,min_y)
    }

    let max_x=min_x+sub_width;
    let max_y=min_y+sub_height;
    if julia_config.background_type == 2 && save_option.scale_factor>1 {
        return (img_buf,min_x,min_y);
    }
    for x in min_x..max_x {
        for y in min_y..max_y {
            let new_pixel =
                if julia_config.background_type == 0 {
                    image::Rgba([0, 0, 0, 255])
                } else if julia_config.background_type == 2 {
                    image::Rgba([255, 255, 255, 255])
                } else {
                    let px = rgb_image_buffer.get_pixel(x - min_x, y - min_y);
                    let image::Luma(data) = *px;
                    if data[0] > 0 {
                        let l=julia_config.color_palette.len();
                        image::Rgba([255-julia_config.color_palette[l-1].r, 255-julia_config.color_palette[l-1].g, 255-julia_config.color_palette[l-1].b, 125])
                    }else {
                        image::Rgba([0,0,0,255])
                    }

                };
            img_buf.put_pixel(x - min_x, y - min_y, new_pixel);
        }
    }

    (img_buf,min_x,min_y)
}

fn merge_background_mt(img_buf_foreground_edges: &image::ImageBuffer<Luma<u8>,Vec<u8>>, julia_config:& JuliaConfig, save_option: &SaveOption ) -> ImageBuffer<Rgba<u8>,Vec<u8>> {
    let nthreads:u32=*super::N_THREADS;

    let mut children = vec![];
    let (img_width,img_height):(u32,u32)= get_empty_image_size((*save_option).scale_factor as f64/(*save_option).preview_downscale);
    let mut img_buf = <ImageBuffer<Rgba<u8>, _>>::new(img_width, img_height);

    for i in 0..nthreads {
        let julia_config_temp=julia_config.clone();
        let save_option_temp=save_option.clone();
        let nthreads_sqrt=(nthreads as f64).powf(0.5) as u32;
        let sub_width=img_width/nthreads_sqrt;
        let sub_height=img_height/nthreads_sqrt;
        let min_x=sub_width*(i%nthreads_sqrt);
        let min_y=sub_height*(i/nthreads_sqrt);

        let img_buf_foreground_edges_crop=imageops::crop_imm(img_buf_foreground_edges,min_x,min_y,min_x+sub_width,min_y+sub_height).to_image();
        children.push(thread::spawn(move || {
            merge_background_thread(min_x, sub_width, min_y, sub_height, nthreads_sqrt, &img_buf_foreground_edges_crop,&julia_config_temp, &save_option_temp)
        }));
    }
    for child in children {
        let (img,mx,my) = child.join().unwrap();
        let _=img_buf.copy_from(&img, mx, my);
    }
    img_buf

}

fn merge_background_st(img_buf_foreground_edges: &image::ImageBuffer<Luma<u8>,Vec<u8>>, julia_config:& JuliaConfig, save_option: &SaveOption) -> ImageBuffer<Rgba<u8>,Vec<u8>> {
    let (img_width,img_height):(u32,u32)= get_empty_image_size((*save_option).scale_factor as f64/(*save_option).preview_downscale);
    let (img_buf,_,_)= merge_background_thread(0, img_width, 0, img_height, 1, img_buf_foreground_edges, julia_config, save_option);
    img_buf
}

fn rgba8_to_gray(img: image::ImageBuffer<Rgba<u8>, Vec<u8>>) -> image::GrayImage {
    let (width, height) = img.dimensions();
    let mut buffer: image::GrayImage = image::ImageBuffer::new(width, height);
    for (to, &image::Rgba([r, g, b, _])) in buffer.pixels_mut().zip(img.pixels()) {
        let gray=(0.2126 * r as f64 + 0.7152 * g as f64 + 0.0722 * b as f64) as u8;
        *to = image::Luma([gray]);
    }
    buffer
}

fn create_background(julia_config:& JuliaConfig, save_option: &SaveOption) -> ImageBuffer<Rgba<u8>,Vec<u8>> {


    let mut new_julia_config = (*julia_config).clone();
    new_julia_config.color_palette.clear();
    let img_buf_foreground_edges=if (*save_option).enable_multithreading {
        create_foreground_mt(&new_julia_config,save_option)
    }else {
        create_foreground_st(&new_julia_config, save_option)
    };
    let  gray = rgba8_to_gray(img_buf_foreground_edges);
    let dynamic_image_foreground_edges=imageops::filter3x3(&gray,&[-1.0,-1.0,-1.0,-1.0,8.0,-1.0,-1.0,-1.0,-1.0]);

    if (*save_option).enable_multithreading {
        merge_background_mt(&dynamic_image_foreground_edges,julia_config, save_option)
    }else {
        merge_background_st(&dynamic_image_foreground_edges,julia_config, save_option)
    }

}

fn create_foreground(julia_config:& JuliaConfig, save_option:&SaveOption) ->ImageBuffer<Rgba<u8>,Vec<u8>>{
    if (*save_option).enable_multithreading {
        create_foreground_mt(julia_config,save_option)
    }else {
        create_foreground_st(julia_config, save_option)
    }
}

pub fn gen_julia(julia_config:& JuliaConfig, save_option:&SaveOption) -> DynamicImage  {

    let img_buf_foreground=   create_foreground(julia_config,save_option);

    let img_buf_background= create_background(julia_config,save_option);

    let mut dynamic_image =DynamicImage::ImageRgba8(combine_fg_bg(img_buf_foreground, img_buf_background));
    dynamic_image=dynamic_image.adjust_contrast(save_option.contrast);
    dynamic_image=dynamic_image.brighten(save_option.brightness);
    save_image(&dynamic_image, save_option, "Julia");
    dynamic_image

}