use image::*;
use crate::img_generator::{SaveOption, ColorRGB, Config};
use chrono::prelude::*;
use std::fs::{create_dir,write,read_to_string};
use serde::{Serialize, Deserialize};
use anyhow::{bail, Result};
use image::io::Reader as ImageReader;
use std::path::{Path,PathBuf};

pub fn save_image(dynamic_image:&DynamicImage, save_option:&SaveOption, prefix:&str){
    let (img_width,img_height):(u32,u32)=dynamic_image.dimensions();
    if (*save_option).scale_factor>1 {
        let file_name = format!("{}_{}.png", prefix, Local::now()).replace(":", "_");
        let save_path=save_option.path.join(file_name);
        if (*save_option).crop_h > 0 && (*save_option).crop_h < img_height {
            let temporary_dynamic_image = dynamic_image.crop_imm(0, (img_height - (*save_option).crop_h) / 2, img_width, (*save_option).crop_h);
            temporary_dynamic_image.save(save_path).unwrap();
        } else {
            dynamic_image.save(save_path).unwrap();
        }
    }
}


pub fn save_color_palette(c_data:&Vec<ColorRGB>, path_no:usize) -> Result<(), anyhow::Error> {
    if !super::COLOR_DATA_FOLDER_PATH.exists(){
        create_dir(&*super::COLOR_DATA_FOLDER_PATH).unwrap();
    }
    let color_data=c_data.clone();
    let serialized_cb = serde_json::to_string_pretty(&color_data).unwrap();

    let write_file=write(&*super::COLOR_DATA_FILE_PATH[path_no], &serialized_cb)?;
    Ok(write_file)
}


pub fn load_color_palette(path_no: usize) -> Result<Vec<ColorRGB>, anyhow::Error> {
    let contents = read_to_string(&*super::COLOR_DATA_FILE_PATH[path_no])?;
    let vec_rgb:Vec<ColorRGB> =serde_json::from_str(&contents)?;
    Ok(vec_rgb)
}


pub fn save_config<T: Config>(config: &T) -> Result<(), anyhow::Error> {
    if !super::CONFIG_FOLDER_PATH.exists(){
        create_dir(&*super::CONFIG_FOLDER_PATH).unwrap();
    }
    let path_no= config.get_type_no();
    let json_string= config.serialize();
    let write_file=write(&*super::CONFIG_FILE_PATH[path_no], &json_string)?;
    Ok(write_file)
}

pub fn load_config<T: Config>(config: &mut T) -> Result<(), anyhow::Error> {
    let path_no = config.get_type_no();
    let json_string = read_to_string(&*super::CONFIG_FILE_PATH[path_no])?;
    config.deserialize(&json_string)

}


