#![allow(unused_doc_comments)]
use rand::{Rng,SeedableRng, rngs::StdRng};
use std::f64::consts::PI;
use image::*;
use std::collections::{VecDeque};
use indexmap::{IndexMap};
use crate::img_generator::*;
use crate::io::*;
use std::thread;
use std::process::exit;
use std::cmp::min;
use image::imageops::overlay;
use std::time::{Duration, Instant};
use serde::{Serialize, Deserialize};

extern crate palette;

#[derive(Serialize, Deserialize, Clone,Debug)]
pub struct ChaosGameConfig {
    pub scalar: f64,
    pub no_point:i32,
    pub include_mid_points:bool,
    pub include_center:bool,
    pub distance_ratio:f64,
    pub condition_id: i32,
    pub condition_modifier_i:i32,
    #[serde(skip)]
    pub color_palette: Vec<ColorRGB>,
    pub add_transparency: bool,
    pub background_type: i32,

}


#[derive(Copy,Clone,Debug)]
struct Point{
    x:u32,
    y:u32,
    color_rgba: ColorRGB,
}

fn points_calculator(current_point:[i32;2],dist: f64, degree:f64) -> [i32;2]{
    let t_x=degree.sin() *dist;
    let t_y=degree.cos() *dist;
    return [t_x.round() as i32+current_point[0],t_y.round() as i32+current_point[1]]
}

fn original_point_maker(chaos_game_config: &ChaosGameConfig, save_option: &SaveOption) -> Vec<Point> {
    assert!((*chaos_game_config).no_point>2);
    let (img_width,img_height):(u32,u32)= get_empty_image_size((*save_option).scale_factor as f64/(*save_option).preview_downscale);
    let r = if img_width>img_height {
        (img_height as f64 /(*chaos_game_config).scalar) as u32
    }else {
        (img_width as f64 /(*chaos_game_config).scalar) as u32
    };
    let mut pt_vec:Vec<Point> = Vec::new();

    for i in 0..(*chaos_game_config).no_point{
        let new_coordinate=points_calculator([(img_width / 2) as i32, (img_height / 2) as i32], r as f64, i as f64 *2.0*PI/(*chaos_game_config).no_point as f64);
            pt_vec.push(Point{
                x: new_coordinate[0] as u32,
                y: new_coordinate[1] as u32,
                color_rgba: chaos_game_config.color_palette[pt_vec.len()]
            })
    }

    if (*chaos_game_config).include_mid_points{
        let mut pt_vec_mid:Vec<Point> = Vec::new();
        for i in 0..pt_vec.len(){
            let mid_x=(pt_vec[i].x+if i<pt_vec.len()-1{pt_vec[i+1].x}else { pt_vec[0].x })/2;
            let mid_y=(pt_vec[i].y+if i<pt_vec.len()-1{pt_vec[i+1].y}else { pt_vec[0].y })/2;
            pt_vec_mid.push(Point{
                x: mid_x,
                y: mid_y,
                color_rgba: chaos_game_config.color_palette[pt_vec.len()]
            });
        }
        let mut new_pt_vec:Vec<Point> = Vec::new();
        for i in 0..pt_vec.len(){
            new_pt_vec.push(pt_vec[i]);
            new_pt_vec.push(pt_vec_mid[i]);
        }
        pt_vec=new_pt_vec;
    }

    if (*chaos_game_config).include_center {
        pt_vec.push(Point{
            x: img_width / 2,
            y: img_height / 2,
            color_rgba: chaos_game_config.color_palette[pt_vec.len()]
        })
    }

    let (mut min_y,mut max_y):(u32,u32)=(img_height,0);
    for i in 0..pt_vec.len(){
        if pt_vec[i].y<min_y {
            min_y=pt_vec[i].y;
        }
        if pt_vec[i].y>max_y {
            max_y=pt_vec[i].y;
        }
        pt_vec[i].color_rgba = chaos_game_config.color_palette[i];
    }

    let extra_dif=(img_height as i32-max_y as i32-min_y as i32)/2;
    for i in 0..pt_vec.len(){
        pt_vec[i].y=(pt_vec[i].y as i32 + extra_dif) as u32;
        pt_vec[i].y=img_height-pt_vec[i].y-1;
    }
    return pt_vec;
}


fn point_randomizer(original_points: &Vec<Point>, chaos_game_config: &ChaosGameConfig, save_option:&SaveOption) -> IndexMap<[u32;2],ColorRGB>{
    let mut point_map=IndexMap::new();
    let (img_width,img_height):(u32,u32)= get_empty_image_size((*save_option).scale_factor as f64/(*save_option).preview_downscale);
    let mut rng = StdRng::seed_from_u64(32);
    let mut anchor:Point;
    let mut current:Point= original_points[0];
    let mut anchor_pt:usize;
    let mut anchor_cache_queue = VecDeque::new();
    let n_original_points=chaos_game_config.no_point*if chaos_game_config.include_mid_points {2}else{1}+if chaos_game_config.include_center{1}else {0};
    let no_random_points= ((((img_width as f64).powf(2.0) + ((img_height) as f64).powf(2.0)).powf(0.5)/10.0).powf(2.0) / ((*chaos_game_config).scalar).powf(2.0)*(n_original_points as f64).powf(0.25)* 256.0) as i32;
    let central_point_val=   if (*chaos_game_config).include_center{
        original_points.len()-1
    }else {
        999999_usize
    };
    let mut eli_count=0_usize;
    let mut eli_vec:Vec<usize>=Vec::new();

    let transparency_mod=if chaos_game_config.add_transparency{(10.0*(n_original_points as f64).powf(0.25))as i32}else{255};

    for _ in 0..no_random_points {
        loop {
            anchor_pt = rng.gen_range(0..original_points.len()) as usize;

            if (*chaos_game_config).condition_id==1 && (*chaos_game_config).condition_modifier_i >0 {
                assert!(original_points.len()>((*chaos_game_config).condition_modifier_i as f64 / 0.628) as usize);
                if anchor_cache_queue.len()<(*chaos_game_config).condition_modifier_i as usize {
                    anchor_cache_queue.push_back(anchor_pt);
                    break
                }else {
                    let mut break_con:bool = true;
                    for i in 0..(*chaos_game_config).condition_modifier_i {
                        if break_con && anchor_cache_queue[i as usize ]==anchor_pt {
                            break_con=false
                        }
                    }
                    if break_con {
                        anchor_cache_queue.pop_front();
                        anchor_cache_queue.push_back(anchor_pt);
                        break
                    }
                }
            }else if (*chaos_game_config).condition_id==2 && (*chaos_game_config).condition_modifier_i >0 {
                if anchor_cache_queue.len() < 1 {
                    anchor_cache_queue.push_back(anchor_pt);
                    break
                } else {
                    let ran_val: i32 = rng.gen_range(0..100);
                    if (*chaos_game_config).condition_modifier_i > ran_val {
                        anchor_pt = anchor_cache_queue[0];
                        break
                    } else {
                        anchor_cache_queue.push_back(anchor_pt);
                        anchor_cache_queue.pop_front();
                        break
                    }
                }
            }else if (*chaos_game_config).condition_id==3 && ((original_points.len()>3 && !(*chaos_game_config).include_center) || (original_points.len()>4 && (*chaos_game_config).include_center)) {
                if anchor_cache_queue.len() < 2 {
                    anchor_cache_queue.push_back(anchor_pt);
                }

                if eli_count ==0 && anchor_cache_queue.len() >= 2 && (anchor_cache_queue[1] == anchor_cache_queue[0]) && (anchor_cache_queue[1] != central_point_val) {
                    let mut next_point_vec:Vec<usize>=Vec::new();
                    let mut prev_point_vec:Vec<usize>=Vec::new();
                    let vec_len_cut=0.25;
                    if (*chaos_game_config).include_center {
                        let vec_len=((original_points.len()-1) as f64*vec_len_cut).round() as usize;
                        let mut current_point=anchor_cache_queue[1];
                        for _ in 0..vec_len {
                            if current_point + 2 >= original_points.len() {
                                current_point = 0_usize
                            } else {
                                current_point += 1
                            };
                            next_point_vec.push(current_point)
                        }

                    } else {
                        let vec_len=((original_points.len()) as f64*vec_len_cut).round() as usize;
                        let mut current_point=anchor_cache_queue[1];
                        for _ in 0..vec_len {
                            if current_point + 1 >= original_points.len() {
                                current_point = 0_usize
                            } else {
                                current_point += 1
                            };
                            next_point_vec.push(current_point)
                        }
                    };
                    if (*chaos_game_config).include_center {
                        let vec_len=((original_points.len()-1) as f64*vec_len_cut).round() as usize;
                        let mut current_point=anchor_cache_queue[1];
                        for _ in 0..vec_len {
                            if current_point == 0 {
                                current_point = original_points.len() - 2
                            } else {
                                current_point -= 1
                            };
                            prev_point_vec.push(current_point)
                        }
                    } else {
                        let vec_len=((original_points.len()-1) as f64*vec_len_cut).round() as usize;
                        let mut current_point=anchor_cache_queue[1];
                        for _ in 0..vec_len {
                            if current_point == 0 {
                                current_point = original_points.len() - 1
                            } else {
                                current_point -= 1
                            };
                            prev_point_vec.push(current_point)
                        }
                    };
                    eli_count=next_point_vec.len();
                    eli_vec.append(&mut next_point_vec);
                    eli_vec.append(&mut prev_point_vec);
                }
                if eli_count>0 {
                    let mut eli_check =true;
                    for i in 0..eli_vec.len(){
                        if anchor_pt==eli_vec[i]{
                            eli_check=false;
                            break;
                        }
                    }
                    if eli_check {
                        eli_count-=1;
                        if eli_count ==0 {
                            eli_vec.clear();
                            anchor_cache_queue.push_back(anchor_pt);
                            anchor_cache_queue.pop_front();
                        }
                        break
                    }
                }else {
                    anchor_cache_queue.push_back(anchor_pt);
                    anchor_cache_queue.pop_front();
                    break
                }
            }else {
                break
            }
        }
        anchor = original_points[anchor_pt];


        current=Point{
            x: (anchor.x as f64 * (*chaos_game_config).distance_ratio + current.x as f64 * (1.0 - (*chaos_game_config).distance_ratio)).round() as u32,
            y: (anchor.y as f64 * (*chaos_game_config).distance_ratio + current.y as f64 * (1.0 - (*chaos_game_config).distance_ratio)).round() as u32,
            color_rgba: chaos_game_config.color_palette[anchor_pt]
        };

        let color_rgb_entry = point_map.entry([ current.x, current.y]);
        match color_rgb_entry {
            indexmap::map::Entry::Occupied(occupied) => {
                let color:&mut ColorRGB=occupied.into_mut();
                let new_transparency=(( (*color).a as f64*5.0) as i32) +transparency_mod;
                (*color).a=if new_transparency>=255{
                    255_u8
                }else{
                    new_transparency as u8};
            }
            indexmap::map::Entry::Vacant(vacant) => {
                vacant.insert( ColorRGB{
                    r: current.color_rgba.r,
                    g: current.color_rgba.g,
                    b: current.color_rgba.b,
                    a: transparency_mod as u8
                } );
            }
        }

    }
    point_map
}


fn create_foreground_thread(min_x:u32, min_y:u32, count:u32, point_map: &IndexMap<[u32;2],ColorRGB>, save_option: &SaveOption) ->  (ImageBuffer<Rgba<u8>,Vec<u8>>, u32, u32){

    let (img_width,img_height):(u32,u32)= get_empty_image_size(save_option.scale_factor as f64/save_option.preview_downscale);
    let mut img_buf = <ImageBuffer<Rgba<u8>, _>>::new(img_width/count, img_height/count);
    for (k,v) in point_map {
        if k[0] < img_width && k[1] < img_height {
            img_buf.put_pixel(
                k[0] -min_x, k[1] -min_y,
                image::Rgba([v.r,v.g,v.b,v.a]))
        }
    }

    (img_buf,min_x,min_y)
}

#[allow(dead_code)]
fn create_foreground_mt(point_map: &IndexMap<[u32;2],ColorRGB>, save_option: &SaveOption) ->  ImageBuffer<Rgba<u8>,Vec<u8>>{
    let (img_width,img_height):(u32,u32)= get_empty_image_size(save_option.scale_factor as f64/save_option.preview_downscale);
    let mut img_buf = <ImageBuffer<Rgba<u8>, _>>::new(img_width, img_height);
    let nthreads:u32=*super::N_THREADS;


    let (mut min_x_vec,mut min_y_vec):(Vec<u32>,Vec<u32>)=(Vec::new(),Vec::new());

    let mut sub_index_map_vec:Vec<IndexMap<[u32;2],ColorRGB>>=Vec::new();

    let nthreads_sqrt=(nthreads as f64).powf(0.5) as u32;
    let (sub_width, sub_height) = (img_width/nthreads_sqrt, img_height/nthreads_sqrt);
    for i in 0..nthreads {
        min_x_vec.push(sub_width*(i%nthreads_sqrt));
        min_y_vec.push(sub_height*(i /nthreads_sqrt));
        let m:IndexMap<[u32;2],ColorRGB>=IndexMap::new();
        sub_index_map_vec.push(m);
    }

    for (k,v) in point_map{
        let w=(*k)[0]/sub_width;
        let h=(*k)[1]/sub_height;
        let loc=(h*nthreads_sqrt+w) as usize;
        sub_index_map_vec[loc].insert(*k, *v);
    }


    let mut children = vec![];
    for i in 0..nthreads as usize {
        let point_map_clone=sub_index_map_vec[i].clone();
        let save_option_temp=(*save_option).clone();
        let nthreads_sqrt=nthreads_sqrt.clone();
        let min_x=min_x_vec[i];
        let min_y=min_y_vec[i];

        children.push(thread::spawn(move || {
            create_foreground_thread(min_x, min_y, nthreads_sqrt, &point_map_clone, &save_option_temp)
        }));
    }

    for child in children {
        let (img,mx,my)=child.join().unwrap();
        let _ = img_buf.copy_from(&img,mx,my);
    }
    img_buf
}





fn create_foreground_st(point_map: &IndexMap<[u32;2],ColorRGB>, save_option: &SaveOption) ->  ImageBuffer<Rgba<u8>,Vec<u8>>{
    let (img_buf,_,_)= create_foreground_thread(0, 0, 1, &point_map, &save_option);

    img_buf
}


fn create_background_thread(min_x:u32, sub_width:u32, min_y:u32, sub_height:u32, count:u32, original_points: &Vec<Point>, chaos_game_config: &ChaosGameConfig, save_option: &SaveOption) ->(ImageBuffer<Rgba<u8>,Vec<u8>>, u32, u32) {
    let max_x=min_x+sub_width;
    let max_y=min_y+sub_height;

    let (img_width,img_height):(u32,u32)= get_empty_image_size(save_option.scale_factor as f64/save_option.preview_downscale);
    let (mut img_buf,return_flag) =  background_colorize(&RgbaImage::new(img_width/count, img_height/count),& chaos_game_config.background_type ,save_option);
    if return_flag {
        return  (img_buf,min_x,min_y)
    }
    for x in min_x..max_x {
        for y in min_y..max_y {
            let mut inverse_distance: Vec<f64> = Vec::new();
            let mut sum_inverse_distance = 0.0;
            for i in 0..original_points.len() {
                inverse_distance.push(((
                    original_points[i].x as f64 - x as f64).powf(2.0)
                    + (original_points[i].y as f64 - y as f64).powf(2.0))
                    .powf(-0.5));
                sum_inverse_distance += inverse_distance[i];
            }
            let (mut r_val, mut g_val, mut b_val): (f64, f64, f64) = (0.0, 0.0, 0.0);
            for i in 0..inverse_distance.len() {
                r_val += (255 - original_points[i].color_rgba.r) as f64 * inverse_distance[i] / sum_inverse_distance;
            }
            for i in 0..inverse_distance.len() {
                g_val += (255 - original_points[i].color_rgba.g) as f64 * inverse_distance[i] / sum_inverse_distance;
            }
            for i in 0..inverse_distance.len() {
                b_val += (255 - original_points[i].color_rgba.b) as f64 * inverse_distance[i] / sum_inverse_distance;
            }

            img_buf.put_pixel(x - min_x, y - min_y, image::Rgba([r_val as u8, g_val as u8, b_val as u8, 255]));
        }
    }

    (img_buf,min_x,min_y)

}


fn create_background_mt(original_points: &Vec<Point>,chaos_game_config:& ChaosGameConfig, save_option: &SaveOption) -> ImageBuffer<Rgba<u8>,Vec<u8>>{

    let nthreads:u32=*super::N_THREADS;
    assert_eq!(((nthreads as f64).powf(0.5) as i32).pow(2),nthreads as i32);
    let mut children = vec![];
    let (img_width,img_height):(u32,u32)= get_empty_image_size((*save_option).scale_factor as f64/(*save_option).preview_downscale);
    let mut img_buf = <ImageBuffer<Rgba<u8>, _>>::new(img_width, img_height);

    for i in 0..nthreads {
        let original_points_temp=(*original_points).clone();
        let chaos_game_config_temp=(*chaos_game_config).clone();
        let save_option_temp=(*save_option).clone();
        let nthreads_sqrt=(nthreads as f64).powf(0.5) as u32;
        let sub_width=img_width/nthreads_sqrt;
        let sub_height=img_height/nthreads_sqrt;
        let min_x=sub_width*(i%nthreads_sqrt);
        let min_y=sub_height*(i/nthreads_sqrt);
        children.push(thread::spawn(move || {
            create_background_thread(min_x, sub_width, min_y, sub_height, nthreads_sqrt, &original_points_temp,&chaos_game_config_temp, &save_option_temp)
        }));
    }
    for child in children {
        let (img,mx,my) = child.join().unwrap();
        let _ = img_buf.copy_from(&img, mx, my);
    }
    img_buf
}

fn create_background_st(original_points: &Vec<Point>,chaos_game_config:& ChaosGameConfig, save_option: &SaveOption) -> ImageBuffer<Rgba<u8>,Vec<u8>> {
    let (img_width,img_height):(u32,u32)= get_empty_image_size((*save_option).scale_factor as f64/(*save_option).preview_downscale);

    let (img_buf,_,_)= create_background_thread(0, img_width, 0, img_height, 1, &original_points,chaos_game_config, save_option);
    img_buf
}

fn create_background(original_points: &Vec<Point>, chaos_game_config:& ChaosGameConfig, save_option: &SaveOption) -> ImageBuffer<Rgba<u8>,Vec<u8>> {
    if (*save_option).enable_multithreading {
        create_background_mt(original_points, chaos_game_config, save_option)
    }else {
        create_background_st(original_points, chaos_game_config, save_option)
    }
}



fn create_foreground(points_map: &IndexMap<[u32;2],ColorRGB>, save_option: &SaveOption) ->  ImageBuffer<Rgba<u8>,Vec<u8>>{
    if (*save_option).enable_multithreading {
        /**
        create_foreground_mt(points_vector,chaos_game_config,save_option)
        Disabled because not using MT is actually faster
        **/
        create_foreground_st(points_map,  save_option)

    }else {
        create_foreground_st(points_map,  save_option)
    }

}

pub fn gen_chaos_game(chaos_game_config:& ChaosGameConfig, save_option:&SaveOption) -> DynamicImage {

    let points_vector =original_point_maker( chaos_game_config, save_option);


    let points_map= point_randomizer(&points_vector, chaos_game_config, save_option);

    let img_buf_background = create_background(&points_vector, chaos_game_config, save_option);


    let img_buf_foreground = create_foreground(&points_map, save_option);

    let mut dynamic_image=DynamicImage::ImageRgba8(combine_fg_bg(img_buf_foreground,img_buf_background));
    dynamic_image=dynamic_image.adjust_contrast(save_option.contrast);
    dynamic_image=dynamic_image.brighten(save_option.brightness);
    save_image(&dynamic_image, save_option, "Chaos_game");
    dynamic_image
}


