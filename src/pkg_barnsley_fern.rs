use rand::{Rng,SeedableRng, rngs::StdRng,prelude::*,distributions::WeightedIndex};
use image::*;
use crate::img_generator::*;
use crate::io::*;
use std::thread;
use indexmap::{IndexMap};
use std::collections::hash_map::Entry;
use std::time::{Duration, Instant};
use image::imageops::overlay;
use serde::{Serialize, Deserialize};


#[derive(Serialize, Deserialize, Clone,Debug)]
pub struct BarnsleyFernConfig {
    pub max_no_weight: usize,
    pub enabled: Vec<bool>,
    pub coefficient: Vec<Vec<f64>>,
    pub probability_score: Vec<i32>,
    pub sample_multiplier: u32,
    pub weight_adj: bool,
    #[serde(skip)]
    pub color_palette: Vec<ColorRGB>,
    pub background_type: i32,
}

fn non_uniform_rand(distribution_vec:&Vec<i32>, rng: &mut StdRng)->usize{
    let mut choice:Vec<usize>=Vec::new();
    for i in 0..distribution_vec.len(){
        choice.push(i);
    }
    let dist = WeightedIndex::new(&*distribution_vec).unwrap();
    choice[dist.sample(rng)]

}



fn create_hashmap_thread(rng:&mut StdRng, no_layer: &u32, barnsley_fern_config: &BarnsleyFernConfig, save_option: &SaveOption) -> IndexMap<[u32;2], usize> {


    let (img_width,img_height):(u32,u32)= get_empty_image_size((*save_option).scale_factor as f64/(*save_option).preview_downscale);
    let no_iterations =((((img_width as f64).powf(2.0) + ((img_height) as f64).powf(2.0)).powf(0.5)/10.0).powf(2.0)) as u32 *(barnsley_fern_config).sample_multiplier /(*no_layer);
    let mut probability_score:Vec<i32>=Vec::new();

    for i in 0..barnsley_fern_config.probability_score.len(){
        probability_score.push(if barnsley_fern_config.enabled[i]{
            barnsley_fern_config.probability_score[i]
        }else {0})
    }

    let mut x = 0.;
    let mut y = 0.;
    let mut point_map = IndexMap::new();

    for _ in 0..no_iterations {
        let cx: f64;
        let cy: f64;
        let w_no=non_uniform_rand(&probability_score, rng);

        cx = (*barnsley_fern_config).coefficient[w_no][0] * x as f64 + (*barnsley_fern_config).coefficient[w_no][1] * y as f64 +(*barnsley_fern_config).coefficient[w_no][4];
        cy = (*barnsley_fern_config).coefficient[w_no][2] * x as f64 + (*barnsley_fern_config).coefficient[w_no][3] * y as f64 + (*barnsley_fern_config).coefficient[w_no][5];

        x = cx;
        y = cy;

        let real_x = ((img_width as f64) / 2. + x * (img_width as f64) / 10.1).round() as u32;

        let real_y = ((img_height as f64)  - y * (img_height as f64) / 10.1).round() as u32;

        let counter = point_map.entry([real_y, real_x]).or_insert(0_usize);
        *counter += 2;

    }

    point_map
}

fn create_hashmap(barnsley_fern_config: &BarnsleyFernConfig, save_option: &SaveOption) -> IndexMap<[u32;2],usize> {
    let nthreads:u32=*super::N_THREADS;
    let (img_width,img_height):(u32,u32)= get_empty_image_size((*save_option).scale_factor as f64/(*save_option).preview_downscale);

    let mut rng = StdRng::seed_from_u64(32);
    let mut hashmap_vec=Vec::new();
    if save_option.enable_multithreading {
        let mut children = vec![];
        for _ in 0..nthreads as usize {
            let barnsley_fern_config_temp = (*barnsley_fern_config).clone();
            let save_option_temp = (*save_option).clone();
            let nthreads = nthreads.clone();
            let mut rng_temp = StdRng::seed_from_u64(rng.gen_range(0..4096) as u64);

            children.push(thread::spawn(move || {
                create_hashmap_thread(&mut rng_temp, &nthreads, &barnsley_fern_config_temp, &save_option_temp)
            }));
        }

        for child in children {
            hashmap_vec.push(child.join().unwrap());
        }
    }else {
        for _ in 0..nthreads as usize {
            let mut rng_temp = StdRng::seed_from_u64(rng.gen_range(0..4096) as u64);
            hashmap_vec.push( create_hashmap_thread(&mut rng_temp, &nthreads, &barnsley_fern_config, &save_option))
        }
    }
    let mut hashmap=IndexMap::new();

    let no_adj_pix:i32=if (*barnsley_fern_config).weight_adj{1}else {0};

    for i in 0..hashmap_vec.len(){

        for (k,v) in hashmap_vec[i].clone(){
            if k[0]>= img_width || k[1]>=img_height {
                continue
            }
            let palette = hashmap.entry(k).or_insert(v);
            *palette +=v;

            for j in -no_adj_pix..=no_adj_pix{
                for l in -no_adj_pix..=no_adj_pix{
                    if k[0]  as i32 + j < 0 || k[0] as i32 + j >= img_width as i32 || k[1]  as i32 +l < 0 || k[1] as i32 +l >= img_height  as i32 {
                        continue
                    }
                    if j!=0 || l !=0  {
                        let sub_counter = hashmap.entry([( k[0] as i32+ j) as u32, (k[1] as i32+l) as u32]).or_insert(1);
                        *sub_counter+=v
                    }
                }
            }

        }
    }
    hashmap
}



fn create_fern_thread(hashmap: &IndexMap<[u32;2],usize>, hue_rotator: &i32, barnsley_fern_config: &BarnsleyFernConfig, save_option: &SaveOption) -> (ImageBuffer<Rgba<u8>,Vec<u8>>, i32) {
    let (img_width,img_height):(u32,u32)= get_empty_image_size((*save_option).scale_factor as f64/(*save_option).preview_downscale);
    let mut img_buf = <ImageBuffer<Rgba<u8>, _>>::new(img_width, img_height);

        let mut count=0;
        for (k, v) in (*hashmap).clone() {
            let real_v = if v < (*barnsley_fern_config).color_palette.len() - 1 { v } else { (*barnsley_fern_config).color_palette.len() - 1 };
            count += 1;
            if *hue_rotator != 0 {
                if count % 10 != *hue_rotator {
                    continue
                }

                let real_y: f64 = (k[1] as f64) as f64 + (img_height as f64 / 9.0) as f64 * (if *hue_rotator <= 3 { *hue_rotator } else { *hue_rotator - 7 }) as f64;
                if real_y < 0.0 || real_y >= img_height as f64 {
                    continue
                }
                img_buf.put_pixel(k[0], real_y as u32,
                                  image::Rgba([255 - (*barnsley_fern_config).color_palette[real_v].r, 255 - (*barnsley_fern_config).color_palette[real_v].g, 255 - (*barnsley_fern_config).color_palette[real_v].b, 255])
                );
            } else {
                img_buf.put_pixel(k[0], k[1],
                                  image::Rgba([(*barnsley_fern_config).color_palette[real_v].r, (*barnsley_fern_config).color_palette[real_v].g, (*barnsley_fern_config).color_palette[real_v].b, 255])
                );
            }
        }

    (if *hue_rotator!=0 {
        imageops::huerotate(&img_buf, *hue_rotator * 60)
    }  else {
        img_buf
    },*hue_rotator)



}

fn create_ferns_mt(hashmap: &IndexMap<[u32;2],usize>,barnsley_fern_config: &BarnsleyFernConfig, save_option: &SaveOption)-> (ImageBuffer<Rgba<u8>,Vec<u8>>,ImageBuffer<Rgba<u8>,Vec<u8>>) {
    let (img_width, img_height): (u32, u32) = get_empty_image_size((*save_option).scale_factor as f64 / (*save_option).preview_downscale);
    let mut img_buf_foreground = <ImageBuffer<Rgba<u8>, _>>::new(img_width, img_height);
    let fern_count=if barnsley_fern_config.background_type != 1 {
       1
    }else {7};

    let mut children = Vec::new();
    for i in 0..fern_count {
        let hashmap_temp = hashmap.clone();
        let barnsley_fern_config_temp = barnsley_fern_config.clone();
        let save_action_temp = save_option.clone();
        let hue_rotator = i.clone();
        children.push(thread::spawn(move || {
            create_fern_thread(&hashmap_temp, &hue_rotator, &barnsley_fern_config_temp, &save_action_temp)
        }));
    }
    let mut fern_map = IndexMap::new();
    for child in children {
        let (fern, hue_rotator) = child.join().unwrap();
        if hue_rotator == 0 {
            img_buf_foreground = fern
        } else {
            fern_map.insert(hue_rotator, fern);
        }
    }

    let ( mut img_buf_background,return_flag) =  background_colorize(&RgbaImage::new(img_width, img_height),&barnsley_fern_config.background_type,save_option);
    if return_flag {
        return (img_buf_foreground, img_buf_background)
    }

    for i in 1..fern_count {
        overlay(&mut img_buf_background, fern_map.get(&i).unwrap(), 0, 0);
    }

    (img_buf_foreground, img_buf_background)
}

fn create_ferns_st(hashmap: &IndexMap<[u32;2],usize>,barnsley_fern_config: &BarnsleyFernConfig, save_option: &SaveOption)-> (ImageBuffer<Rgba<u8>,Vec<u8>>,ImageBuffer<Rgba<u8>,Vec<u8>>) {
    let (img_width, img_height): (u32, u32) = get_empty_image_size((*save_option).scale_factor as f64 / (*save_option).preview_downscale);

    let (img_buf_foreground, _) = create_fern_thread(&hashmap, &0, &barnsley_fern_config, &save_option);

    let ( mut img_buf_background,return_flag) =  background_colorize(&RgbaImage::new(img_width, img_height),&barnsley_fern_config.background_type,save_option);
    if return_flag {
        return (img_buf_foreground, img_buf_background)
    }

    for i in 1..7 {
        let (img_buf_background_detail, _) = create_fern_thread(&hashmap, &i, &barnsley_fern_config, &save_option);
        overlay(&mut img_buf_background, &img_buf_background_detail, 0, 0);
    }
    (img_buf_foreground, img_buf_background)
}


fn create_ferns(hashmap: &IndexMap<[u32;2],usize>,barnsley_fern_config: &BarnsleyFernConfig, save_option: &SaveOption)-> (ImageBuffer<Rgba<u8>,Vec<u8>>,ImageBuffer<Rgba<u8>,Vec<u8>>){
    if save_option.enable_multithreading{
        create_ferns_mt(hashmap,barnsley_fern_config,save_option)
    }else{
        create_ferns_st(hashmap,barnsley_fern_config,save_option)
    }
}

pub fn gen_barnsley_fern(barnsley_fern_config: &BarnsleyFernConfig, save_option:&SaveOption) -> DynamicImage {


    let hashmap = create_hashmap(barnsley_fern_config, save_option);

    let (img_buf_foreground,img_buf_background)=create_ferns(&hashmap,barnsley_fern_config,save_option);

    let mut dynamic_image=DynamicImage::ImageRgba8(combine_fg_bg(img_buf_foreground,img_buf_background));
    dynamic_image=dynamic_image.adjust_contrast(save_option.contrast);
    dynamic_image=dynamic_image.brighten(save_option.brightness);
    save_image(&dynamic_image, save_option, "Barnsley_fern");
    dynamic_image
}


