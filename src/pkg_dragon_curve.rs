use image::*;
use imageproc::drawing::*;
use std::f64::consts::PI;
use crate::img_generator::*;
use crate::io::*;
use palette::{Srgb, Hsv, Pixel, Hue, Shade};
use image::imageops::overlay;
use std::time::{Duration, Instant};
use indexmap::{IndexMap};
use std::thread;
use imageproc::definitions::Image;
use serde::{Serialize, Deserialize};


#[derive(Serialize, Deserialize, Clone,Debug)]
pub struct DragonConfig {
    pub max_dragon: usize,
    pub enabled: Vec<bool>,
    pub position_1: Vec<i32>,
    pub position_2: Vec<i32>,
    pub position_3: Vec<i32>,
    pub rotate: Vec<i32>,
    pub flip: Vec<i32>,
    pub no_iter: Vec<i32>,
    pub stroke_width: Vec<f64>,
    #[serde(skip)]
    pub color_palette: Vec<ColorRGB>,
    pub add_transparency:bool,
    pub add_gradient: bool,
    pub lighten: bool,
    pub background_type: i32,
}

fn gen_dragon_direction(n_iteration: i32) -> Vec<i32>{
    let mut dragon_cardinal:Vec<i32>=Vec::new();

    let mut prev=-1;
    for i in 0..n_iteration{
        let current=(((i & -i) << 1) & i) != 0;

        if prev == -1 {
            prev = if current { 1 } else { 3 };
        } else if prev == 3 {
            prev = if current { 0 } else { 2 };
        } else if prev == 1 {
            prev = if current { 2 } else { 0 };
        } else if prev == 2 {
            prev = if current { 1 } else { 3 };
        } else {
            prev = if current { 3 } else { 1 };
        }

        dragon_cardinal.push(prev);

    }
    dragon_cardinal
}

fn points_calculator(current_point:[i32;2], dist: f64, cardinal:i32, deviation:f64, rotate:usize,flip:i32) -> [i32;2]{
    let mut degree:Vec<f64>=vec![PI/2.0-2.0*deviation,PI-deviation,3.0*PI/2.0-2.0*deviation,-deviation];
    if flip!=0{
        let t=(degree[0],degree[1]);
        if flip!=2 {
            degree[1] = degree[3];
            degree[3] = t.1;
        }
        if flip!=1 {
            degree[0] = degree[2];
            degree[2] = t.0;
        }

    }
    let rad = degree[(cardinal as usize+ rotate)%4_usize];
    let t_x= rad.sin() *dist;
    let t_y= rad.cos() *dist;
    return [t_x.round() as i32+current_point[0],t_y.round() as i32+current_point[1]]
}

fn dragon_generator(no_inter: i32,first_pixel:[i32;2],pixel_dis:f64,deviation:f64,rotate: usize, flip:i32) ->(Vec<[i32;2]>,[i32;4]){
    let (mut min_x,mut min_y, mut max_x,mut max_y)=(0, 0, 0, 0);
    let mut point_link:Vec<[i32;2]>=Vec::new();
    point_link.push(first_pixel);
    let dragon_cardinal= gen_dragon_direction(no_inter);
    for i in 0..dragon_cardinal.len(){ //* (i as f64+no_inter as f64)/(no_inter as f64)
        point_link.push(points_calculator(point_link[i], pixel_dis , dragon_cardinal[i], deviation, rotate,flip));

        if point_link[i][0]> max_x{
            max_x=point_link[i][0];
        }
        if point_link[i][0]< min_x{
            min_x=point_link[i][0];
        }

        if point_link[i][1]> max_y{
            max_y=point_link[i][1];

        }
        if point_link[i][1]< min_y{
            min_y=point_link[i][1];
        }
    }
    (point_link,[min_x,min_y,max_x,max_y])
}


fn line_eqn_two_pts(point_a:[f64;2], point_b:[f64;2]) ->[f64;3]{
    assert!((point_a!=point_b));
    if point_a[0]==point_b[0]{
        [1.0,0.0,-point_a[0]]
    }else if point_a[1]==point_b[1]{
        [0.0,1.0,-point_a[1]]
    }else{
        [1.0,(point_b[0]-point_a[0])/(point_a[1]-point_b[1]),- point_a[0]- point_a[1]*(point_b[0]-point_a[0])/(point_a[1]-point_b[1])]
    }
}

fn line_eqn_pen(point:[f64;2],line:[f64;3]) ->[f64;3]{
    assert!(line[0]!=0.0 || line[1]!=0.0);
    if line[0]==0.0{
        [1.0,0.0,-point[0]]
    }else if line[1]==0.0 {
        [0.0,1.0,-point[1]]
    }else {
        [-line[1],line[0],line[1]*point[0]-line[0]*point[1]]
    }
}

fn two_pts_from_center(point:[f64;2],line:[f64;3], distance: f64) -> ([f64;2],[f64;2]){
    assert!(line[0]!=0.0 || line[1]!=0.0);
    let (x1, x2, y1, y2):(f64,f64,f64,f64);
    if line[0]==0.0{
        x1=(distance.powf(2.0)-(-line[2]/line[1]-point[1]).powf(2.0)).sqrt()+point[0];
        y1=-line[2]/line[1];
        x2=-(distance.powf(2.0)-(-line[2]/line[1]-point[1]).powf(2.0)).sqrt()+point[0];
        y2=-line[2]/line[1];
    }else if line[1]==0.0{
        y1=(distance.powf(2.0)-(-line[2]/line[0]-point[0]).powf(2.0)).sqrt()+point[1];
        x1=-line[2]/line[0];
        y2=-(distance.powf(2.0)-(-line[2]/line[0]-point[0]).powf(2.0)).sqrt()+point[1];
        x2=-line[2]/line[0];
    }else {
        let a=(line[1]/line[0]).powf(2.0)+1.0;
        let b=(line[1]/line[0])*(line[2]/line[0]+point[0])-point[1];
        let c= (line[2]/line[0]+point[0]).powf(2.0)+point[1].powf(2.0)-distance.powf(2.0);

        let delta=b.powf(2.0)-a*c;
        y1=(-b+delta.sqrt())/a;
        x1=(-line[1]*y1-line[2])/line[0];
        y2=(-b-delta.sqrt())/a;
        x2=(-line[1]*y2-line[2])/line[0];
    }
    ([x1,y1],[x2,y2])

}

fn distance_two_points(point_a:[f64;2], point_b:[f64;2]) ->f64 {
    ((point_a[0]-point_b[0]).powf(2.0)+(point_a[1]-point_b[1]).powf(2.0)).sqrt()
}

fn point_line_pos(point:[f64;2],line:[f64;3]) ->i32{
    let sign=line[0]*point[0]+line[1]*point[1]+line[2];
    if sign>0.0 {
        1
    }else if sign<0.0{
        -1
    }else {0}
}

fn find_new_starting_pts(point_set:([i32;2],[i32;2],[i32;2]), position_1: i32, position_2: i32) ->[i32;2]{
    if position_1 ==0 {
        if position_2==0 {
            point_set.0
        }else {
            point_set.2
        }
    }else if position_1 ==3 {
        if position_2==1 {
            [2 * point_set.2[0] - point_set.0[0], 2 * point_set.2[1] - point_set.0[1]]
        }else {
            [2 * point_set.0[0] - point_set.2[0], 2 * point_set.0[1] - point_set.2[1]]
        }
    }else {
        let start_f64 = [point_set.0[0] as f64, point_set.0[1] as f64];
        let start_2nd_f64 = [point_set.1[0] as f64, point_set.1[1] as f64];
        let center_f64 = [point_set.2[0] as f64, point_set.2[1] as f64];
        let line_through = line_eqn_two_pts(start_f64, center_f64);
        let line_perpendicular = if position_2==1{
            line_eqn_pen(center_f64, line_through)}
        else {
            line_eqn_pen(start_f64, line_through)};

        let (p1, p2) = if position_2==1 {
            two_pts_from_center(start_f64, line_perpendicular, distance_two_points(start_f64, center_f64) * (2.0_f64.sqrt()))
        }else {
            two_pts_from_center(center_f64, line_perpendicular, distance_two_points(start_f64, center_f64) * (2.0_f64.sqrt()))
        };
        let sign_0 = point_line_pos(start_2nd_f64, line_through);
        let sign_1 = point_line_pos(p1, line_through);
        let p_final =

                if position_1 ==2 {
                    if sign_0 == sign_1 {
                        p1
                    } else { p2 }
                } else {
                    if sign_0 == sign_1 {
                        p2
                    } else { p1 }
                }

        ;
        [p_final[0].round() as i32, p_final[1].round() as i32]
    }

}

fn get_point_set(dragon_curve: &Vec<[i32;2]>) ->([i32;2], [i32;2], [i32;2]){
    (dragon_curve[0],dragon_curve[1],dragon_curve[dragon_curve.len()-1])
}

fn generate_dragon_data(dragon_config: &DragonConfig, save_option:&SaveOption) ->Vec<(Vec<[i32;2]>,[i32;4])>{
    let (img_width ,img_height)=get_empty_image_size((*save_option).scale_factor as f64/(*save_option).preview_downscale);

    let mut dragon_data:Vec<(Vec<[i32;2]>,[i32;4])>=Vec::new();


    dragon_data.push(dragon_generator(2_i32.pow(dragon_config.no_iter[0] as u32 ) -1, [0,0], 2.0*dragon_config.stroke_width[0]*(save_option.scale_factor as f64)/(*save_option).preview_downscale, 0.0, ((*dragon_config).rotate[0]) as usize, (*dragon_config).flip[0]));
    for i in 1..dragon_config.max_dragon {
        let n=dragon_config.position_3[i] as usize;
        if dragon_config.enabled[i] && dragon_config.enabled[n] {
            dragon_data.push(dragon_generator(2_i32.pow(dragon_config.no_iter[i] as u32) -1,
                                              find_new_starting_pts(get_point_set(&dragon_data[n].0), (*dragon_config).position_1[i], (*dragon_config).position_2[i]), 2.0*dragon_config.stroke_width[i]*(save_option.scale_factor as f64)/(*save_option).preview_downscale,
                                              0.0, ((*dragon_config).rotate[i]) as usize, (*dragon_config).flip[i]));
        }else {
            continue
        }
    }
    let (mut min_x, mut min_y,mut max_x, mut max_y)=(99999,99999,0,0);


    for i in 0..dragon_data.len(){
        if max_x<(dragon_data[i].1)[2] {
            max_x=(dragon_data[i].1)[2]
        }
        if max_y<(dragon_data[i].1)[3] {
            max_y=(dragon_data[i].1)[3]
        }
        if min_x>(dragon_data[i].1)[0] {
            min_x=(dragon_data[i].1)[0]
        }
        if min_y>(dragon_data[i].1)[1] {
            min_y=(dragon_data[i].1)[1]
        }
    }

    let (dif_x,dif_y)=((img_width as i32-(max_x+min_x))/2,(img_height as i32-(max_y+min_y))/2);

    for i in 0..dragon_data.len(){
        for j in 0..dragon_data[i].0.len() {
            (dragon_data[i].0)[j][0]+=dif_x;
        }
        for j in 0..dragon_data[i].0.len() {
            (dragon_data[i].0)[j][1]+=dif_y;
        }
            (dragon_data[i].1)[0]+=dif_x;
            (dragon_data[i].1)[1]+=dif_y;
            (dragon_data[i].1)[2]+=dif_x;
            (dragon_data[i].1)[3]+=dif_y;

    }
    dragon_data

}
fn create_foreground_layer_thread(dragon_data: &(Vec<[i32;2]>, [i32;4]), layer_no:&usize, dragon_config: &DragonConfig, save_option: &SaveOption) ->(ImageBuffer<Rgba<u8>,Vec<u8>>, usize) {
    let (img_width ,img_height)=get_empty_image_size((*save_option).scale_factor as f64/(*save_option).preview_downscale);
    let mut img_buf = <ImageBuffer<Rgba<u8>, _>>::new(img_width, img_height);
    let extra_width=((save_option.scale_factor as f64 -1.0).sqrt()/2.0).round() as i32;
    let val_x_max_sqr =(((dragon_data.0.len()-1).pow(2)) as f32+1_f32).log2().powf(2.0);
    let color_set_vec_hsv= rgb_hsv_vec(&dragon_config.color_palette);
    let color_hsv = color_set_vec_hsv[*layer_no];
    for j in 0..dragon_data.0.len()-1 {
        let val_x=(j.pow(2) as f32+1_f32).log2();
        let mut new_color_hsv=color_hsv;
        if dragon_config.add_gradient {
            new_color_hsv=new_color_hsv.shift_hue(100.0*(1.0-(val_x.powf(2.0))/ val_x_max_sqr));
        }
        if dragon_config.lighten{
            new_color_hsv=new_color_hsv.lighten(0.5);
        }
        let new_color_rgb=hsv_rgb(&new_color_hsv);
        let transparency = if !dragon_config.add_transparency{255_u8 }else {
            (255.0*(1.0-(val_x.powf(1.8))/ val_x_max_sqr)) as u8
        };

        draw_line_segment_mut(&mut img_buf, ((dragon_data.0)[j][0] as f32, (dragon_data.0)[j][1] as f32),
                              ((dragon_data.0)[j+1][0] as f32, (dragon_data.0)[j+1][1] as f32),
                              image::Rgba([new_color_rgb.r,new_color_rgb.g, new_color_rgb.b, transparency] )
        );
        for k in 1..=extra_width {
            for l in &[k,-1*k] {
                if (dragon_data.0)[j][1] == (dragon_data.0)[j + 1][1] {
                    let sign=if (dragon_data.0)[j][0]<(dragon_data.0)[j+1][0] {1.0}else{-1.0};
                    draw_line_segment_mut(&mut img_buf, ((dragon_data.0)[j][0] as f32 - sign*extra_width as f32, (dragon_data.0)[j][1] as f32 + *l as f32),
                                          ((dragon_data.0)[j + 1][0] as f32 + sign*extra_width as f32, (dragon_data.0)[j + 1][1] as f32 +  *l as f32),
                                          image::Rgba([new_color_rgb.r, new_color_rgb.g, new_color_rgb.b, transparency])
                    );
                } else {
                    let sign=if (dragon_data.0)[j][1]<(dragon_data.0)[j+1][1] {1.0}else{-1.0};
                    if (dragon_data.0)[j][0] == (dragon_data.0)[j + 1][0] {
                        draw_line_segment_mut(&mut img_buf, ((dragon_data.0)[j][0] as f32 + *l as f32, (dragon_data.0)[j][1] as f32 - sign*extra_width as f32),
                                              ((dragon_data.0)[j + 1][0] as f32 +  *l as f32, (dragon_data.0)[j + 1][1] as f32 + sign*extra_width as f32),
                                              image::Rgba([new_color_rgb.r, new_color_rgb.g, new_color_rgb.b, transparency])
                        );
                    }
                }
            }
        }
    }
    (img_buf, *layer_no)
}

fn create_foreground_layers(dragon_data_vec: &Vec<(Vec<[i32;2]>, [i32;4])>, dragon_config: &DragonConfig, save_option:&SaveOption) ->Vec<ImageBuffer<Rgba<u8>,Vec<u8>>>{

    let mut img_buf_vec:Vec<ImageBuffer<Rgba<u8>,Vec<u8>>>=Vec::new();
    let mut layer_map = IndexMap::new();
    if save_option.enable_multithreading {
        let mut children = vec![];
        for i in 0..dragon_data_vec.len() {
            let dragon_config_temp= dragon_config.clone();
            let save_option_temp=save_option.clone();
            let dragon_data=dragon_data_vec[i].clone();
            let layer_num=i;
            children.push(thread::spawn(move || {
                create_foreground_layer_thread(&dragon_data, &layer_num, &dragon_config_temp, &save_option_temp)
            }));
        }
        for child in children {
            let (layer,layer_no) = child.join().unwrap();
            layer_map.insert(layer_no,layer);

        }
        for i in 0..dragon_data_vec.len() {
            let foreground =layer_map.get(&i).unwrap(); //:ImageBuffer<Rgba<u8>,Vec<u8>>
            img_buf_vec.push((*foreground).clone());

        }
    } else {
        for i in 0..dragon_data_vec.len() {
            let (img_buf,_) = create_foreground_layer_thread(&dragon_data_vec[i], &i, dragon_config, save_option);
            img_buf_vec.push(img_buf)
        }
    }

    img_buf_vec
}

fn create_foreground(foreground_layers: &Vec<ImageBuffer<Rgba<u8>,Vec<u8>>>)->ImageBuffer<Rgba<u8>,Vec<u8>>{
    let mut img_buf=foreground_layers[0].clone();
    for i in 1..foreground_layers.len(){
        overlay(&mut img_buf, &foreground_layers[i], 0, 0);
    }
    img_buf
}

fn pixel_finder(x:&i32,y:&i32,min:&i32, max: &i32, step: &usize,layer: &ImageBuffer<Rgba<u8>,Vec<u8>>)->(i32,[u8;4]){
    let mut found_pixel=-1;
    let mut found_color:[u8;4] = [0,0,0,0];
    let j =if (*x)>=0{
        x
    }else {
        y
    };

    let range = if *min <= *max {
        (*min..=*max).step_by(*step).collect::<Vec<i32>>()
    }else {
        (*max..=*min).rev().step_by(*step).collect::<Vec<i32>>()
    };

    for k in range {

        let pixel = if (*x) >= 0 {
            (*layer).get_pixel((*j) as u32, k as u32)
        } else {
            (*layer).get_pixel(k as u32, (*j) as u32)
        };

        let image::Rgba(data) = *pixel;
        if data[3] > 0 {
            found_pixel = k;
            found_color = [255 - data[0], 255 - data[1], 255 - data[2], 255];
            break;
        }
    }

    (found_pixel,found_color)

}

fn create_background_layer(min_max_data:&[i32;4],foreground_layer:&ImageBuffer<Rgba<u8>,Vec<u8>>,layer_no: &usize,dragon_config: &DragonConfig, save_option:&SaveOption)->(ImageBuffer<Rgba<u8>,Vec<u8>>, usize){
    let pixel_distance=(2.0*dragon_config.stroke_width[*layer_no]*(save_option.scale_factor as f64)/(*save_option).preview_downscale as f64) as usize;
    let (img_width ,img_height)=get_empty_image_size((*save_option).scale_factor as f64/(*save_option).preview_downscale);
    assert_eq!(img_width, img_height);
    let mut img_buf = RgbaImage::new(img_width as u32, img_height as u32);
    if (min_max_data[0] == min_max_data[2]) && (min_max_data[1]==min_max_data[3]){
        return (img_buf,*layer_no)
    }
    for i in 0..2 {
        let (min_1, max_1, min_2, max_2) = if i==0 {
            (min_max_data[0],min_max_data[2],min_max_data[1],min_max_data[3])
        }else {
            (min_max_data[1],min_max_data[3],min_max_data[0],min_max_data[2])
        };
        for j in (min_1..=max_1).step_by(pixel_distance) {
            let (mut min_temp, mut max_temp)=(min_2, max_2);
            if j<0 || j >= img_width as i32 {
                continue
            }
            if min_temp<0 {
                min_temp=0;
            }
            if max_temp>= img_width as i32 {
                max_temp= img_width as i32 -1;
            }
            let (mut a, mut b)= if i==0{
                //i==0: sinh ra sọc dọc
                (j,-1)
            }else {
                //i==1: sinh ra sọc ngang
                (-1,j)
            };

            let (row_min_pixel_x, color_start) = pixel_finder(&a, &b, &min_temp, &max_temp, &pixel_distance, foreground_layer);
            let (row_max_pixel_x, color_finish) = pixel_finder(&a, &b, &max_temp, &min_temp, &pixel_distance, foreground_layer);

            let row_distance = row_max_pixel_x - row_min_pixel_x + 1;
            let mut row_start = row_min_pixel_x - row_distance / 2;
            if row_start < 0 {
                row_start = 0;
            }
            let mut row_finish = row_max_pixel_x + row_distance / 2;
            if row_finish > img_width as i32 {
                row_finish = img_width as i32;
            }
            let extra_width=((save_option.scale_factor as f64 -1.0).sqrt()/2.0).round() as i32;
            for k in row_start..row_finish {
                let (d1, d2) = ((row_min_pixel_x - k).abs(), (row_max_pixel_x - k).abs());
                let inverse_distance_sum=(1.0 /d1 as f64).powf(2.0)+(1.0 /d2 as f64).powf(2.0);
                let mut color_new: [u8; 3] = [0, 0, 0];
                for l in 0..3 {
                    color_new[l] = (color_start[l] as f64 * (1.0 /d1 as f64).powf(2.0) / inverse_distance_sum) as u8 + (color_finish[l] as f64 * (1.0 /d2 as f64).powf(2.0) / inverse_distance_sum) as u8;
                }
                let mut color_new_hsl = rgb_hsl(&ColorRGB {
                    r: color_new[0],
                    g: color_new[1],
                    b: color_new[2],
                    a: 255
                });
                color_new_hsl.lightness=0.5;
                if i==0{
                    b=k
                }else {
                    a=k
                };
                let color_final=hsl_rgb(&color_new_hsl);
                let da = if d1<d2 {d1 } else {d2};
                let alpha=255.0 * ((row_distance-da)as f64/row_distance as f64).powf(3.0) ;
                img_buf.put_pixel(a as u32, b as u32, image::Rgba([color_final.r, color_final.g, color_final.b, alpha as u8]));
                for l in 0..extra_width{
                    let (ew,eh) = if i==0 {
                        (l+1,0)
                    }else {
                        (0,l+1)
                    };
                    if a+ew < img_width as i32 && b+eh < img_height as i32 {
                        img_buf.put_pixel((a + ew) as u32, (b + eh) as u32, image::Rgba([color_final.r, color_final.g, color_final.b, alpha as u8]));
                    }
                    if a-ew >=0 && b-eh>=0 {
                        img_buf.put_pixel((a - ew) as u32, (b - eh) as u32, image::Rgba([color_final.r, color_final.g, color_final.b, alpha as u8]));
                    }
                }

            }
        }
    }
    (img_buf, *layer_no)
}


fn create_background(dragon_data: &Vec<(Vec<[i32;2]>, [i32;4])>, foreground_layers:&Vec<ImageBuffer<Rgba<u8>,Vec<u8>>>, dragon_config: &DragonConfig, save_option:&SaveOption) ->ImageBuffer<Rgba<u8>,Vec<u8>>{
    let (img_width ,img_height)=get_empty_image_size((*save_option).scale_factor as f64/(*save_option).preview_downscale);

    let (mut img_buf,return_flag) =  background_colorize(&RgbaImage::new(img_width, img_height),&dragon_config.background_type ,save_option);
    if return_flag {
        return img_buf
    }

    let mut background_vec:Vec<ImageBuffer<Rgba<u8>,Vec<u8>>>=Vec::new();
    let mut layer_map = IndexMap::new();
    if save_option.enable_multithreading {
        let mut children = vec![];
        for i in 0..foreground_layers.len() {
            let min_max_data = (dragon_data[i].1).clone();
            let foreground_layer = foreground_layers[i].clone();
            let dragon_config_temp= dragon_config.clone();
            let save_option_tempt=save_option.clone();
            let layer_num = i;
            children.push(thread::spawn(move || {
                create_background_layer(&min_max_data, &foreground_layer, &layer_num, &dragon_config_temp,  &save_option_tempt)
            }));
        }
        for child in children {
            let (layer, layer_no) = child.join().unwrap();
            layer_map.insert(layer_no,layer);
        }
        for i in 0..foreground_layers.len() {
            let background = layer_map.get(&i).unwrap();
            background_vec.push((*background).clone());
        }
    }else {

        for i in 0..foreground_layers.len() {
            let (bg_layer, _) = create_background_layer(&(dragon_data[i].1), &(foreground_layers)[i], &i, dragon_config, save_option);
            background_vec.push(bg_layer);
        }
    }
    for i in 0..background_vec.len() {
        overlay(&mut img_buf, &background_vec[i], 0, 0);
    }
    img_buf
}

pub fn  gen_dragon(dragon_config: &DragonConfig, save_option: &SaveOption)->DynamicImage{

    let dragon_data = generate_dragon_data(dragon_config,save_option);

    let img_buf_foreground_layers= create_foreground_layers(&dragon_data, dragon_config, save_option);

    let img_buf_foreground = create_foreground(&img_buf_foreground_layers);

    let img_buf_background= create_background(&dragon_data, &img_buf_foreground_layers, dragon_config, save_option);


    let mut dynamic_image =DynamicImage::ImageRgba8(combine_fg_bg(img_buf_foreground, img_buf_background));
    dynamic_image=dynamic_image.adjust_contrast(save_option.contrast);
    dynamic_image=dynamic_image.brighten(save_option.brightness);
    save_image(&dynamic_image, save_option, "Dragon_curve");
    dynamic_image

}