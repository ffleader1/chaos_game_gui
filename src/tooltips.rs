use indexmap::{IndexMap};


pub fn gen_tooltip_hashmap() ->IndexMap<String,String>{
    let mut tooltip_map=IndexMap::new();

    tooltip_map.insert("chs_gm_inp_scalar".to_string(),"Change image scale.".to_string());
    tooltip_map.insert("chs_gm_sli_scalar".to_string(),"Change image scale.".to_string());
    tooltip_map.insert("chs_gm_inp_no_vertices".to_string(),"Change the number of initial points.".to_string());
    tooltip_map.insert("chs_gm_check_incl_center".to_string(),"Include the center of initial points as jump point.".to_string());
    tooltip_map.insert("chs_gm_check_incl_mid".to_string(),"Include the midpoints between two adjacent initial Points as jump points.".to_string());
    tooltip_map.insert("chs_gm_inp_jump_ratio".to_string(),"Change the length of the jump toward another vertex.".to_string());
    tooltip_map.insert("chs_gm_sli_jump_ratio".to_string(),"Change the length of the jump toward another vertex.".to_string());
    tooltip_map.insert("chs_gm_ch_condition".to_string(),"Special condition for choosing the vertex to jump.\n\
    1: No special condition.\n\
    2: Avoid choosing a/some previously selected vertex(s) as jump point.\n\
    3: Increase the chance of re-selecting a previously chosen vertex as jump point.\n\
    4: Avoid choosing the neighbors of the previously selected vertex as jump point.".to_string());
    tooltip_map.insert("chs_gm_inp_modifier_i".to_string(),"Modify special condition parameter.".to_string());


    tooltip_map.insert("rgb_inp_color_rand_seed".to_string(),"This fractal representation uses RGB color space palette.\n\
    Change the random seed to generate color palette.".to_string());
    tooltip_map.insert("rgb_check_custom_color".to_string(),"Use defined custom color palette instead.\n\
    More advanced as it requires editing .json file".to_string());
    tooltip_map.insert("rgb_but_edit_color".to_string(),"This fractal representation uses RGB color space palette.\n\
    Edit the .json file for color palette (Will open the default .json file editor).".to_string());
    tooltip_map.insert("rgb_but_load_color".to_string(),"Load custom color palette from the .json file.".to_string());
    tooltip_map.insert("rgb_check_transparent_color".to_string(),"Add transparency to the colors.".to_string());
    tooltip_map.insert("rgb_check_gradient_color".to_string(),"Add gradient to the colors.".to_string());
    tooltip_map.insert("rgb_check_lighten".to_string(),"Lighten the colors.".to_string());

    tooltip_map.insert("chs_gm_ch_background".to_string(),"Change the background style of the image.".to_string());


    tooltip_map.insert("jul_inp_scalar".to_string(),"Change image scale.".to_string());
    tooltip_map.insert("jul_sli_scalar".to_string(),"Change image scale.".to_string());
    tooltip_map.insert("jul_inp_c_real".to_string(),"Julia set can be express as f(z)=Z^n+C.\n\
    C is a complex number.\n\
    This value represents the real part of C.".to_string());
    tooltip_map.insert("jul_sli_c_real".to_string(),"Julia set can be express as f(z)=Z^n+C.\n\
    C is a complex number.\n\
    This value represents the real part of C.".to_string());
    tooltip_map.insert("jul_inp_c_imaginary".to_string(),"Julia set can be express as f(z)=Z^n+C.\n\
    C is a complex number.\n\
    This value represents the imaginary part of C.".to_string());
    tooltip_map.insert("jul_sli_c_imaginary".to_string(),"Julia set can be express as f(z)=Z^n+C.\n\
    C is a complex number.\n\
    This value represents the imaginary part of C.".to_string());
    tooltip_map.insert("jul_inp_z_pow".to_string(),"Julia set can be express as f(z)=Z^n+C.\n\
    This value represents n - the exponent of Z.".to_string());
    tooltip_map.insert("jul_sli_z_pow".to_string(),"Julia set can be express as f(z)=Z^n+C.\n\
    This value represents n - the exponent of Z.".to_string());



    tooltip_map.insert("hsv_hue_from".to_string(),"This fractal representation uses HSV color space palette.\n\
    Change the Hue (in HSV) of the least iterated point(s).".to_string());
    tooltip_map.insert("hsv_hue_to".to_string(),"This fractal representation uses HSV color space palette.\n\
    Change the Hue (in HSV) of the most iterated point(s).".to_string());
    tooltip_map.insert("hsv_sat_from".to_string(),"Change the Saturation (in HSV) of the least iterated point(s).".to_string());
    tooltip_map.insert("hsv_sat_to".to_string(),"Change the Saturation (in HSV) of the most iterated point(s).".to_string());
    tooltip_map.insert("hsv_val_from".to_string(),"Change the Value (in HSV) of the least iterated point(s)".to_string());
    tooltip_map.insert("hsv_val_to".to_string(),"Change the Value (in HSV) of the most iterated point(s)".to_string());

    tooltip_map.insert("jul_ch_background".to_string(),"Change the background style of the image.".to_string());


    tooltip_map.insert("dragon_choice_current".to_string(),"Select the dragon to config.".to_string());
    tooltip_map.insert("dragon_check_enabled".to_string(),"Enable the currently selected dragon.".to_string());
    tooltip_map.insert("dragon_ch_position".to_string(),"Change the starting position of the currently selected dragon, relative to another.".to_string());
    tooltip_map.insert("dragon_ch_rotate".to_string(),"Change the rotation of the currently selected dragon.".to_string());
    tooltip_map.insert("dragon_ch_flip".to_string(),"Change the orientation of the currently selected dragon.".to_string());
    tooltip_map.insert("dragon_inp_no_iter".to_string(),"Change the number of iteration to draw the currently selected dragon.".to_string());
    tooltip_map.insert("dragon_sli_no_iter".to_string(),"Change the number of iteration to draw the currently selected dragon.".to_string());
    tooltip_map.insert("dragon_inp_stroke_width".to_string(),"Change the stroke width to draw the currently selected dragon.".to_string());
    tooltip_map.insert("dragon_sli_stroke_width".to_string(),"Change the stroke width to draw the currently selected dragon.".to_string());
    tooltip_map.insert("dragon_ch_background".to_string(),"Change the background style of the image.".to_string());

    tooltip_map.insert("fern_choice_current".to_string(),"Barnsley fern can be represented by 4 different equations.\n\
    Each equation has six different co-efficient (a,c,d,e,f) and a probability p.\n\
    Select the equation of the Barnsley fern to config.".to_string());
    tooltip_map.insert("fern_check_enabled".to_string(),"Enable the currently selected equation.".to_string());
    tooltip_map.insert("fern_inp_co_temp".to_string(),"Change the co-efficient.".to_string());
    tooltip_map.insert("fern_sli_co_temp".to_string(),"Change the co-efficient.".to_string());
    tooltip_map.insert("fern_inp_prob".to_string(),"Change the probability, represented by the probability score.".to_string());
    tooltip_map.insert("fern_sli_prob".to_string(),"Change the probability, represented by the probability score.".to_string());
    tooltip_map.insert("fern_inp_sample".to_string(),"Change the number of random sample points user to construct the fern.".to_string());
    tooltip_map.insert("fern_sli_sample".to_string(),"Change the number of random sample points user to construct the fern.".to_string());
    tooltip_map.insert("fern_check_weight_adj".to_string(),"A point selected nearby previously chosen points will increase their iteration count.".to_string());
    tooltip_map.insert("fern_ch_background".to_string(),"Change the background style of the image.".to_string());



    tooltip_map.insert("mandelbrot_inp_scalar".to_string(),"Change the zoom-in scale.".to_string());
    tooltip_map.insert("mandelbrot_sli_scalar".to_string(),"Change the zoom-in scale.".to_string());
    tooltip_map.insert("mandelbrot_ck_show_zoom".to_string(),"Show the zoom-in scale.".to_string());
    tooltip_map.insert("mandelbrot_inp_x_loc".to_string(),"Change zoom-in area x-coordinate.".to_string());
    tooltip_map.insert("mandelbrot_sli_x_loc".to_string(),"Change zoom-in area x-coordinate.".to_string());
    tooltip_map.insert("mandelbrot_inp_y_loc".to_string(),"Change zoom-in area y-coordinate.".to_string());
    tooltip_map.insert("mandelbrot_inp_y_loc".to_string(),"Change zoom-in area y-coordinate.".to_string());
    tooltip_map.insert("mandelbrot_ck_show_selector".to_string(),"Show the coordinate selector.".to_string());
    tooltip_map.insert("mandelbrot_ck_but_zoom_in".to_string(),"Zoom into the selected region.".to_string());
    tooltip_map.insert("mandelbrot_ck_but_zoom_out".to_string(),"Zoom out to the previously selected region.".to_string());
    tooltip_map.insert("mandelbrot_inp_max_inter".to_string(),"Change the maximum number of iteration to generate the Mandelbrot set.".to_string());
    tooltip_map.insert("mandelbrot_sli_max_inter".to_string(),"Change the maximum number of iteration to generate the Mandelbrot set.".to_string());
    tooltip_map.insert("mandelbrot_inp_max_z".to_string(),"The Mandelbrot set can be explained with the equation Zn+1 = Zn^2 + C.\n\
    This value represents the maximum value of Z".to_string());
    tooltip_map.insert("mandelbrot_sli_max_z".to_string(),"The Mandelbrot set can be explained with the equation Zn+1 = Zn^2 + C.\n\
    This value represents the maximum value of Z".to_string());

    tooltip_map.insert("rgb_cof_ln".to_string(),"Coefficient when calculating ln(Z). Used for coloring".to_string());
    tooltip_map.insert("rgb_cof_r".to_string(),"Coefficient when calculating Red.".to_string());
    tooltip_map.insert("rgb_cof_g".to_string(),"Coefficient when calculating Green.".to_string());
    tooltip_map.insert("rgb_cof_b".to_string(),"Coefficient when calculating Blue.".to_string());

    tooltip_map.insert("mandelbrot_ch_coloring".to_string(),"Change the coloring algorithm for the image.".to_string());

    tooltip_map.insert("check_smooth_preview".to_string(),"Reduce preview resolution to make the process smoother.".to_string());
    tooltip_map.insert("check_enable_smt".to_string(),"Utilize multicore processing to speed up review and export.".to_string());
    tooltip_map.insert("ch_export_resolution".to_string(),"Change the exported image resolution.".to_string());
    tooltip_map.insert("check_un_crop".to_string(),"Export the image in 1:1 ratio (square) instead of 16:9.".to_string());
    tooltip_map.insert("check_un_crop".to_string(),"Export the image in 1:1 ratio (square) instead of 16:9.".to_string());
    tooltip_map.insert("adj_contrast_inp".to_string(),"Adjust contrast.".to_string());
    tooltip_map.insert("adj_contrast_sli".to_string(),"Adjust contrast.".to_string());
    tooltip_map.insert("adj_brightness_inp".to_string(),"Adjust brightness.".to_string());
    tooltip_map.insert("adj_brightness_sli".to_string(),"Adjust brightness.".to_string());
    tooltip_map.insert("but_save_config".to_string(),"Save configuration to file.".to_string());
    tooltip_map.insert("but_load_config".to_string(),"Load configuration to file.".to_string());
    tooltip_map.insert("but_reset_config".to_string(),"Reset configuration to default.".to_string());
    tooltip_map.insert("but_export_image".to_string(),"Export image.".to_string());
    tooltip_map.insert("but_locate_image".to_string(),"Locate the export image folder.".to_string());

    tooltip_map
}