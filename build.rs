extern crate winres;

use std::path::Path;

fn main() {
    if cfg!(target_os = "windows") {
        let mut res = winres::WindowsResource::new();
        let ico_path =Path::new("icon.ico");
        if ico_path.exists() {
            res.set_icon(ico_path.to_str().unwrap());
        }
        res.compile().unwrap();
    }
}